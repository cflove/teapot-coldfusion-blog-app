<!--- ************************************************************ --->
<!--- Login/logout Page. Independent page.                         --->
<!--- ************************************************************ --->
<cfinclude template="system/defaults.cfm">
<cfimport taglib="system/ct" 	prefix="ct">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="http://code.jquery.com/jquery-1.5.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<title><cfoutput>#Application.sti.sitename#</cfoutput> : Please Login</title>
</head><body>
<cfparam name="form.username" 	default="">
<cfparam name="form.password" 	default="">
<cfparam name="form.keep"		default="">
<cfparam name="url.r"			default="">
<cfparam name="url.path"		default="">
<cfparam name="url.action"		default="">

<!--- ************************************************************ --->
<!--- Logout                                                       --->
<!--- ************************************************************ --->
<cfswitch expression="#url.path#">
	<cfcase value="logout">
		<cfcookie name="blogid" 	expires="now">
		<cfset StructClear(Session)>
		<cftry>
		<cfset session.setMaxInactiveInterval(javaCast( "long", 1 ))>
		<cfcatch></cfcatch></cftry>
	</cfcase>
</cfswitch>


<div style="vertical-align:middle; height:100%; width:100%;" align="center">
<div style="position: absolute; left:0px; width:100%; top: 30%; vertical-align: middle; ">
<ct:msg>
<div style="background-color:#F0F0F0; border-bottom:1px solid #CCC; background-image:url(images/loginlinebg.png); background-repeat:repeat-x; background-position:0px 43px">

<cfswitch expression="#url.action#">
<cfcase value="formunlock">
<!--- ************************************************************ --->
<!--- Unlock account Message                                       --->
<!--- ************************************************************ --->
<div style="text-align:right"><a href="login.cfm"><img src="images/loginback.png" title="Back" style="float:right" align="absmiddle" border="0" /></a></div>
<div style="background-color:#CCC"><div style="width:330px; text-align:center; margin-left:auto; margin-right:auto;"><img src="images/teapot.gif" align="absmiddle" /><img class="hello" src="images/helpisontheway.gif" align="absmiddle" /></div></div>
<div style="width:450px; padding:5px; text-align:center; margin-left:auto; margin-right:auto;">
<br />Your account is locked to prevent unauthorized access to your account. Don't worry, we just send you an email with instruction how to unlock your account with a single click. 
<b>Please check your email</b>. If mail is missing, don't forget to peek into your junk mail folder.
<div style="text-align:center; padding:10px">
<input title="Back" class="buttonbk" type="submit" value="Back" name="Back" onclick="window.location='login.cfm'" />
</div></div>
</cfcase>
<cfcase value="unlock">
<!--- ************************************************************ --->
<!--- Unlock account                                               --->
<!--- ************************************************************ --->
	<cfquery name="addunlockkey" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		update bloguser set 
		unlockkey	= NULL,
		retrycount	= 0
		where unlockkey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#url.key#">
	</cfquery>
	<cfset session.msg	= "Your account unlocked successfully">
	<cfset session.login.fail = 0>
	<cflocation addtoken="no" url="login.cfm">
</cfcase>
<cfcase value="help">
<!--- ************************************************************ --->
<!--- Forget password form                                         --->
<!--- ************************************************************ --->
<div style="text-align:right"><a href="login.cfm"><img src="images/loginback.png" title="Back" style="float:right" align="absmiddle" border="0" /></a></div>
<div style="background-color:#CCC"><div style="width:330px; text-align:left; margin-left:auto; margin-right:auto;"><img src="images/teapot.gif" align="absmiddle" /><img src="images/whatsmylogin.gif" align="absmiddle" /></div></div>
<div style="width:350px; padding:5px; text-align:left; margin-left:auto; margin-right:auto;">

<div style="padding-left:10px; padding-top:5px">Forgotten your login details? Please enter your Email. We will email you new login details at once.</div>

<cfform action="login.cfm?action=helpdone" method="post" id="forgetform">
	<div class="formfield">
	<div class="label">Your Email</div>
		<div class="field">
		<cfinput type="text" id="email" name="email" style="width:200px; font-family:Verdana, Geneva, sans-serif; font-size:11px">
		</div>
	</div>
	
	<div class="formfield"><div class="label">&nbsp;</div>
		<div class="field"><input title="Retrieve My Login Details" class="button" type="submit" value="Send" name="submit" /></div>
	</div></div>	
</cfform>
</cfcase>
<cfcase value="helpdone">
<!--- ************************************************************ --->
<!--- Forget password email action                                 --->
<!--- ************************************************************ --->
	<cfif len(trim(form.email))>
			
		<cfquery name="gethelp" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			select unlockkey, bloguserid, blogusername, password, email, username from bloguser where 
			<cfif len(trim(form.email))>
				email = <cfqueryparam cfsqltype="cf_sql_varchar" value="#trim(form.email)#">
			</cfif>
			and retrycount <= 10 and
			valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
		</cfquery>
		<cfif gethelp.recordCount>
			<cfquery name="addunlockkey" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
				update bloguser set 
				unlockkey	= '#hash(CreateUUID())#'
				where bloguserid = #val(gethelp.bloguserid)# and unlockkey is null
			</cfquery>
			<cfquery name="getunlockkey" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
				select unlockkey from bloguser where bloguserid = #val(gethelp.bloguserid)#
			</cfquery>
			<ct:mail
				to			= "#gethelp.email#" 
				subject		= '(#Application.sti.sitename#) Your Login Details.'><cfoutput>
				Hello #gethelp.blogusername#,<br /><br />
				You want to reset your Teapot password? It's easy. Just <a href="#application.root#administrator/login.cfm?action=passwordchange&key=#getunlockkey.unlockkey#">click here</a> and follow the screen. Your password is case sensitive. <br /><br />
				Your User Name is : #gethelp.username#<br /><br />
				If the above link does not work, copy and paste following link into your browser:<br /><br />
				#application.root#administrator/login.cfm?action=passwordchange&key=#getunlockkey.unlockkey#
				</cfoutput>
			</ct:mail>
			<cfset session.msg = "Login Details sent to #gethelp.email#. Please check your email.">
			<cflocation addtoken="no" url="login.cfm">		
		<cfelse>
			<cfset session.error = "No User found! Have you entered the correct email or user name?">
			<cflocation addtoken="no" url="login.cfm?action=help">	
		</cfif>
	<cfelse>
		<cfset session.error = "Please Enter Email Address">
		<cflocation addtoken="no" url="login.cfm?action=help">
	</cfif>
</cfcase>

<cfcase value="passwordchange">
<!--- ************************************************************ --->
<!--- Password Change Form                                         --->
<!--- ************************************************************ --->
<cfquery name="chk" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select bloguserid from bloguser 
	where unlockkey	= <cfqueryparam cfsqltype="cf_sql_varchar" value="#trim(url.key)#"> and 
	valid 			= <cfqueryparam cfsqltype="cf_sql_bit" value="1">
</cfquery>
<cfif not chk.recordCount>
	<cfset session.error = "The password reset link is invalid or expired">
	<cflocation addtoken="no" url="login.cfm">
</cfif>

<div style="text-align:right"><a href="login.cfm"><img src="images/loginback.png" title="Back" style="float:right" align="absmiddle" border="0" /></a></div>
<div style="background-color:#CCC"><div style="width:330px; text-align:left; margin-left:auto; margin-right:auto;"><img src="images/teapot.gif" align="absmiddle" /><img src="images/newpassword.gif" title="Set New Password" align="absmiddle" /></div></div>
<div style="width:350px; padding:5px; text-align:left; margin-left:auto; margin-right:auto;">
<cfform action="login.cfm?action=passwordupdate&key=#url.key#" method="post" id="updateform">
	<div class="formfield">
	<div class="label">New Password</div>
		<div class="field">
		<cfinput type="password" id="Password" name="Password" required="yes" style="width:200px; font-family:Verdana, Geneva, sans-serif; font-size:11px" message="Please Enter New Password">
		</div>
	</div>
	<div class="formfield">
		<div class="label">New Password (again)</div>
		<div class="field">
		<cfinput type="password" id="Password1" name="Password1" required="yes" style="width:200px; font-family:Verdana, Geneva, sans-serif; font-size:11px" message="Please Enter New Password Again">
		</div>
	</div>
	<div class="formfield"><div class="label">&nbsp;</div>
		<div class="field"><input title="Click Here to Submit" class="button" type="submit" value="Update" name="submit" /></div>
	</div></div>
	<script type="text/javascript">
		$('#updateform').submit(function(){
			if ( $('#Password').val() !== $('#Password1').val() ) {
				alert("Password does not match. Please Type again.")
				$('#Password').val('')
				$('#Password1').val('')
				return false;
			}
		})
	</script>
</cfform>
</cfcase>
<!--- ************************************************************ --->
<!--- Password Update Action                                       --->
<!--- ************************************************************ --->
<cfcase value="passwordupdate">
	<cfif form.Password neq form.Password1>
		<cfset session.error = "Password does notmatch. Please Type password again.">
		<cflocation addtoken="no" url="login.cfm?action=passwordchange&key=#url.key#">
	</cfif>
	<ct:passwordenc password="#form.password#">
	<cfquery name="chk" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		update bloguser set
		salt	 		= <cfqueryparam cfsqltype="cf_sql_varchar" value="#salt#">,
		password		= <cfqueryparam cfsqltype="cf_sql_varchar" value="#passwordenc#">,
		unlockkey		= NULL,
		retrycount		= 0
		where unlockkey	= <cfqueryparam cfsqltype="cf_sql_varchar" value="#trim(url.key)#"> and 
		valid 			= <cfqueryparam cfsqltype="cf_sql_bit" value="1">
	</cfquery>
	<cfset session.msg	= "Password updated successfully">
	<cflocation addtoken="no" url="login.cfm">
</cfcase>
<cfdefaultcase>

<cfparam name="session.login.fail" default="0">
<cfif len(form.username) and len(form.password)>
<!--- ************************************************************ --->
<!--- Login Action                                                 --->
<!--- ************************************************************ --->
	<cfquery name="getsalt" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		select salt, retrycount, bloguserid from bloguser 
		where username 	= <cfqueryparam cfsqltype="cf_sql_varchar" value="#trim(form.username)#"> and 
		valid 			= <cfqueryparam cfsqltype="cf_sql_bit" value="1">
	</cfquery>
	
	<cfif session.login.fail lt 10>
		<ct:passwordenc password="#form.password#" salt="#getsalt.salt#">
		<cfquery name="chk" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			SELECT bloguserid as id, cookiekey, blogusername as name from bloguser
			where username 	= <cfqueryparam cfsqltype="cf_sql_varchar" value="#trim(form.username)#"> and 
			password 		= <cfqueryparam cfsqltype="cf_sql_varchar" value="#passwordenc#"> and 
			retrycount		<= 10 and
			valid 			= <cfqueryparam cfsqltype="cf_sql_bit" value="1">
		</cfquery>
	<cfelse>
		<cfset chk.recordcount = 0>
	</cfif>
	
	<cfif chk.recordcount>
		<cfset session.login.fail 	= 0>
		<cfquery name="retrycount" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			update bloguser set 
			retrycount	= 0
			where bloguserid = #val(chk.id)#
		</cfquery>
			
		<!--- use of cfheader to create cookies seems to solve few issues occured in IE --->
		<cfif YesNoFormat(form.keep)>
			<cfheader name="Set-Cookie" value="blogid=#Encrypt(chk.cookiekey, application.key, 'AES', 'hex')#;Expires=#DateFormat(dateadd('m',3,now()),'ddd, dd mmm yyyy')# 12:00:00 GMT;HTTPOnly">
			<cfheader name="Set-Cookie" value="name=#chk.name#;Expires=#DateFormat(dateadd('m',3,now()),'ddd, dd mmm yyyy')# 12:00:00 GMT;HTTPOnly">
		<cfelse>
			<cfset session.id	= chk.id>
			<cfheader name="Set-Cookie" value="blogid=#Encrypt(chk.cookiekey, application.key, 'AES', 'hex')#;HTTPOnly">
			<cfheader name="Set-Cookie" value="name=#chk.name#;HTTPOnly">
		</cfif>		
		<cfheader statuscode="302"	statustext="Object Temporarily Moved">
		<cfif len(url.r)>
			<cfheader name="location"	value="index.cfm?#Decrypt(url.r, application.key, 'AES', 'hex')#">
		<cfelse>
			<cfheader name="location"	value="index.cfm">
		</cfif>
		
	<cfelse>
		<cfquery name="retrycount" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			update bloguser set 
			retrycount = #val(getsalt.retrycount)# + 1
			where bloguserid = #val(getsalt.bloguserid)#
		</cfquery>
		<cfset session.login.fail = session.login.fail + 1>
		<cfif getsalt.retrycount gte 9>
			<!--- maximum retry reached for this account. lock it and create key --->
			<cfquery name="addunlockkey" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
				update bloguser set 
				unlockkey	= '#hash(CreateUUID())#'
				where bloguserid = #val(getsalt.bloguserid)# and unlockkey is null
			</cfquery>
			<cfquery name="getunlockkey" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
				select unlockkey,email,blogusername from bloguser 
				where bloguserid = #val(getsalt.bloguserid)#
			</cfquery>
			<!--- email the unlock key to the user --->
			<ct:mail
				to			= "#getunlockkey.email#" 
				subject		= '(#Application.sti.sitename#) Your Account is locked due to unsuccessful login attempts.'><cfoutput>
				Hello #getunlockkey.blogusername#,<br /><br />
				Your account is locked to prevent unauthorized access. Click on the link to unlock:<br />
				<a href="#application.root#administrator/login.cfm?action=unlock&key=#getunlockkey.unlockkey#">#listfirst(cgi.HTTP_REFERER,'?')#?action=unlock&key=#getunlockkey.unlockkey#</a>
				</cfoutput>
			</ct:mail>
			
			<cfset session.error 	= "Your Account is locked due to unsuccessful login attempts.">
			<cflocation addtoken="no" url="login.cfm?action=formunlock">
		<cfelse>
			<cfif val(session.login.fail) gt 10>
				<cfset session.error = "Maximum retry limit exceeded. Please try again later.">
			<cfelse>
				<cfset session.error = "Username or Password is wrong. [#session.login.fail# of 10 Retry]">
			</cfif>
			<cflocation addtoken="no" url="login.cfm?r=#url.r#">
		</cfif>
	</cfif>
<cfelse>
	<cfcookie name="blogid" expires="now">
</cfif>

<!--- ************************************************************ --->
<!--- Login Form                                                   --->
<!--- ************************************************************ --->
<div style="text-align:right"><a href="login.cfm?action=help"><img src="images/loginhelp.png" title="Help" style="float:right" align="absmiddle" border="0" /></a></div>
<div style="background-color:#CCC"><div style="width:330px; text-align:left; margin-left:auto; margin-right:auto;"><img src="images/teapot.gif" align="absmiddle" /><img class="hello" src="images/hello1.gif" align="absmiddle" /><img src="images/myblog.gif" align="absmiddle" /></div></div>
<div style="width:350px; padding:5px; text-align:left; margin-left:auto; margin-right:auto;">
<cfform action="login.cfm?r=#url.r#" method="post">
	<div class="formfield">
	<div class="label">User Name</div>
		<div class="field">
		<cfinput type="text" name="Username" value="#form.Username#" required="yes" style="width:200px; font-family:Verdana, Geneva, sans-serif; font-size:11px" message="Please Enter User name">
		</div>
	</div>
	
	<div class="formfield">
		<div class="label">Password</div>
		<div class="field">
		<cfinput type="password" name="Password" value="#form.Password#" required="yes" style="width:200px; font-family:Verdana, Geneva, sans-serif; font-size:11px" message="Please Enter Password">
		</div>
	</div>
	
	<div class="formfield">
		<div class="label"> &nbsp; </div>
		<div class="field">
		<input type="checkbox" name="keep" value="1" /> Keep me signed in
		</div>
	</div>
	
	<div class="formfield"><div class="label">&nbsp;</div>
		<div class="field"><input title="Click Here to Submit" class="button" type="submit" value="Login" name="submit" /></div>
	</div></div>
	
</cfform>
<script type="text/javascript">
$(function(){
var i = 2;
function sayhello() { setTimeout(function () {
	$('.hello').attr('src','images/hello'+i+'.gif');
	i++; if (i < 13) { sayhello(); } else {i = 1; sayhello(); }}, 2000);
}
sayhello();
})
</script>
</cfdefaultcase>
</cfswitch>

</div>
<div style="background:url(images/loginbgline.png); background-repeat:repeat-x; background-position:top"><img src="images/sdow_btm_lft.png" align="left" /><img src="images/sdow_btm_rite.png" align="right" /><div style="clear:both"></div></div>

</div>
</div>
</body>
</html>