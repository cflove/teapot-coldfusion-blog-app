<cfimport 
	taglib =	"../../system/form"  
	prefix =	"fm">
	
<!--- *********************************************** --->
<!--- Image Gallery                                   --->
<!--- *********************************************** --->
<cfdirectory
	action 		= "list"
	directory 	= "#application.wh#posts/#url.id#/igallery/"
	type 		= "file"
	name 		= "ifiles" />
<div class="formbar">
<div style="cursor:pointer" onclick="$('#imagegal').slideToggle()">Image Gallery <cfif ifiles.recordCount>(<cfoutput><span id="imgcount">#ifiles.recordCount#</span></cfoutput>) </cfif><img src="images/sort1.gif" /></div>
</div>
<div id="imagegal" style="display:none; padding-bottom:5px; padding-top:5px">
	<cfif ifiles.recordCount>
	<fm:label label="">
		<cfoutput query="ifiles">
			<cfset thumbname = "#listdeleteat(name,listlen(name,'.'),'.')#.jpg">
			<div id="imagebox#ifiles.currentRow#" class="imagebox" title="#ifiles.name#"><img src="../#application.warehouse#/posts/#url.id#/igallery/_thumb/#thumbname#" style="max-width:75px; max-height:75px; cursor:pointer;" />
			<div file="#Encrypt('posts/#url.id#/igallery/_thumb/#thumbname#,posts/#url.id#/igallery/#thumbname#', session.key, 'AES', 'hex')#" title="Delete Image" class="idelete">Delete</div></div>
		</cfoutput>
	</fm:label><div id="stabbox"></div>
	</cfif>

	<fm:label label="">
		<input type="file" class="text" name="ifile1" /> (image file)
		<div id="moreimg"><a href="javascript:moreimages()">more</a></div>
	</fm:label>
</div>
<input type="hidden" name="ifilecount" value="1" id="ifilecount" />

<div id="film"></div>
<div id="bigimg"><div class="imgholder"><div title="Close"></div></div></div>

<cfsavecontent variable="js">
<script type="text/javascript">
function moreimages() {
	$('#imagegal .labelblk:last').clone().appendTo('#imagegal');
	$('#imagegal #moreimg:first').remove();
	$('#imagegal input').each( function(i) { $(this).attr('name','ifile'+ (i+1) ) });
	$('#ifilecount').val( $('#imagegal input').length );
};

$(document).ready(function(){
	$('#imagegal .imagebox').mouseover(function(){
		$('#imagegal .imagebox').not(this).children('.idelete:visible').slideUp('fast')
		$(this).children('.idelete').slideDown('slow')
	})
	// image delete
	$('.idelete').click(function(){
		var answer = confirm("Are You sure You want to Delete this file?");
		if (answer){
			$(this).html('Deleting..')
			$.ajax({url: cfc + "?method=fileremove&returnformat=plain&files="+$(this).attr('file')+"&id="+$(this).parent().attr('id'), dataType: "text", cache:false, success: function(data){ 
				if ($('#'+$.trim(data)).length == 1) {$('#'+$.trim(data)).hide("slow").remove(); $('#imgcount').html( $('#imagegal .imagebox').length ) }
			}})
		};
	})
	// image preview		
	function imgsize() {
		var w = $('#bigimg').innerWidth() 
		var h = $('#bigimg').innerHeight() 
		$('#bigimgpre').css({'max-height':h-65,'max-width':w-25})
		$('.imgholder div:first').height(h- $('#bigimg #imgmenu').height() )
	}
	$('.imagebox img').click(function(){
		var img = $(this).parent().attr('title')
		$('#film').fadeTo('slow', 0.9)
		$('#bigimg').fadeIn("slow", function(){ 
			$('.imgholder div:first').html('<img id="bigimgpre" src="../<cfoutput>#application.warehouse#/posts/#url.id#</cfoutput>/igallery/'+img+'" />').css('cursor','pointer')
			imgsize()
		})	
	})
	$('#film,#bigimg,#imgholder').click(function(){
		$('#film').fadeOut('slow')
		$('#bigimg').fadeOut('slow')
		$('.imgholder div:first').html('')
	})
})
</script>
</cfsavecontent>
<cfhtmlhead text="#js#">