<!--- **************************************************** --->
<!--- code coloring                                        --->
<!--- **************************************************** --->
<cfset codeColoring_obj = CreateObject("component", "CodeColoring") />
<cfset stringOrginal 	= stringFixed>
<cfset stringFixed 		= ''>
<cfset count 			= 1>
<cfloop condition="len(stringOrginal) gt 1">
	<cfdump var="#stringOrginal#">
	<cfset codeStart1 	= FindNoCase("<cf:code",stringOrginal)>
	<cfset codestart	= findnocase('>',stringOrginal,codeStart1)+1>
	<cfset taglength	= codestart-codeStart1>
	<cfset codeEnd		= findnocase('</cf:code>',stringOrginal,codeStart)>
	<cfif val(codeStart) lt val(codeEnd)>
		<cfif val(codeStart-taglength-1)>
			<cfset stringFixed	= "#stringFixed##left(stringOrginal,codeStart-taglength-1)#">
		</cfif>
		<cfset codeblock	= mid(stringOrginal,codeStart, codeEnd-(codeStart))>
		<cfif len(trim(codeblock))>
			<cfset codeblock		= replace(codeblock,'#chr(13)#','','all')>
			<cfset codeblock 		= ReRePlaceNoCase(codeblock,'<p>|</p>|<p />|<br>|<br />','#chr(13)#','all')>
			<cfset codeblock		= ReReplaceNoCase(codeblock,'<a[^>]*>|</a[^>]*>','','all')>
			<cfset codeblock		= ReReplaceNoCase(codeblock,'&lt;','<','all')>
			<cfset codeblock		= ReReplaceNoCase(codeblock,'&gt;','>','all')>
			<cfset codeblock		= ReReplaceNoCase(codeblock,'&nbsp;',' ','all')>
			<cfset codeblock		= ReReplaceNoCase(codeblock,'&amp;','&','all')>
			<cfset codeblock		= ReReplaceNoCase(codeblock,"''",'""','all')> <!--- colorString replace '' in to a single ', replace them to "" --->
			<cfif listlen(trim(codeblock),chr(13)) gt 1>
				<cfset btn			= '<div class="toolclass"><img src="#application.root#images/codetoolleft.gif" align="absmiddle" style="float:left"><span class="hideline" onclick="copyview(''cb#count#'')">Show/Hide Line Numbers</span> . <span class="hideline" onclick="codepop(''cb#count#'')">Full Screen</span> . <span class="hideline" onclick="codecopy(''cb#count#'')">Plain</span><img src="#application.root#images/codetoolright.gif" align="absmiddle" style="float:right"></div>'>
			<cfelse>
				<cfset btn			= ''>
			</cfif>
			<cfset codeblock		= '<div id="cb#count#"><div class="codeblock">#codeColoring_obj.colorString(dataString=trim(codeblock), lineNumbers=true,useCaching=false,blockname="cb#count#")#</div>#btn#</div>'>
		<cfelse>
			<cfset codeblock		= ''>
		</cfif>
		<cfset stringFixed		= '#stringFixed##codeblock#'>
		<cfset stringOrginal	= right(stringOrginal,len(stringOrginal)-(codeEnd+9))>
	<cfelse>
		<cfset stringFixed	= '#stringFixed##stringOrginal#'>
		<cfbreak>
	</cfif>
	<cfset count = 1+count>
	<cfif count gt 1000>
		<!--- safety break --->
		<cfbreak>
	</cfif>
</cfloop>