<!--- **************************************************** --->
<!--- Embed Video and content from url                     --->
<!--- filemanager & simpleMedia plugins use this           --->
<!--- **************************************************** --->
<cfset media.loc.pos[1]	= 1>
<cfset count 			= 1>

<cfloop condition="val(media.loc.pos[1]) gt 0">
<cfif media.loc.pos[1] gt 1>
	<cfset media.loc.pos[1] = media.loc.pos[1]+1>
</cfif>
<cfset media.loc		= REFind('\[[^]]*\]',stringFixed, media.loc.pos[1]+1 ,'true')>

<cfif val(media.loc.pos[1])>
	<!--- ****************************** --->
	<!--- video sharing sites            --->
	<!--- ****************************** --->
	<cfset media.string	= ReReplace(trim(mid(stringFixed,media.loc.pos[1]+1,media.loc.len[1]-2)),'<[^>]*>','','all') >
	<cfif listlen(media.string,'/\') gt 1>
	<cfset media.site	= listgetat(media.string,2,'\/')>
	<cfset media.site	= ListDeleteAt( media.site , listlen(media.site,'.'), '.')>
	<cfset media.site	= ListLast(media.site,'.')>
	<cfswitch expression="#media.site#">
		<cfcase value="youtube">
			<cfset media.key = replacenocase(media.string,'v=','|')>
			<cfset media.key = listlast(media.key,'|')>
			<cfset media.key = listfirst(media.key,'&')>
			<cfset media.txt = '<iframe title="YouTube video player" width="480" height="390" src="http://www.youtube.com/embed/#trim(media.key)#" frameborder="0" allowfullscreen></iframe>'>
			<cfset stringFixed = replacenocase(stringFixed, mid(stringFixed,media.loc.pos[1],media.loc.len[1]) , media.txt )>
		</cfcase>
		<cfcase value="github">
			<cfset media.posi = listlen(media.string,'?/\')>
			<cfset media.txt = '<div class="gist"><script src="https://gist.github.com/#listgetat(media.string,media.posi-1,'?/\')#/#listlast(media.string,'?/\')#.js"></script></div>'>
			<cfset stringFixed = replacenocase(stringFixed, mid(stringFixed,media.loc.pos[1],media.loc.len[1]) , media.txt )>
		</cfcase>
		<cfcase value="vimeo">
			<cfset media.key = listlast(media.string,'?/\')>
			<cfset media.txt = '<iframe src="http://player.vimeo.com/video/#trim(media.key)#?title=0&amp;byline=0&amp;portrait=0&amp;color=4f4f4f" width="400" height="225" frameborder="0"></iframe>'>
			<cfset stringFixed = replacenocase(stringFixed, mid(stringFixed,media.loc.pos[1],media.loc.len[1]) , media.txt )>
		</cfcase>
		<cfcase value="dailymotion">
			<cfset media.key = listlast(media.string,'/\')>
			<cfset media.key = listfirst(media.key,'_')>
			<cfset media.txt = '<object width="480" height="270"><param name="movie" value="http://www.dailymotion.com/swf/video/#trim(media.key)#?theme=none"></param><param name="allowFullScreen" value="true"></param><param name="allowScriptAccess" value="always"></param><param name="wmode" value="transparent"></param><embed type="application/x-shockwave-flash" src="http://www.dailymotion.com/swf/video/#trim(media.key)#?theme=none" width="480" height="270" wmode="transparent" allowfullscreen="true" allowscriptaccess="always"></embed></object>'>
			<cfset stringFixed = replacenocase(stringFixed, mid(stringFixed,media.loc.pos[1],media.loc.len[1]) , media.txt )>
		</cfcase>
		<cfcase value="yahoo">
			<cfset media.key = replacenocase(media.string,'vid=','|')>
			<cfset media.key = listlast(media.key,'|')>
			<cfset media.key = listfirst(media.key,'&')>
			<cfset media.txt = '<object width="576" height="324"><param name="movie" value="http://d.yimg.com/nl/cbe/butterfinger/player.swf"></param><param name="flashVars" value="vid=#trim(media.key)#&browseCarouselUI=hide&shareUrl=http%3A//comedy.media.yahoo.com/%3Fv%3D#trim(media.key)#&repeat=0&"></param><param name="allowfullscreen" value="true"></param><param name="wmode" value="transparent"></param><embed width="576" height="324" allowFullScreen="true" src="http://d.yimg.com/nl/cbe/butterfinger/player.swf" type="application/x-shockwave-flash" flashvars="vid=#trim(media.key)#&browseCarouselUI=hide&shareUrl=http%3A//comedy.media.yahoo.com/%3Fv%3D#trim(media.key)#&repeat=0&"></embed></object>'>
			<cfset stringFixed = replacenocase(stringFixed, mid(stringFixed,media.loc.pos[1],media.loc.len[1]) , media.txt )>
		</cfcase>
		<cfcase value="metacafe">
			<cfset media.key = listgetat(media.string,4,'/\')>
			<cfset media.txt = '<embed flashVars="playerVars=showStats=yes|autoPlay=no|" src="http://www.metacafe.com/fplayer/#trim(media.key)#/mr_poppers_penguins_movie_trailer.swf" width="440" height="272" wmode="transparent" allowFullScreen="true" allowScriptAccess="always" name="Metacafe_#trim(media.key)#" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed>'>
			<cfset stringFixed = replacenocase(stringFixed, mid(stringFixed,media.loc.pos[1],media.loc.len[1]) , media.txt )>
		</cfcase>
		<cfdefaultcase>
			<!--- *********************************************** --->
			<!--- code and execute links from filemanager plugin  --->
			<!--- *********************************************** --->
			<cfswitch expression="#listfirst(media.string,':')#">
				<cfcase value="CODE">
					<cffile action="read" file="#application.wh##listlast(media.string,':')#" variable="media.file" charset="utf-8">
					<cfset media.file	= replacenocase(media.file,chr(10),'<br>','all')>
					<cfset stringFixed	= replacenocase(stringFixed, mid(stringFixed,media.loc.pos[1],media.loc.len[1]) , media.file )>
				</cfcase>
				<cfcase value="EXECUTE">
					<cffile action="read" file="#application.wh##listlast(media.string,':')#" variable="media.file" charset="utf-8">
					<cfset media.file 	= ReRePlaceNoCase(media.file,'<br>|<br />','&lt;br>','all')>
					<cfset media.file 	= ReRePlaceNoCase(media.file,'<p>|<p />','&lt;p>','all')>
					<cfset stringFixed	= replacenocase(stringFixed, mid(stringFixed,media.loc.pos[1],media.loc.len[1]) , media.file )>
				</cfcase>
				<cfdefaultcase>
				<!--- ******************************************* --->
				<!--- lets handle file types in here              --->
				<!--- ******************************************* --->
					<cfset media.ext	= listlast(media.string,'.')>
					<cfswitch expression="#media.ext#">
						<cfcase value="jpg,gif,png">
							<cfset media.txt = '<img src="#media.string#" class="innerimg" />'>
							<cfset stringFixed = replacenocase(stringFixed, mid(stringFixed,media.loc.pos[1],media.loc.len[1]) , media.txt )>
						</cfcase>
						<cfcase value="pdf">
							<cfset media.txt = '<iframe style="height:500px" src ="#media.string#" width="100%" class="innerimg"></iframe>'>
							<cfset stringFixed = replacenocase(stringFixed, mid(stringFixed,media.loc.pos[1],media.loc.len[1]) , media.txt )>
						</cfcase>
						<cfcase value="mp3">
							<cfset media.txt = ''>
							<cfif not StructKeyExists(variables,'audioPlayerID')>
								<!--- load javascript only one time --->
								<cfset audioPlayerID = 1>
								<cfset media.txt = "<script type='text/javascript' src='#application.root#js/audio-player/audio-player.js'></script><script type='text/javascript'>AudioPlayer.setup('#application.root#js/audio-player/player.swf', {width: 290});</script>">
								<cfset media.txt = '<cfhtmlhead text="#media.txt#">'>
							<cfelse>
								<cfset audioPlayerID = audioPlayerID+1>
							</cfif>
							<cfset media.txt = '#media.txt#<p id="audioplayer_#audioPlayerID#"></p><script type="text/javascript"> AudioPlayer.embed("audioplayer_#audioPlayerID#", {soundFile: "#media.string#"});</script>'>
							<cfset stringFixed = replacenocase(stringFixed, mid(stringFixed,media.loc.pos[1],media.loc.len[1]) , media.txt )>
						</cfcase>
					</cfswitch>
				</cfdefaultcase>
			</cfswitch>
		</cfdefaultcase>
	</cfswitch>

<cfelse>
	<cfbreak>
</cfif>

<cfset count = 1+count>
<cfif count gt 50>
	<!--- safety break --->
	<cfbreak>
</cfif>
</cfif>
</cfloop>