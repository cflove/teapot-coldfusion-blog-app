<!--- **************************************************** --->
<!--- handle execute code                                  --->
<!--- **************************************************** --->
<cfset stringOrginal 	= stringFixed>
<cfset stringFixed 		= ''>
<cfset count 			= 1>
<cfloop condition="len(stringOrginal) gt 1">
	<cfset codeStart1 	= FindNoCase("<cf:execute",stringOrginal)>
	<cfset codestart	= findnocase('>',stringOrginal,codeStart1)+1>
	<cfset taglength	= codestart-codeStart1>
	<cfset codeEnd		= findnocase('</cf:execute>',stringOrginal,codeStart)>
	
	<cfif val(codeStart) lt val(codeEnd)>
		<cfif val(codeStart-taglength)>
			<!--- add content before the execute tag --->
			<cfset stringFixed	= "#stringFixed##left(stringOrginal,codeStart-taglength-1)#">
		</cfif>
		<cfset codeblock	= mid(stringOrginal,codeStart, codeEnd-(codeStart))>
		<cfif len(trim(codeblock))>
			<cfset codeblock 		= ReRePlaceNoCase(codeblock,'<p>|</p>|<p />|<br>|<br />','#chr(13)#','all')>
			<cfset codeblock		= ReReplaceNoCase(codeblock,'&lt;','<','all')>
			<cfset codeblock		= ReReplaceNoCase(codeblock,'&gt;','>','all')>
			<cfset codeblock		= ReReplaceNoCase(codeblock,'&nbsp;',' ','all')>
			<cfset codeblock		= ReReplaceNoCase(codeblock,'&amp;','&','all')>
		<cfelse>
			<cfset codeblock		= ''>
		</cfif>
		<cfset stringFixed		= '#stringFixed##codeblock#<br />'>
		<cfset stringOrginal	= right(stringOrginal,len(stringOrginal)-(codeEnd+12))>
	<cfelse>
		<cfset stringFixed	= '#stringFixed##stringOrginal#'>
		<cfbreak>
	</cfif>
	<cfset count = 1+count>
	<cfif count gt 1000>
		<!--- safety break --->
		<cfbreak>
	</cfif>
</cfloop>