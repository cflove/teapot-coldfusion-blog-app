<!--- **************************************************** --->
<!--- Pin to Wall                                          --->
<!--- **************************************************** --->
<cfif not StructKeyExists(session,'id')><cfabort></cfif>

<cfquery name="local.pageM" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select blogpostid, blogpostname, created,url from blogpost 
	where valid 	= <cfqueryparam cfsqltype="cf_sql_bit" value="1"> and sticky = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
	order by orderby, blogpostname, mblogpostid
</cfquery>

<cfsavecontent variable="local.catsave"><cfif local.pageM.recordCount>
[cfprocessingdirective pageEncoding="utf-8">[cfinclude template="settings/Sticky.cfm"><ul class="list" id="StickyWidget"><li class="title">[cfoutput>#widget.title#[/cfoutput></li>
<cfoutput query="local.pageM"><li class="item"><a href="#application.root##DateFormat(created,'yyyy')#/#DateFormat(created,'mm')#/#local.pageM.url#.cfm">#blogpostname#</a></li></cfoutput></ul>
</cfif></cfsavecontent>

<cfset local.catsave = replace(local.catsave,'[cf','<cf','all')>
<cfset local.catsave = replace(local.catsave,'[/cf','</cf','all')>
<cffile
	action 		= "write"
	mode		= "#application.chomd#"
	file 		= "#application.wh#widgets/Sticky.cfm"
	output 		= "#trim(local.catsave)#"
	addNewLine 	= "No" 
	charset 	= "utf-8" >