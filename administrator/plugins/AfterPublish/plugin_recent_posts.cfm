<!--- **************************************************** --->
<!--- recent posts                                         --->
<!--- **************************************************** --->
<cfif not StructKeyExists(session,'id')><cfabort></cfif>

<cfinclude template="../../../#application.warehouse#/widgets/settings/Recent_Posts.cfm">
<cfquery name="toppost" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		<cfswitch expression="#application.dbtype#">
			<cfcase value="mssql">
				select top #widget.RecentPosts# blogpostid, blogpostname, created,url 
				from blogpost where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1"> order by blogpostid desc
			</cfcase>
			<cfdefaultcase>
				select blogpostid, blogpostname, created,url 
				from blogpost where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1"> order by blogpostid desc
				limit #widget.RecentPosts#
			</cfdefaultcase>
		</cfswitch>
</cfquery>
<cfsavecontent variable="catsave"><cfif toppost.recordCount>[cfprocessingdirective pageEncoding="utf-8">[cfinclude template="settings/Recent_Posts.cfm"><ul class="list" id="RecentPostWidget"><li class="title">[cfoutput>#widget.title#[/cfoutput></li><cfoutput query="toppost"><li class="item"><a href="#application.root##DateFormat(created,'yyyy')#/#DateFormat(created,'mm')#/#toppost.url#.cfm">#blogpostname#</a></li></cfoutput></ul></cfif></cfsavecontent>

<cfset catsave = replace(catsave,'[cf','<cf','all')>
<cfset catsave = replace(catsave,'[/cf','</cf','all')>
<cffile
	action 		= "write"
	mode		= "#application.chomd#"
	file 		= "#application.wh#widgets/Recent_Posts.cfm"
	output 		= "#trim(catsave)#"
	addNewLine 	= "No" 
	charset 	= "utf-8">