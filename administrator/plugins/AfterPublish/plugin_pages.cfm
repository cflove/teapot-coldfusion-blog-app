<!--- **************************************************** --->
<!--- Pages                                                --->
<!--- **************************************************** --->
<cfif not StructKeyExists(session,'id')><cfabort></cfif>

<cfquery name="local.pageM" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select blogpostid, blogpostname, created,url from blogpost 
	where valid 	= <cfqueryparam cfsqltype="cf_sql_bit" value="1"> and mblogpostid = <cfqueryparam cfsqltype="cf_sql_integer" value="0"> 
	and pagetype 	= <cfqueryparam cfsqltype="cf_sql_tinyint" value="2">
	order by orderby, blogpostname, mblogpostid
</cfquery>

<cfsavecontent variable="local.catsave">
<cfif local.pageM.recordCount>
[cfprocessingdirective pageEncoding="utf-8">[cfinclude template="settings/Pages.cfm"><ul class="list" id="PagesWidget"><li class="title">[cfoutput>#widget.title#[/cfoutput></li>
<cfoutput query="local.pageM"><li class="item"><a href="#application.root##DateFormat(created,'yyyy')#/#DateFormat(created,'mm')#/#local.pageM.url#.cfm">#blogpostname#</a>

<cfquery name="local.pageS" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select blogpostid, blogpostname, created,url from blogpost 
	where valid 	= <cfqueryparam cfsqltype="cf_sql_bit" value="1">
	and mblogpostid = #val(local.pageM.blogpostid)# 
	and pagetype 	= <cfqueryparam cfsqltype="cf_sql_tinyint" value="2">
	order by orderby, blogpostname, mblogpostid
</cfquery>

<cfif local.pageS.recordCount>
<ul class="subset">
<cfloop query="local.pageS"><li class="sub"><a href="#application.root##DateFormat(local.pageS.created,'yyyy')#/#DateFormat(local.pageS.created,'mm')#/#local.pageS.url#.cfm">#local.pageS.blogpostname#</a></li></cfloop>
</ul>
</cfif>
</li></cfoutput></ul></cfif>
</cfsavecontent>

<cfset local.catsave = replace(local.catsave,'[cf','<cf','all')>
<cfset local.catsave = replace(local.catsave,'[/cf','</cf','all')>
<cffile
	action 		= "write"
	mode		= "#application.chomd#"
	file 		= "#application.wh#widgets/Pages.cfm"
	output 		= "#trim(local.catsave)#"
	addNewLine 	= "No" 
	charset 	= "utf-8" >