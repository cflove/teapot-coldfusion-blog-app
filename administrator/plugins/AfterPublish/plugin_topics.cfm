<!--- **************************************************** --->
<!--- Keywords                                             --->
<!--- **************************************************** --->
<cfif not StructKeyExists(session,'id')><cfabort></cfif>

<cfquery name="q" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	SELECT distinct bloglabel.bloglabelid, bloglabel.bloglabelname
	FROM bloglabel INNER JOIN
	blogpost_bloglabel ON bloglabel.bloglabelid = blogpost_bloglabel.bloglabelid RIGHT OUTER JOIN
	blogpost ON blogpost_bloglabel.blogpostid = blogpost.blogpostid
	where bloglabelname <> '' 
	and bloglabel.valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1"> 
	and blogpost.valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1"> 
	order by bloglabel.bloglabelname
</cfquery>

<cfsavecontent variable="catsave">
<cfif q.recordCount>[cfprocessingdirective pageEncoding="utf-8">[cfinclude template="settings/Topics.cfm"><ul class="list" id="KeywordWidget"><li class="title">[cfoutput>#widget.title#[/cfoutput></li><cfoutput query="q"><li class="item"><a href="#application.root#index.cfm?key=#URLEncodedFormat(q.bloglabelname)#">#q.bloglabelname#</a></li></cfoutput></ul></cfif>
</cfsavecontent>

<cfset catsave = replace(catsave,'[cf','<cf','all')>
<cfset catsave = replace(catsave,'[/cf','</cf','all')>
<cffile
	action 		= "write"
	mode		= "#application.chomd#"
	file 		= "#application.wh#widgets/Topics.cfm"
	output 		= "#trim(catsave)#"
	addNewLine 	= "No" 
	charset 	= "utf-8">