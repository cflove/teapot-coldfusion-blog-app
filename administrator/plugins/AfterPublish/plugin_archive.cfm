<!--- **************************************************** --->
<!--- Archive                                              --->
<!--- **************************************************** --->
<cfif not StructKeyExists(session,'id')><cfabort></cfif>

<cfinclude template="../../../#application.warehouse#/widgets/settings/Archive.cfm">
<cfquery name="archlist" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	<cfswitch expression="#application.dbtype#">
		<cfcase value="mssql">
			SELECT top #val(widget.ArchiveMonths)# YEAR(publisheddate) AS 'Year', MONTH(publisheddate) AS 'Month', count(publisheddate) AS "posts"
			FROM  blogpost where MONTH(publisheddate) is not null and valid = 1
			GROUP BY YEAR( publisheddate), MONTH(publisheddate)
			ORDER BY YEAR(publisheddate) desc, MONTH(publisheddate) desc
		</cfcase>
		<cfcase value="mysql">
			SELECT YEAR(publisheddate) AS 'Year', MONTH(publisheddate) AS 'Month', count(publisheddate) AS "posts"
			FROM  blogpost where MONTH(publisheddate) is not null and valid = 1
			GROUP BY YEAR( publisheddate), MONTH(publisheddate)
			ORDER BY YEAR(publisheddate) desc, MONTH(publisheddate) desc
			LIMIT #val(widget.ArchiveMonths)#
		</cfcase>
		<cfcase value="PostgreSQL">
			SELECT extract('year' from publisheddate) as year, extract('month' from publisheddate) as month, count(publisheddate) AS "posts"
			FROM  blogpost where extract('month' from publisheddate) is not null and valid = 1
			GROUP BY extract('year' from publisheddate) , extract('month' from publisheddate)
			ORDER BY extract('year' from publisheddate) desc, extract('month' from publisheddate) desc
			LIMIT #val(widget.ArchiveMonths)#
		</cfcase>
	</cfswitch>
</cfquery>

<cfsavecontent variable="catsave"><cfif archlist.recordCount>[cfprocessingdirective pageEncoding="utf-8">[cfinclude template="settings/Archive.cfm"><ul class="list" id="ArchiveWidget"><li class="title">[cfoutput>#widget.title#[/cfoutput></li><cfoutput query="archlist"><li class="item"><a href="#application.root##archlist.Year#/#numberformat(archlist.Month,'09')#">#Year# #MonthAsString(Month)# (#posts#)</a></li></cfoutput></ul></cfif></cfsavecontent>

<cfset catsave = replace(catsave,'[cf','<cf','all')>
<cfset catsave = replace(catsave,'[/cf','</cf','all')>
<cffile
	action 		= "write"
	mode		= "#application.chomd#"
	file 		= "#application.wh#widgets/Archive.cfm"
	output 		= "#trim(catsave)#"
	addNewLine 	= "No" 
	charset 	= "utf-8" >