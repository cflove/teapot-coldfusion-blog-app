<cfif not StructKeyExists(session,'id')><cfabort></cfif>

<cfif listfirst(server.coldfusion.productversion) gte 8>
	<cfparam name="itypes" default="JPG,GIF,PNG,BMP">
<cfelse>
	<cfparam name="itypes" default="jpg">
</cfif>
<cfset this.path	= "#application.wh#posts/#url.id#/igallery/">

<cfloop from="1" to="#form.ifilecount#" index="if">
	<cfif len(form['ifile#if#'])>
		<!--- create a folder for image gallery    --->
		<cfif not DirectoryExists("#this.path#")>
			<cfdirectory action = "create" mode= "#application.chomd#" directory = "#this.path#">
			<cfdirectory action = "create" mode= "#application.chomd#" directory = "#this.path#_thumb">
		</cfif>
		<!--- upload file                         --->
		<cffile
			action 			= "upload"
			fileField 		= "form.ifile#if#"
			destination 	= "#this.path#" 
			mode			= "#application.chomd#"
			nameconflict	= "overwrite">
		<!--- find number of images in this folder --->
		<cfif not ListFindNoCase(itypes,file.clientFileExt)>
		<!--- if not image, delete --->
			<cfset session.error	= "Error Uploading file. The File You Upload is #ucase(clientFileExt)#. Please upload #ucase(itypes)# Files Only.">
			<cffile action = "delete" file = "#this.path##cffile.serverFile#">
		<cfelse>
			<cfset this.save 		= Replace(cffile.serverFile,' ','_','all')>
			<cfset this.thumbname 	= "#listdeleteat(this.save,listlen(this.save,'.'),'.')#.jpg">
			<!--- cf 8 and higher, user cfimage --->
			<cfif listfirst(server.coldfusion.productversion) gte 8>
				<cftry>
				<cf_cfimagetag this="#this#" serverFile="#cffile.serverFile#">
					<cfcatch></cfcatch>
				</cftry>
			<cfelse>
			<!--- older cf usse iedit           --->
				<cfset myImage = CreateObject("Component", "iEdit")>
				<cfset myImage.SelectImage("#this.path##cffile.serverFile#")>
				
				<cfif myImage.getWidth() gt 1280 OR myImage.getHeight() gt 720>
					<cfset myImage.ScaletoFit(1280,720)>
					<cfset myImage.output("#this.path##cffile.serverFile#", "jpg",100)>
				</cfif>
				
				<cfif myImage.getWidth() gt 150 OR myImage.getHeight() gt 150>
					<cfset myImage.ScaletoFit(150,150)>
					<cfset myImage.output("#this.path#_thumb/#lcase(this.thumbname)#", "jpg",100)>
				<cfelse>
					<cffile action="copy" destination="#this.path##this.thumbname#" source="#this.path##lcase(cffile.serverFile)#">
				</cfif>
				
				<!--- rename the main image --->
				<cfif Compare(cffile.serverFile,this.save)>
					<cffile
						action 		= "rename"
						source 		= "#this.path##cffile.serverFile#"
						destination = "#this.path##this.save#">
				</cfif>
			</cfif>
			
		</cfif>
	</cfif>
</cfloop>