<!---<cfthread action="run" name="scale#ttributes.this.thumbname#" all="#attributes.this#" imagename="#attributes.serverFile#">--->
<cfset all="#attributes.this#">
<cfset imagename="#attributes.serverFile#">
	<cfset myImage	= ImageRead('#all.path##imagename#')>
	<cfset imginfo	= ImageInfo(myImage)>

	<cfif imginfo.width gt 1280 and imginfo.height gt 720>
		<cfset ImageScaleToFit(myImage,1280,720)>
		<cfset ImageWrite(myImage,'#all.path##imagename#')>
	</cfif>
	
	<cfif imginfo.width gt 150 OR imginfo.height gt 150>
		<cfset ImageScaleToFit(myImage,150,150)>
		<cfset ImageWrite(myImage,'#all.path#_thumb/#lcase(all.thumbname)#')>
	<cfelse>
		<cffile action="copy" destination="#all.path##all.thumbname#" source="#all.path##lcase(imagename)#" mode="#application.chomd#">
	</cfif>

	<!--- rename the main image --->
	<cfif Compare(imagename,all.save)>
		<cffile
			action 		= "rename"
			source 		= "#all.path##imagename#"
			destination = "#all.path##all.save#">
	</cfif>
<!---</cfthread>--->