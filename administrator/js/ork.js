cfc = 'js/ork.cfc'
// jquery ///////////////////////////////
$("document").ready(function() {
	$('.alert').tipsy({fade: true, gravity: $.fn.tipsy.autoNS});
	$('.cmticonred').tipsy({fade: true, gravity: $.fn.tipsy.autoNS});
	//$(".date").datepicker();
	
	// modifiled input text
	$('.textbig').wrap('<div class="textbigdiv"></div>').before('<img src="images/inputlft.gif" align="absmiddle" />').after('<img src="images/inputrite.gif" align="absmiddle" />')
	$('textarea[maxlength]').on('keyup change', function() {
		var str = $(this).val();
		var mx  = parseInt($(this).attr('maxlength'));
		if (str.length > mx) {
			$(this).val(str.substr(0, mx));
			return false;
		}})
})

// confirmation box /////////////////
function conf(t,q,l) {
	var $dialog = $('<div></div>')
		.html(q)
		.dialog({resizable: false,autoOpen: false,show: 'blind',hide: 'clip',height:140,modal: true,title : t,
			buttons: {
				'Yes': function() {window.location = l},
				'No': function() {$(this).dialog('close');}
			}
		});
	$dialog.dialog('open');
}

/////////////////////////////////////
// error message                  ///
/////////////////////////////////////
function showmsg(i) {
	$('.alertboxmsg').html( '<img src="images/success.png" align="absmiddle" /> ' + i)
	$('#alertbox').slideDown('slow', function() { setTimeout('hidealertbox()', 5000) });
}

function showerror(i) {
	$('.alertboxmsg').html( '<img src="images/critical.png" align="absmiddle" /> ' + i)
	$('#alertbox').slideDown('slow', function() {});
}

function hidealertbox() {
	$('#alertbox').slideUp('slow', function() { });
}

/////////////////////////////////////
/// text field resize function  /////
/////////////////////////////////////
function textSize(i) {
	var l = $('#'+i).val().length
	if (l < 100) {
		if ( $('#'+i).attr('type') !== 'text' ) {
			$('#'+i).parent().html('<input type="text" class="text" onKeyUp="textSize(this.id)" value="'+$('#'+i).val()+'" name="'+i+'" id="'+i+'" />')	
		}
	}
	
	if (l < 5) {
		$('#'+i).css({'width':'60px','min-width':'60px'})
	} else if (l < 20) {
		$('#'+i).css({'width':'250px','min-width':'250px'})
	} else if (l < 100) {
		$('#'+i).css({'width':'500px','min-width':'500px'})
	} else {
		if ( $('#'+i).attr('type') == 'text' ) {
			$('#'+i).parent().html('<textarea class="text" style="width:99%; height:70px" name="'+i+'" id="'+i+'" onKeyUp="textSize(this.id)">'+$('#'+i).val()+'</textarea>')
		}
	}
}

////////////////////////////////////////////////////////////////////////////
/// popup        ///////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
function popUp(URL,w,h,id) {
	if (id == null){
		day = new Date();
		id = day.getTime();
	}
	if (h == null){h = 400}
	if (w == null){w = 600}
	eval("page" + id + " = window.open(URL + '&header=0', '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width="+w+",height="+h+"');");
}