<cfcomponent displayname ="ork" output ="no" extends="JSONUtil">
<cfprocessingdirective pageEncoding="utf-8">
<cfimport 
	taglib =	"../system/ct"  
	prefix =	"ct">
	
<cfif not StructKeyExists(session,'id')>
	<cfif StructKeyExists(cookie,'id')>
		<cfset session.id		= val(Decrypt(cookie.blogid, application.key, 'AES', 'hex'))>
	<cfelse>
		<cfabort>
	</cfif>
</cfif>

<!--- ************************************************************************************ --->
<!--- save Sub page display order and set main pages                                       --->
<!--- ************************************************************************************ --->
<cffunction name="subpageorder" access="remote" output="no">
	<cfargument name="pages" required="yes" default="">
	
	<cfset local.count = 1>
	<cfloop list="#arguments.pages#" index="local.i">
		<cfquery name="update" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			update blogpost set
			orderby		= #val(local.count)#,
			mblogpostID = #listlast(local.i,'|')# 
			where blogpostid = #listfirst(local.i,'|')# 
			and bloguser = #val(session.id)#
		</cfquery>
		<cfset local.count = 1+local.count>
	</cfloop>
	<cfinclude template="../plugins/AfterPublish/plugin_pages.cfm">
	<cfreturn 1>
</cffunction>

<!--- ************************************************************************************ --->
<!--- save Main page display order                                                         --->
<!--- ************************************************************************************ --->
<cffunction name="pageorder" access="remote" output="no">
	<cfargument name="pages" required="yes" default="">

	<cfset local.count = 1>
	<cfloop list="#arguments.pages#" index="local.i">
		<cfquery name="update" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			update blogpost set
			orderby				= #val(local.count)#
			where blogpostid 	= #val(local.i)# 
			and bloguser 		= #val(session.id)#
		</cfquery>
		<cfset local.count = 1+local.count>
	</cfloop>
	<cfinclude template="../plugins/AfterPublish/plugin_pages.cfm">
	<cfreturn arguments.pages>
</cffunction>

<!--- ************************************************************************************ --->
<!--- save Widget                                                                          --->
<!--- ************************************************************************************ --->
<cffunction name="widgetSave" access="remote" output="no">
	<cfargument name="side" 	required="yes" default="">
	<cfargument name="footer" 	required="yes" default="">

<cfsavecontent variable="pageContent">
[cfprocessingdirective pageEncoding="utf-8">
<div id="right"><cfoutput>
<cfloop list="#arguments.side#" index="i">
[cftry>[cfinclude template='widgets/#i#'>[cfcatch>[/cfcatch>[/cftry></cfloop></cfoutput>
</div>
</cfsavecontent>

<cfset pageContent = replace(pageContent,'[cf','<cf','all')>
<cfset pageContent = replace(pageContent,'[/cf','</cf','all')>

<cffile 
	action			="write"
	mode			= "#application.chomd#"  
	nameconflict	="overwrite" 
	charset			="utf-8"
	file			="#ExpandPath('../../#application.warehouse#/widget_list.cfm')#" 
	output			="#trim(pageContent)#">

<cfsavecontent variable="pageContent">
[cfprocessingdirective pageEncoding="utf-8">
<div id="footerbox"><cfoutput>
<cfloop list="#arguments.footer#" index="i">
[cftry>[cfinclude template='widgets/#i#'>[cfcatch>[/cfcatch>[/cftry></cfloop></cfoutput>
</div>
</cfsavecontent>

<cfset pageContent = replace(pageContent,'[cf','<cf','all')>
<cfset pageContent = replace(pageContent,'[/cf','</cf','all')>

<cffile 
	action			="write"
	mode			="#application.chomd#"  
	nameconflict	="overwrite" 
	charset			="utf-8"
	file			="#ExpandPath('../../#application.warehouse#/footer_widget_list.cfm')#" 
	output			="#trim(pageContent)#">
	
	<cfreturn arguments.side>
</cffunction>

<!--- ************************************************************************************ --->
<!--- update comment                                                                       --->
<!--- ************************************************************************************ --->
<cffunction name="editComment" access="remote" output="no">
	<cfargument name="commentid"	required="yes" 	type="string">
	<cfargument name="comments"		required="yes" 	type="string">
	
	<cfset arguments.comments = replacenocase(arguments.comments,chr(13),'<br />','all')>
	<cfset local.out 			= StructNew()>
	
	<cfif not IsXML('<xml>#arguments.comments#</xml>')>
		<cfset local.out.msg	 	= 'Updated Failed! Incorrect formatting found.'>
		<cfset local.out.i		 	= ''>
	<cfelse>
		<cfquery name="update" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			update comments set
			comments		= '#arguments.comments#'
			where commentid = <cfqueryparam cfsqltype="cf_sql_char" value="#arguments.commentid#">
		</cfquery>

		<cfset local.out.i		 	= arguments.commentid>
		<cfset local.out.msg	 	= 'Comment Updated'>
		<cfsavecontent variable="local.out.html"><cfoutput><cfif len(arguments.comments) gt 200>
			<span>#application.ork.fullLeft(arguments.comments,200)#...(<a href="##" class="showfull">full</a>)</span>
			<span style="display:none"><span class="fullcmt">#arguments.comments#</span> (<a href="##" class="showshot">short</a>)</span>
		<cfelse>
			<span class="fullcmt">#arguments.comments#</span>
		</cfif></cfoutput></cfsavecontent>
		
		<cfquery name="local.thispost" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			select blogpostid from comments where commentid = <cfqueryparam cfsqltype="cf_sql_char" value="#arguments.commentid#">
		</cfquery>
		<ct:act_collection id="#local.thispost.blogpostid#" makeCollection="No">
	</cfif>	
	<cfreturn serializeToJSON(local.out)>
</cffunction>

<!--- ************************************************************************************ --->
<!--- delete file in the warehouse folder                                                  --->
<!--- ************************************************************************************ --->
<cffunction name="fileremove" access="remote" output="no">
	<cfargument name="files"		required="yes" 	type="string">
	<cfargument name="inWarehouse"	required="No" 	type="string" default="Yes">
	<cfargument name="id"			required="No" 	type="string" default="">

	<cfset local.string = Decrypt(arguments.files,session.key, 'AES', 'hex')>
	<cfloop list="#local.string#" index="i">
		<cfif YesNoFormat(arguments.inWarehouse)>
			<cfset i = '#application.wh##i#'>
			<cfif FileExists(i)>
				<cffile action="delete" file="#i#">
			</cfif>
		</cfif>
	</cfloop>
	
	<cfreturn arguments.id>
</cffunction>

<!--- ************************************************************************************ --->
<!--- keywords                                                                             --->
<!--- ************************************************************************************ --->
<cffunction name="keywords" access="remote" output="No"> 
	<cfargument name="tag" required="No" type="string" default=""> 
	
	<cfquery name="local.get" datasource="#application.ds#" username="#application.un#" password="#application.pw#"> 	
		<cfswitch expression="#application.dbtype#">
			<cfcase value="mssql">
				SELECT top 5 bloglabelname FROM bloglabel 
				where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
				<cfif len(arguments.tag)> and bloglabelname like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#trim(arguments.tag)#%"> </cfif> order by bloglabelname
			</cfcase>
			<cfcase value="PostgreSQL">
				SELECT bloglabelname FROM bloglabel 
				where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
				<cfif len(arguments.tag)> and bloglabelname ~* '#trim(arguments.tag)#' </cfif> order by bloglabelname
				limit 5
			</cfcase>
			<cfcase value="mysql">
				SELECT bloglabelname FROM bloglabel 
				where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
				<cfif len(arguments.tag)> and bloglabelname like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#trim(arguments.tag)#%"> </cfif> order by bloglabelname
				limit 5
			</cfcase>
		</cfswitch>
	</cfquery> 
	
	<cfset local.return = ArrayNew(1)> 
	<cfoutput query="local.get"> 
	<cfset local.return[local.get.currentRow]['key'] = local.get.bloglabelname> 
	<cfset local.return[local.get.currentRow]['value'] = local.get.bloglabelname> 
	</cfoutput> 
	
	<cfreturn serializeToJSON(local.return)>
</cffunction> 
 
<!--- ************************************************************************************ --->
<!--- republish the enter site                                                             --->
<!--- ************************************************************************************ --->
<cffunction name="republish" access="remote" returntype="string" output="no">
	<cfset local.start = now()>
	<cfquery name="local.get" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		select blogpostid from blogpost where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
	</cfquery>
	<cfset pluginfolder = ExpandPath('../plugins/Formating')>
	<cfloop query="local.get">
		<cfset local.blogpostid	= local.get.blogpostid>
		<cfinclude template="../bin/blogpost/act_publish.cfm">
	</cfloop>
	<!--- **************************************************** --->
	<!--- :: Plugin Include :: After Publish                   --->
	<!--- **************************************************** --->
	<cfdirectory action="list" directory="#ExpandPath('../plugins/AfterPublish')#" name="plugins" filter="plugin_*.cfm">
	<cfloop query="plugins"><cfinclude template="../plugins/AfterPublish/#plugins.name#" /></cfloop>
	<ct:act_collection folder="#ExpandPath('../../')#">
	<cfreturn '<img src="images/success.png" align="absmiddle" /> Site Published Successfully. (#DateDiff("s",local.start,now())# seconds. #local.get.recordCount# Posts)'>
</cffunction>

<!--- ************************************************************************************ --->
<!--- rebuild the search index                                                             --->
<!--- ************************************************************************************ --->
<cffunction name="reindex" access="remote" returntype="string" output="no">
	<cfset local.start = now()>
	<ct:act_collection folder="#ExpandPath('../../')#">
	<cfreturn '<img src="images/success.png" align="absmiddle" /> Search Index Republished Successfully. (#DateDiff("s",local.start,now())# seconds.)'>
</cffunction>

<!--- ************************************************************************************ --->
<!--- submit site to search engines                                                        --->
<!--- ************************************************************************************ --->
<cffunction name="submitsite" access="remote" returntype="string" output="no">
	<cfset local.stat = ArrayNew(1)>
	<cftry>
	<cfhttp url="http://www.bing.com/webmaster/ping.aspx?sitemap=#(application.root)#sitemap.xml" 
		timeout		="10"
		resolveurl	="No"/>
		<cfif IsDefined('cfhttp.Statuscode') and listfirst(cfhttp.Statuscode,' ') eq 200>
			<cfset ArrayAppend(local.stat,'Bing : <img src="images/success.png" align="absmiddle" /> ')>
		<cfelse>
			<cfset ArrayAppend(local.stat,'Bing : Failed ')>
		</cfif>
		<cfcatch><cfset ArrayAppend(local.stat,'Bing : Error ')></cfcatch>
	</cftry>
	
	<cftry>
	<cfhttp url="http://www.google.com/webmasters/tools/ping?sitemap=#(application.root)#sitemap.xml" 
		timeout		="10"
		resolveurl	="No"/>
		<cfif IsDefined('cfhttp.Statuscode') and listfirst(cfhttp.Statuscode,' ') eq 200>
			<cfset ArrayAppend(local.stat,'Google : <img src="images/success.png" align="absmiddle" /> ')>
		<cfelse>
			<cfset ArrayAppend(local.stat,'Google : Failed ')>
		</cfif>
		<cfcatch><cfset ArrayAppend(local.stat,'Google : Error ')></cfcatch>
	</cftry>
	
	<cfreturn ArrayToList(local.stat,'<br/>')>
</cffunction>

<!--- ************************************************************************************ --->
<!--- get Title from URL                                                                   --->
<!--- ************************************************************************************ --->
<cffunction name="getTitle" access="remote" returntype="any" output="no">
	<cfargument name="u" 		required="yes" default="">
		<cfif isURL(trim(arguments.u))>
			<cftry>
			<cfhttp url="#trim(arguments.u)#" 
				timeout		="3"
				resolveurl	="No" 
				userAgent	="cflove.org Blog Application" />
				
				<cfif IsDefined('cfhttp.Filecontent')>
					<cfset local.title = GetHTMLTitle(cfhttp.Filecontent)>
				<cfelse>
					<cfset local.title = ''>
				</cfif>
				<cfcatch>
					<cfset local.title = ''>
				</cfcatch>
			</cftry>
		<cfelse>
			<cfset local.title = ''>
		</cfif>
	<cfreturn trim(local.title)>
</cffunction>

<!--- ************************************************************************************ --->
<!--- Helpers                                                                              --->
<!--- ************************************************************************************ --->
<cfscript>
/**
* Parses an HTML page and returns the title.
* 
* @param str      The HTML string to check. 
* @return Returns a string. 
* @author Raymond Camden (ray@camdenfamily.com) 
* @version 1, December 3, 2001 
*/
function GetHTMLTitle(str) {
    var matchStruct = reFindNoCase("<[[:space:]]*title[[:space:]]*>([^<]*)<[[:space:]]*/title[[:space:]]*>",str,1,1);
    if(arrayLen(matchStruct.len) lt 2) { return ""; }
    return Mid(str,matchStruct.pos[2],matchStruct.len[2]);    
}
/**
* A quick way to test if a string is a URL
* 
* @param stringToCheck      The string to check. 
* @return Returns a boolean. 
* @author Nathan Dintenfass (nathan@changemedia.com) 
* @version 1, November 22, 2001 
*/
function isURL(stringToCheck){
	return REFindNoCase("^(((https?:|ftp:|gopher:)\/\/))[-[:alnum:]\?%,\.\/&##!@:=\+~_]+[A-Za-z0-9\/]$",stringToCheck) NEQ 0;
}
function fncFileSize(size) {
	if ((size gte 1024) and (size lt 1048576)) {
		return round(size / 1024) & "Kb";
	} else if (size gte 1048576) {
		return decimalFormat(size/1048576) & "Mb";
	} else {
		return "#size# b";
	}
}
</cfscript>


</cfcomponent>