<cfcomponent displayname ="filemanager" output ="yes" extends="ork">
<cfprocessingdirective pageEncoding="utf-8">
	<cfif not StructKeyExists(session,'id')>
		<cfif StructKeyExists(cookie,'id')>
			<cfset session.id		= val(Decrypt(cookie.blogid, application.key, 'AES', 'hex'))>
		<cfelse>
			<cfabort>
		</cfif>
	</cfif>

<!--- ************************************************************************************ --->
<!--- File manager, get file list                                                          --->
<!--- ************************************************************************************ --->
<cffunction name="getfmfiles" access="remote" returntype="any" output="yes">
	<cfargument name="path" required="No" default="">
	
	<!--- ************************************************************ --->
	<!--- path first position is a friendly name, like [home]. ignore  --->
	<!--- ************************************************************ --->
	<cfset arguments.path	= "/#ListDeleteAt(arguments.path,1,'/\')#">
	<cfif listlen(arguments.path,'\/')>
		<cfset arguments.path	= "#arguments.path#/">
	</cfif>
	
	<!--- ************************************************************ --->
	<!--- Build the Three paths first keep handy                       --->
	<!--- ************************************************************ --->
	<cfset local.filepath	= "filemanager#arguments.path#">
	<cfset local.midpath	= "filemanager/_middle#arguments.path#">
	<cfset local.thumbpath	= "filemanager/_thumb#arguments.path#">
	
	<cfdirectory 
		directory	= "#application.wh##local.filepath#"
		action		= "list"
		sort		= "type asc, name asc"
		name		= "this.getall">

	<cfif not listlen(arguments.path,'/\')>
		<!--- remove two storage folders from the list --->
		<cfquery name="this.getall" dbtype="query">
			select * from this.getall where name not in ('_thumb','_middle','_recycle_bin')
		</cfquery>
	</cfif>
	
	<cfsavecontent variable="local.list">
	<!--- display folders                    --->
	<cfoutput query="this.getall">
	<cfswitch expression="#this.getall.type#">
		<cfcase value="dir">
			<div class="fmd">
				<cfdirectory directory = "#application.wh##local.filepath#/#this.getall.name#" action = "list" name = "local.thisdir">
				<cfif local.thisdir.recordCount>
					<img src="img/folder.gif">
				<cfelse>
					<img src="img/folder_empty.gif">
				</cfif>
				<span class="fmname">#this.getall.name#</span>
			</div>
		</cfcase>
	<!--- display files                     ---->
		<cfdefaultcase>
			<cfset local.thumb		= "No">
			<cfif FileExists('#application.wh##local.thumbpath#/#this.getall.name#')>
				<cfset local.thumb	= "Yes">
			</cfif>
			<cfset local.midle		= "No">
			<cfif FileExists('#application.wh##local.midpath#/#this.getall.name#')>
				<cfset local.midle	= "Yes">
			</cfif>
			<div class="fmi" size="#JSStringFormat(fncFileSize(this.getall.size))#">
			<cfif YesNoFormat(local.thumb)>
				<img wh="#JSStringFormat('#application.root##application.warehouse#/')#" class="fmimg" thumb="#local.thumb#" midle="#local.midle#" file="#local.filepath##URLEncodedFormat(this.getall.name)#" src="#application.root##application.warehouse#/#local.thumbpath##this.getall.name#">
			<cfelse>
				<cfset local.fileext = listlast(this.getall.name,'.')>
				<cfswitch expression="#local.fileext#">
					<!--- No Thumb :: send the full image               --->
					<cfcase value="jpg,jpeg,png,gif">
						<img wh="#JSStringFormat('#application.root##application.warehouse#/')#" class="fmimg" midle="#local.midle#" file="#local.filepath##URLEncodedFormat(this.getall.name)#" src="#application.root##application.warehouse#/#local.filepath##this.getall.name#">
					</cfcase>
					<!--- Not Images :: display icons base on file type --->
					<cfcase value="css,js,php,mp3,pdf,cfm,cfc,wav,zip,avi,mov,wmv,doc,docx,eps,flv,fla,ind,jsf,mpeg,ppt,pptx,proj,psd,pst,rar,txt,xls,xlsx">
						<img wh="#JSStringFormat('#application.root##application.warehouse#/')#" file="#local.filepath##URLEncodedFormat(this.getall.name)#" src="img/#lcase(local.fileext)#.png">
					</cfcase>
					<cfdefaultcase>
						<img wh="#JSStringFormat('#application.root##application.warehouse#/')#" file="#local.filepath##URLEncodedFormat(this.getall.name)#" src="img/un.png">
					</cfdefaultcase>
				</cfswitch>
			</cfif>
				<span class="fmname">#this.getall.name#</span>
			</div>
		</cfdefaultcase>
		</cfswitch>
	</cfoutput>
	</cfsavecontent>
	<!--- compress the string a bit --->
	<cfset local.list = reReplace( local.list, "[[:space:]]{2,}", " ", "all" )>
	<cfset local.list = replace( local.list, "> <", "><", "all" )>
	<!---serializeToJSON(local.out)--->
<cfreturn local.list>
</cffunction>

<!--- ************************************************************************************ --->
<!--- File manager, Delete File                                                            --->
<!--- ************************************************************************************ --->
<cffunction name="delfmfiles" access="remote" returntype="any">
	<cfargument name="path" required="No" default="">
	
	<!--- ************************************************************ --->
	<!--- path first position is a friendly name, like [home]. ignore  --->
	<!--- ************************************************************ --->
	<cfset arguments.path	= "/#ListDeleteAt(arguments.path,1,'/\')#">
	
	<cfset local.filepath	= "filemanager#arguments.path#">
	<cfset local.midpath	= "filemanager/_middle#arguments.path#">
	<cfset local.thumbpath	= "filemanager/_thumb#arguments.path#">

	<cfif FileExists('#application.wh##local.thumbpath#')>
		<cffile action="delete" file="#application.wh##local.thumbpath#">
	</cfif>
	<cfif FileExists('#application.wh##local.midpath#')>
		<cffile action="delete" file="#application.wh##local.midpath#">
	</cfif>
	<cfif FileExists('#application.wh##local.filepath#')>
		<cffile action="delete" file="#application.wh##local.filepath#">
	</cfif>
	
	<cfreturn '#application.wh##local.thumbpath#'>
</cffunction>

</cfcomponent>