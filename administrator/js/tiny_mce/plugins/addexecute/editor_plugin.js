(function () {
    tinymce.create("tinymce.plugins.addexecutePlugin", {
        init: function (ed,  cm, n, co) {
            ed.addCommand("addexecute", function () {
			if ( $( ed.selection.getNode() ).attr('class') !== 'execute') {
				if ($( ed.selection.getNode() ).attr('class') !== 'codeblock') {
					var str = ed.selection.getContent()
					if ( String(str).length > 7 ) {
						if( String(str).substring(0,3) == '<p>' ) {
							str = String(str).substring(3, String(str).length);
						}
					}
					str = str.replace(/<p>/g,"<br>")
					str = str.replace(/<\/p>/g,"")
					ed.selection.setContent( '<cf:execute id="execute" class="execute" title="Executable Code Block">'+str+'</cf:execute>');
				}
			} else {
				var string = $( ed.selection.getNode() ).html()
				$( ed.selection.getNode() ).replaceWith(string)
			}
            })
            ed.addButton("addexecute", {
                title: "Select Executable Code Block",
                cmd: "addexecute",
                image: cm + "/img/execute.gif"
            });
        },
        createControl: function (b, a) {
            return null
        },
        getInfo: function () {
            return {
                longname: "Insert Execute Tag for TeaPot Blog",
                author: "Saman W Jayasekara",
                authorurl: "http://cflove.org",
                infourl: "http://cflove.org",
                version: "1.0"
            }
        }
    });
    tinymce.PluginManager.add("addexecute", tinymce.plugins.addexecutePlugin)
})();