(function () {
	tinymce.PluginManager.requireLangPack("simplemedia");
	tinymce.create("tinymce.plugins.SimpleMediaPlugin", {
		init: function (a, b) {
			a.addCommand("mceSimpleMedia", function () {
				a.windowManager.open({
					file: b + "/dialog.htm",
					width: 400 + parseInt(a.getLang("simplemedia.delta_width", 0)),
					height: 120 + parseInt(a.getLang("simplemedia.delta_height", 0)),
					inline: 1
				}, {
					plugin_url: b
				})
			});
			a.addButton("simplemedia", {
				title: "simplemedia.desc",
				cmd: "mceSimpleMedia",
				image: b + "/img/simplemedia.gif"
			});
			a.onNodeChange.add(function (d, c, e) {
				c.setActive("simplemedia", e.nodeName == "")
			})
		},
		createControl: function (b, a) {
			return null
		},
		getInfo: function () {
			return {
				longname	: 'SimpleMedia plugin',
                author		: "Saman W Jayasekara",
                authorurl	: "http://cflove.org",
                infourl		: "http://cflove.org",
                version		: "1.0"
			}
		}
	});
	tinymce.PluginManager.add("simplemedia", tinymce.plugins.SimpleMediaPlugin)
})();