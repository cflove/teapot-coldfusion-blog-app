/**
 * $Id: editor_plugin_src.js 166 2007-01-05 10:31:50Z spocke $
 *
 * @author Moxiecode
 * @copyright Copyright  2004-2007, Moxiecode Systems AB, All rights reserved.
 */

tinyMCE.importPluginLanguagePack('dd');

var TinyMCE_dd_Plugin = {

	getInfo : function() {
		return {
			longname : 'd:d',
			author : 'Oliver Seidel',
			authorurl : 'http://www.deliciousdays.com',
			infourl : 'http://www.deliciousdays.com',
			version : "1.0"
		};
	},
	
	initInstance : function(inst) {
		inst.addShortcut('ctrl', 's', 'lang_dd_step_desc', 'mcedd_step');
		inst.addShortcut('ctrl', 'n', 'lang_dd_note_desc', 'mcedd_note');
	},

	getControlHTML : function(cn) {
		switch (cn) {
			case "dd_step":
				return tinyMCE.getButtonHTML(cn, 'lang_dd_step_desc', '{$pluginurl}/images/dd_step.gif', 'mcedd_step', true);
			case "dd_note":
				return tinyMCE.getButtonHTML(cn, 'lang_dd_note_desc', '{$pluginurl}/images/dd_note.gif', 'mcedd_note', true);
		}

		return "";
	},

	execCommand : function(editor_id, element, command, user_interface, value) {

		switch (command) {

			case "mcedd_note":
				var inst = tinyMCE.getInstanceById(editor_id);
				var focusElm = inst.getFocusElement();
				var selectedText = inst.selection.getSelectedText();

				html = '<p class="note"><span><strong>Note: </strong>'+selectedText+'</span></p>';
				tinyMCE.execInstanceCommand(editor_id, 'mceInsertContent', false, html);
				tinyMCE.selectedInstance.repaint();

				tinyMCE.triggerNodeChange(false);
				return true;
				
			case "mcedd_step":
				var anySelection = false;
				var inst = tinyMCE.getInstanceById(editor_id);
				var focusElm = inst.getFocusElement();
				var selectedText = inst.selection.getSelectedText();

				// Show UI/Popup
				if (user_interface) {

					var dd_step = new Array();
					dd_step['file'] = '../../plugins/dd/step.htm'; // Relative to theme
					dd_step['width'] = 380;
					dd_step['height'] = 109 + (tinyMCE.isNS7 ? 20 : 0) + (tinyMCE.isMSIE ? 15 : 0);
					tinyMCE.openWindow(dd_step, {editor_id : editor_id, inline : "yes"});

					// Let TinyMCE know that something was modified
					tinyMCE.triggerNodeChange(false);
				}
				return true;
		}

		return false;
	},

	handleNodeChange : function(editor_id, node, undo_index, undo_levels, visual_aid, any_selection) {

		if (node.nodeName == "P") {
			tinyMCE.switchClass(editor_id + '_dd_step', 'mceButtonNormal');
			return true;
		}
		tinyMCE.switchClass(editor_id + '_dd_step', 'mceButtonDisabled');
	},

	// Private plugin internal methods
	_someInternalFunction : function(a, b) {
		return 1;
	}
};

// Add the plugin class to the list of available TinyMCE plugins
tinyMCE.addPlugin("dd", TinyMCE_dd_Plugin);
