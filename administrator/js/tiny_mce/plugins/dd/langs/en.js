// UK lang variables

/* Remember to namespace the language parameters lang_<your plugin>_<some name> */

tinyMCE.addToLang('dd',{
step_desc : 'Add a class to a paragraph.',
note_desc : 'Insert a note (uses highlighted text).',
tab_header : 'Recipe Steps',
label : 'class name (e.g. step1): ',
cancel : 'Cancel',
update : 'Update',
insert : 'Insert'
});
