tinyMCE.addI18n('fr.youtube_dlg',{
    title : "Insérez une vidéo YouTube",
    instr : "Insérez l'identifiant YouTube",
    ytID : "ID",
    ytW : "Width",
    ytH : "Height",
    relvideo: "Inclure les vidéos similaires",
    hd:"Visionner en HD",
    yes:"Oui",
    no:"Non"
});