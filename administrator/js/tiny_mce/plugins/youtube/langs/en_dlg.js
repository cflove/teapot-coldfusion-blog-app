tinyMCE.addI18n('en.youtube_dlg',{
    title : "Insert a YouTube Video",
    instr : "Insert the YouTube video ID of the video you want to appear.",
    ytID : "ID",
    ytW : "Width",
    ytH : "Height",
    relvideo: "Include related videos",
    hd:"Watch in HD",
    yes:"Yes",
    no:"No"
});