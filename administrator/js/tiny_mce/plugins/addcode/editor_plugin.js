(function () {
    tinymce.create("tinymce.plugins.addcodePlugin", {
        init: function (ed,  cm, n, co) {
            ed.addCommand("addcode", function () {
			if ($( ed.selection.getNode() ).attr('class') !== 'codeblock') {
				if ($( ed.selection.getNode() ).attr('class') !== 'execute' ) {
					var str = ed.selection.getContent()
					if ( String(str).length > 7 ) {
						if( String(str).substring(0,3) == '<p>' ) {
							str = String(str).substring(3, String(str).length);
						}
					}
					str = str.replace(/<p>/g,"<br>")
					str = str.replace(/<\/p>/g,"")
					ed.selection.setContent( '<cf:code id="codeblock" class="codeblock" title="Code Block">'+str+'</cf:code>');
				}
			} else {
				var string = $( ed.selection.getNode() ).html()
				$( ed.selection.getNode() ).replaceWith(string)
			}
            });
            ed.addButton("addcode", {
                title: "Add Code Box",
                cmd: "addcode",
                image: cm + "/img/code.gif"
            });
        },
        createControl: function (b, a) {
            return null
        },
        getInfo: function () {
            return {
                longname: "Insert Code",
                author: "Saman W Jayasekara",
                authorurl: "http://cflove.org",
                infourl: "http://cflove.org",
                version: "1.0"
            }
        }
    });
    tinymce.PluginManager.add("addcode", tinymce.plugins.addcodePlugin)
})();