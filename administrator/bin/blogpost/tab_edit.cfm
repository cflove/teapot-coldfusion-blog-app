<div style="padding:10px">
<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">

<ct:tabbox
	tabs	= "Edit Post,All Posts,Draft,New Post">
	<cfswitch expression="#url.tab#">
    	<cfcase value="4">
			<cflocation addtoken="no" url="#application.ork.uf('switch=post&tab=3','id')#">
        </cfcase>
    	<cfcase value="3">
        	<cflocation addtoken="no" url="#application.ork.uf('switch=post&tab=2')#">
        </cfcase>
    	<cfcase value="2">
        	<cflocation addtoken="no" url="#application.ork.uf('switch=post&tab=1')#">
        </cfcase>
    	<cfdefaultcase>
        	<cfinclude template="edit_post.cfm">
        </cfdefaultcase>
    </cfswitch>
</ct:tabbox>
</div>