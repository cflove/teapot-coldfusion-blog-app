<cfprocessingdirective pageEncoding="utf-8"> 
<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">
<ct:quickCheck>

<cfparam name="form.mblogpostID" 	default="">
<cfparam name="form.sticky" 		default="">
<cfparam name="form.comment" 		default="0">

<cftransaction>

<!--- **************************************************** --->
<!--- create search engine friendly url first              --->
<!--- **************************************************** --->
<cfquery name="chk" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	SELECT url,publisheddate from blogpost where blogpostid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">
</cfquery>
<cfif len(trim(chk.url))>
	<cfset urlAlias = "#chk.url#">
	<!--- **************************************************** --->
	<!--- rename url                                           --->
	<!--- **************************************************** --->
	<cfif IsDefined('form.permalink') and len(form.permalink)>
		<!--- delete old file --->
		<cfif FileExists("#ExpandPath('../#dateformat(chk.publisheddate,'yyyy')#/#dateformat(chk.publisheddate,'mm')#')#/#trim(chk.url)#.cfm")>
			<cffile action="delete" file="#ExpandPath('../#dateformat(chk.publisheddate,'yyyy')#/#dateformat(chk.publisheddate,'mm')#')#/#trim(chk.url)#.cfm">
		</cfif>
		<!--- validate new url --->
		<cfset urlAlias = application.ork.urlf(form.permalink)>
		<cfquery name="chk" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			SELECT blogpostid from blogpost where url = '#urlAlias#' and blogpostid <> <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">
		</cfquery>
		<cfif chk.recordCount>
			<cfset urlAlias = "#urlAlias#_#dateformat(now(),'ddmmyyyy')#">
		</cfif>
	</cfif>
<cfelse>
	<cfset urlAlias = application.ork.urlf(form.blogpostname)>
	<cfquery name="chk" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		SELECT blogpostid from blogpost where url = '#urlAlias#' and blogpostid <> <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">
	</cfquery>
	<cfif chk.recordCount>
		<cfset urlAlias = "#urlAlias#_#dateformat(now(),'ddmmyyyy')#">
	</cfif>
</cfif>

<!--- **************************************************** --->
<!--- insert                                               --->
<!--- **************************************************** --->
<cfif not val(url.id)>
<cflock timeout="1" name="blogpostadd">
	<cfquery name="in" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		insert into blogpost
		(blogpostname,bloguser,url,pagetype,created) values
		(<cfqueryparam cfsqltype="cf_sql_varchar" value="#trim(form.blogpostname)#">,
		#session.id#,
		<cfqueryparam cfsqltype="cf_sql_varchar" value="#urlAlias#">,
		<cfqueryparam cfsqltype="cf_sql_tinyint" value="#val(url.pagetype)#">,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(now())#">)
		<cfswitch expression="#application.dbtype#">
			<cfcase value="mssql">
				select SCOPE_IDENTITY() as id
			</cfcase>
			<cfcase value="PostgreSQL">
				RETURNING blogpostid as id;
			</cfcase>
		</cfswitch>
	</cfquery>
	<cfswitch expression="#application.dbtype#">
		<cfcase value="mysql">
			<cfquery name="in" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
				SELECT LAST_INSERT_ID() as id
			</cfquery>
		</cfcase>
	</cfswitch>
	<cfset url.id = in.id>
</cflock>
</cfif>

<!--- **************************************************** --->
<!--- update                                               --->
<!--- **************************************************** --->

<cftry>
<cfif not len(trim(form.smalldes))>
	<!--- find the last para within 500 charators --->
	<cfset form.smalldes	= '#left(replace(replace(REReplaceNoCase(REReplaceNoCase(form.blog,"&lt;[^>]*&gt;","","ALL"),"<[^>]*>|\[[^>]*\]","","ALL"),"#chr(10)#"," ","all"),"#chr(13)#"," ","all"),500)#'>
	<cfset form.smalldes	= Reverse(form.smalldes)>
	<cfset paraEnd			= find(' .',form.smalldes)>
	<cfset form.smalldes	= Reverse(form.smalldes)>
	<cfset form.smalldes	= trim(left(form.smalldes,len(form.smalldes)-paraEnd))>
</cfif>
	<cfcatch></cfcatch>
</cftry>

<cfquery name="chkPublished" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select publisheddate from blogpost where blogpostid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">
</cfquery>

<cfquery name="update" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	update blogpost set 
		blogpostname 	= <cfqueryparam cfsqltype="cf_sql_varchar" 	value="#trim(form.blogpostname)#">,
		blog 			= <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#form.blog#">,
		bloguser 		= <cfqueryparam cfsqltype="cf_sql_integer" 	value="#session.id#">,
		demourl			= <cfqueryparam cfsqltype="cf_sql_varchar" 	value="#trim(form.demourl)#">,
		comment 		= <cfqueryparam cfsqltype="cf_sql_tinyint" 	value="#val(form.comment)#">,
		url				= <cfqueryparam cfsqltype="cf_sql_varchar" 	value="#urlAlias#">,
		smalldes		= <cfqueryparam cfsqltype="cf_sql_varchar" 	value="#trim(form.smalldes)#">,
		mblogpostID		= <cfqueryparam cfsqltype="cf_sql_integer" 	value="#val(form.mblogpostID)#">,
		sticky			= <cfqueryparam cfsqltype="cf_sql_bit" 		value="#val(form.sticky)#">,
		<cfif IsDefined('form.Draft') or IsDefined('form.Preview')>
			valid 			= <cfqueryparam cfsqltype="cf_sql_bit" value="0">,
		</cfif>
		<cfif IsDefined('form.Publish')>
			valid 			= <cfqueryparam cfsqltype="cf_sql_bit" value="1">,
			<cfif not IsDate(chkPublished.publisheddate)>
			publisheddate	= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(now())#">,
			</cfif>
		</cfif>
		lastupdated		= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(now())#">,
		updatedBy		= #session.id#
		where blogpostid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">
</cfquery>

</cftransaction>

<!--- **************************************************** --->
<!--- external links                                       --->
<!--- **************************************************** --->
<cfset this.ExtID	= ''>
<cfloop from="1" to="10" index="i">
	<cfif StructKeyExists(form,'exttitle#i#')>
		<cfset this.title 	= form['exttitle#i#']>
		<cfset this.url		= form['exturl#i#']>
		<cfif len(this.url)>
			<cfquery name="chk" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
				select externallinkid from externallink where externallinkurl = '#trim(this.url)#' and blogpostID = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#"> 
				and valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
			</cfquery>
			<cfif not chk.recordCount>
				<cfset externallinkid = CreateUUID()>
				<cfquery name="insert" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
					insert into externallink 
					(externallinkID,externallinkName, externallinkURL, blogpostID, updatedBy,valid,created ) values
					('#externallinkid#',<cfqueryparam cfsqltype="cf_sql_varchar" value="#trim(this.title)#">,'#trim(this.url)#',<cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">,
					#session.id#, <cfqueryparam cfsqltype="cf_sql_bit" value="1">,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(now())#"> )
				</cfquery>
				<cfset this.ExtID = ListAppend(this.ExtID,"'#externallinkid#'")>
			<cfelse>
				<cfset this.ExtID = ListAppend(this.ExtID,"'#chk.externallinkid#'")>
			</cfif>
		</cfif>
	</cfif>
</cfloop>
<cfif listlen(this.ExtID)>
	<cfquery name="del" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		delete from externallink where blogpostID = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#"> 
		and externallinkid not in ( #PreserveSingleQuotes(this.ExtID)# )
	</cfquery>
</cfif>
	
<!--- **************************************************** --->
<!--- Labels                                               --->
<!--- **************************************************** --->
<cfparam name="form.label" 		default="">
<cfparam name="form['label[]']" default="">
<cfif listlen( form['label[]'] )>
	<cfset form.label = form['label[]']> <!---- Adobe ColdFusion --->
</cfif>
<cfif IsArray(form.label)>
	<cfset form.label = ArrayToList(form.label)> <!---- Railo --->
</cfif>

<cfquery name="clr" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	delete from blogpost_bloglabel where blogpostid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">
</cfquery>

<cfloop list="#form.label#" index="i">
	<cfquery name="chk" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		select bloglabelid from bloglabel where bloglabelname = '#trim(i)#'
	</cfquery>

	<cfif not chk.recordCount>
		<cfset bloglabelid = CreateUUID()>
		<cfquery name="inlabel" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			insert into bloglabel 
			(bloglabelid,bloglabelname,bloguser, valid,created) values
			('#bloglabelid#','#trim(i)#',#session.id#, <cfqueryparam cfsqltype="cf_sql_bit" value="1">, <cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(now())#"> )
		</cfquery>
	<cfelse>
		<cfset bloglabelid = chk.bloglabelid>
	</cfif>
	<cfquery name="addlabel" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		insert into blogpost_bloglabel 
		(blogpostid,bloglabelid) values
		(<cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">,'#bloglabelid#')
	</cfquery>
</cfloop>
<!--- **************************************************** --->
<!--- Downloadable file upload                             --->
<!--- **************************************************** --->
<cfparam name="form.file" default="">
<cfif len(form.file)>
	<cfif not DirectoryExists("#application.wh#posts/#url.id#")>
		<cfdirectory
			action 		= "create"
			mode		= "#application.chomd#"
			directory 	= "#application.wh#posts/#url.id#">
	</cfif>
	<cffile
		action 			= "upload"
		fileField 		= "form.file"
		mode			= "#application.chomd#"
		destination 	= "#application.wh#posts/#url.id#"	
		nameConflict 	= "overwrite">
</cfif>

<cfquery name="thispost" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select created,url from blogpost where blogpostid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">
</cfquery>

<!--- **************************************************** --->
<!--- :: Plugin Include :: Before Publish                  --->
<!--- **************************************************** --->
<cfdirectory action="list" directory="#ExpandPath('./plugins/BeforePublish')#" name="plugins" filter="plugin_*.cfm">
<cfloop query="plugins"><cftry><cfinclude template="../../plugins/BeforePublish/#plugins.name#" /><cfcatch></cfcatch></cftry></cfloop>

<cfif IsDefined('form.Draft') or IsDefined('form.Preview')>
<!--- **************************************************** --->
<!--- DRAFT. remove the published file from the server     --->
<!--- **************************************************** --->
	<cfif FileExists("#ExpandPath('../#dateformat(thispost.created,'yyyy')#/#dateformat(thispost.created,'mm')#')#/#trim(thispost.url)#.cfm")>
		<cffile action="delete" file="#ExpandPath('../#dateformat(thispost.created,'yyyy')#/#dateformat(thispost.created,'mm')#')#/#trim(thispost.url)#.cfm">
	</cfif>
</cfif>

<!--- **************************************************** --->
<!--- publish the file                                     --->
<!--- **************************************************** --->
<cfset local.blogpostid = url.id>

<cfif StructKeyExists(form,'Publish')>
	<cfinclude template="act_publish.cfm">
	<ct:act_collection id="#url.id#">
	<!--- **************************************************** --->
	<!--- :: Plugin Include :: After Publish                   --->
	<!--- **************************************************** --->
	<cfdirectory action="list" directory="#ExpandPath('./plugins/AfterPublish')#" name="plugins" filter="plugin_*.cfm">
	<cfloop query="plugins"><cftry><cfinclude template="../../plugins/AfterPublish/#plugins.name#" /><cfcatch></cfcatch></cftry></cfloop>
</cfif>

<cfif StructKeyExists(form,'Preview')>
	<cfset local.preview	= "Yes">
	<cfinclude template="act_publish.cfm">
	<cfset session.openPreview = "Yes">
<cfelse>
	<!--- delete temporary preview files --->
	<cfif FileExists("#application.wh#preview/#trim(thispost.url)#.cfm")>
		<cffile action="delete" file="#application.wh#preview/#trim(thispost.url)#.cfm">
	</cfif>
</cfif>
<cfset session.msg = "Post Saved">