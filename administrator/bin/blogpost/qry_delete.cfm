<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">
<ct:quickCheck>
<!--- **************************************************** --->
<!--- delete record, set valid off                         --->
<!--- **************************************************** --->
<cfquery name="del" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	delete from blogpost where blogpostid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">
</cfquery>

<cfquery name="del" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	delete from comments where blogpostid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">
</cfquery>

<!--- **************************************************** --->
<!--- delete files                                         --->
<!--- **************************************************** --->
<cfif yesnoformat(Application.sti.deletefile) and DirectoryExists("#application.wh#posts/#url.id#")>
	<cfif listfirst(server.coldfusion.productversion) gte 7>
		<cfdirectory action="delete" directory="#application.wh#posts/#url.id#" recurse="yes">
	<cfelse>
		<cfset deleteDirectory("#application.wh#posts/#url.id#")>
		<!---
		 Recursively delete a directory.
		 
		 @param directory 	 The directory to delete. (Required)
		 @param recurse 	 Whether or not the UDF should recurse. Defaults to false. (Optional)
		 @return Return a boolean. 
		 @author Rick Root (&#114;&#105;&#99;&#107;&#64;&#119;&#101;&#98;&#119;&#111;&#114;&#107;&#115;&#108;&#108;&#99;&#46;&#99;&#111;&#109;) 
		 @version 1, July 28, 2005 
		--->
		<cffunction name="deleteDirectory" returntype="boolean" output="false">
			<cfargument name="directory" type="string" required="yes" >
			<cfargument name="recurse" type="boolean" required="no" default="true">
			
			<cfset var myDirectory = "">
			<cfset var count = 0>
		
			<cfif right(arguments.directory, 1) is not "/">
				<cfset arguments.directory = arguments.directory & "/">
			</cfif>
			
			<cfdirectory action="list" directory="#arguments.directory#" name="myDirectory">
		
			<cfloop query="myDirectory">
				<cfif myDirectory.name is not "." AND myDirectory.name is not "..">
					<cfset count = count + 1>
					<cfswitch expression="#myDirectory.type#">
					
						<cfcase value="dir">
							<!--- If recurse is on, move down to next level --->
							<cfif arguments.recurse>
								<cfset deleteDirectory(
									arguments.directory & myDirectory.name,
									arguments.recurse )>
							</cfif>
						</cfcase>
						
						<cfcase value="file">
							<!--- delete file --->
							<cfif arguments.recurse>
								<cffile action="delete" file="#arguments.directory##myDirectory.name#">
							</cfif>
						</cfcase>			
					</cfswitch>
				</cfif>
			</cfloop>
			<cfif count is 0 or arguments.recurse>
				<cfdirectory action="delete" directory="#arguments.directory#">
			</cfif>
			<cfreturn true>
		</cffunction>
	</cfif>
</cfif>

<ct:act_collection id="#url.id#" action="delete">
<!--- **************************************************** --->
<!--- :: Plugin Include :: After Publish                   --->
<!--- **************************************************** --->
<cfdirectory action="list" directory="#ExpandPath('./plugins/AfterPublish')#" name="plugins" filter="plugin_*.cfm">
<cfloop query="plugins"><cfinclude template="../../plugins/AfterPublish/#plugins.name#" /></cfloop>

<cfset session.msg = "Removed Successfully.">
<cflocation addtoken="no" url="#application.ork.uf('switch=post','id,tab,page,dir')#">
