<div style="padding:10px">
<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">

<ct:tabbox
	tabs	= "All Posts,Draft,New Post">
	<cfswitch expression="#url.tab#">
    	<cfcase value="3">
			<cfset url.id = 0>
        	<cfinclude template="edit_post.cfm">
        </cfcase>
    	<cfcase value="2">
        	<cfinclude template="list_post.cfm">
        </cfcase>
    	<cfdefaultcase>
        	<cfinclude template="list_post.cfm">
        </cfdefaultcase>
    </cfswitch>
</ct:tabbox>
<br /><br />
</div>