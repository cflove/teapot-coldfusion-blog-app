<cfimport 
	taglib =	"../../system/form"  
	prefix =	"fm">

<cfquery name="get" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	SELECT LinksName, Link, LinksId FROM links where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
	ORDER BY DisplayOrder, created
</cfquery>

<cfform action="#application.ork.uf('switch=savesort&id=#url.id#')#" method="post" class="formfield">

<div style="padding-left:10px">Drag and Drop to set link display order</div>

<div id="sortable" style="margin:10px;">
<cfoutput query="get">
	<div style="border:1px solid ##CCC; cursor:pointer; width:400px; background-color:##EBEBEB; padding:5px; margin:1px" title="#Link#">
	<img src="images/move.png" align="absmiddle" /> #LinksName# <input type="hidden" name="sort" value="#LinksId#">
	</div></cfoutput>
</div>

<fm:label>
<input title="Save" class="buttonblu" type="submit" value="Save" name="Save" />
</fm:label>

</cfform>

<SCRIPT type=text/javascript>
$("document").ready(function() {
	$(function() {
		$("#sortable").sortable();
		$("#sortable").disableSelection();
	});
})
</SCRIPT>
