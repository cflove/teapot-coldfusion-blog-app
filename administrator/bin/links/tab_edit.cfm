<div style="padding:10px">
<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">

<cfquery name="cnt" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select count(LinksId)as cnt from links where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
</cfquery>

<cfif val(cnt.cnt) gt 1>
	<cfset tabs = 'Edit Link,My Links,New Link,Sort Links'>
<cfelse>
	<cfset tabs = 'Edit Link,My Links,New Link'>
</cfif>

<ct:tabbox
	tabs	= "#tabs#">
	<cfswitch expression="#url.tab#">
    	<cfcase value="4">
        	<cflocation addtoken="no" url="#application.ork.uf('switch=home&tab=3')#">
        </cfcase>
    	<cfcase value="3">
        	<cflocation addtoken="no" url="#application.ork.uf('switch=home&tab=2')#">
        </cfcase>
    	<cfcase value="2">
        	<cflocation addtoken="no" url="#application.ork.uf('switch=home&tab=1')#">
        </cfcase>
    	<cfdefaultcase>
        	<cfinclude template="edit_links.cfm">
        </cfdefaultcase>
    </cfswitch>
</ct:tabbox>
<br /><br />
</div>