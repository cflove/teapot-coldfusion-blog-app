<div style="padding:10px">
<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">

<cfquery name="cnt" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select count(LinksId)as cnt from links where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
</cfquery>

<cfif val(cnt.cnt) gt 1>
	<cfset tabs = 'All Links,Add Link,Sort Links'>
<cfelse>
	<cfset tabs = 'All Links,Add Link'>
</cfif>

<ct:tabbox
	tabs	= "#tabs#">
	<cfswitch expression="#url.tab#">
		<cfcase value="3">
			<cfinclude template="dsp_sort.cfm">
		</cfcase>
    	<cfcase value="2">
			<cfset url.id = ''>
        	<cfinclude template="edit_links.cfm">
        </cfcase>
    	<cfdefaultcase>
        	<cfinclude template="list_links.cfm">
        </cfdefaultcase>
    </cfswitch>
</ct:tabbox>
<br /><br />
</div>
