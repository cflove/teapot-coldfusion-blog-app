<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">
<ct:quickCheck>

<cfparam name="form.sort" default="">

<cfset count = 1>
<cfloop list="#form.sort#" index="i">
	<cfquery name="update" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		update links set
		DisplayOrder = #count#
		where LinksId = '#i#'
	</cfquery>
	<cfset count = 1+count>
</cfloop>

<cfset session.msg = "Link Display Order Updated">