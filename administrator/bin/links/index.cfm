<cfprocessingdirective pageEncoding="utf-8">

<cfswitch expression="#url.switch#">
	<cfcase value="savesort">
		<cfinclude template="qry_savesort.cfm">
		<cfinclude template="act_makelink.cfm">
		<cflocation addtoken="no" url="#application.ork.uf('switch=home')#">
	</cfcase>
	<cfcase value="delete">
		<cfinclude template="qry_delete.cfm">
		<cfinclude template="act_makelink.cfm">
		<cflocation addtoken="no" url="#application.ork.uf('switch=home')#">
	</cfcase>
	<cfcase value="add">
		<cfinclude template="qry_add.cfm">
		<cfinclude template="act_makelink.cfm">
		<cflocation addtoken="no" url="#application.ork.uf('switch=edit&id=#url.id#')#">
	</cfcase>
	<cfcase value="edit">
		<cfinclude template="tab_edit.cfm">
	</cfcase>
	<cfdefaultcase>
		<cfinclude template="tab_links.cfm">
	</cfdefaultcase>
</cfswitch>
