<!--- **************************************************** --->
<!--- save Link Include Page                               --->
<!--- **************************************************** --->
<cfquery name="toppost" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select LinksName, Link, note from links where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
	order by DisplayOrder, LinksName
</cfquery>
<cfsavecontent variable="catsave"><cfif toppost.recordCount>[cfprocessingdirective pageEncoding="utf-8">[cfinclude template="settings/Links.cfm"><ul class="list" id="LinkWidget"><li class="title">[cfoutput>#widget.title#[/cfoutput></li><cfoutput query="toppost"><li class="item"><a href="#Link#" title="#note#">#LinksName#</a></li></cfoutput></ul></cfif></cfsavecontent>

<cfset catsave = replace(catsave,'[cf','<cf','all')>
<cfset catsave = replace(catsave,'[/cf','</cf','all')>
<cffile
	action 		= "write"
	mode		= "#application.chomd#"
	file 		= "#application.wh#widgets/Links.cfm"
	output 		= "#trim(catsave)#"
	addNewLine 	= "No" 
	charset 	= "utf-8" >