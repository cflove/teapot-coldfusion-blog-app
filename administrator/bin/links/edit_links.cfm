<cfprocessingdirective pageEncoding="utf-8">

<cfinclude template="qry_get.cfm">
<cfimport 
	taglib =	"../../system/form"  
	prefix =	"fm">

<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">

<cfform action="#application.ork.uf('switch=add&id=#url.id#')#" method="post" class="formfield" enctype="multipart/form-data">

<cfif not len(thisq.Link)>
	<cfset Link = 'http://'>
<cfelse>
	<cfset Link = thisq.Link>
</cfif>
<fm:label label="Web Address">
<cfinput 
	name		= "Link"
	Id			= "Link"
	type		= "text" 
	onChange	= "getTitle()"
	required	= "yes" 
	message		= "Please Enter Link"
	class		= "text" 
	style		= "width:350px"
	maxlength	= "500"
	value		= "#Link#" /> (Example: http://cflove.org)
</fm:label>

<fm:label label="Name">
<cfinput 
	name		= "LinksName"
	id			= "LinksName"
	type		= "text" 
	required	= "yes" 
	onFocus		= "getTitle()"
	onChange	= "setStat()" 
	onKeyUp		= "setStat()"
	message		= "Please Enter Link Name"
	class		= "text" 
	style		= "width:350px"
	maxlength	= "50"
	value		= "#thisq.LinksName#" /> <span id="stat" title="(Example: cflove SimpleBlog)">(Example: Teapot Blog)</span>
</fm:label>

<fm:label label="Note">
<cfinput 
	name		= "note"
	id			= "note" 
	type		= "text" 
	class		= "text" 
	style		= "width:350px"
	maxlength	= "100"
	value		= "#thisq.note#" />
</fm:label>

<cfif val(url.id)>
	<cfset delete = "<a href=""javascript:conf('Delete Post','Are you sure you want to delete this Link?','#application.ork.uf('switch=delete&id=#url.id#')#')"">Delete</a>">
<cfelse>
	<cfset delete = "">
</cfif>

<fm:label label="#delete#">
<input title="Publish" class="buttonblu" type="submit" value="Save" name="Save" />
</fm:label>
</cfform>
</div>

<script type="text/javascript">
	function getTitle() {
		if ( $('#Link').val() !== '' && $('#LinksName').val() == '' ) {
			$('.formfield #stat').html('<img src="images/loading.gif" align="absmiddle" /> Fetching Title')
			$.ajax({url : cfc + "?method=getTitle&returnformat=plain&u=" + $('#Link').val(), dataType: 'html',cache:false, success: function(data) { 
				$('#LinksName').val($.trim(data))
				setStat()
			}})
		}
	}

	
	function setStat() {
		if ($('#LinksName').val() !== '') {
			$('.formfield #stat').html( $('.formfield #stat').attr('title') )
		}
	}
</script>