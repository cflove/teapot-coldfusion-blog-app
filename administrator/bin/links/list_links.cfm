<cfquery name="master" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	SELECT linksID from links where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
	<cfif len(url.srch)>
		and (LinksName like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#trim(url.srch)#%"> or Link like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#trim(url.srch)#%">)
	</cfif>
	<cfif len(url.order)>
		order by #Decrypt(url.order, application.key, 'AES', 'hex')# 
		<cfswitch expression="#url.dir#"><cfcase value="1">desc</cfcase><cfdefaultcase>asc</cfdefaultcase></cfswitch>
	<cfelse>
		ORDER BY DisplayOrder, created
	</cfif>
</cfquery>

<cfset searchlist	= "#QuotedValueList(master.linksID)#">
<cfinclude template="../../system/library/grid_calc.cfm">

<cfquery name="get" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	SELECT LinksName, Link, DisplayOrder, note, LinksId
	FROM links
	where <cfif listlen(searchlist)>linksID in (#PreserveSingleQuotes(searchlist)#)<cfelse>linksID = ''</cfif>
	<cfif len(url.order)>
		order by #Decrypt(url.order, application.key, 'AES', 'hex')# 
		<cfswitch expression="#url.dir#"><cfcase value="1">desc</cfcase><cfdefaultcase>asc</cfdefaultcase></cfswitch>
	<cfelse>
		ORDER BY DisplayOrder, created
	</cfif>
</cfquery>

<cfset titlelist = "Name|LinksName,Link|link">
<cfinclude template="../../system/library/grid_header.cfm">
<cfoutput query="get">
<tr <cfif not currentrow mod 2>class="zibrablk"<cfelse>class="zibrawhite"</cfif>>
	<td><a href="#application.ork.uf('switch=edit&id=#LinksId#')#">#LinksName#</a>
	<cfif len(trim(note))><br />#note#</cfif>
	</td><td><a href="#Link#" target="_blank"><img src="images/link.png" border="0" align="middle" /></a> #Link#</td>
</tr>
</cfoutput>
</table>
<cfinclude template="../../system/library/grid_navi.cfm">
</div>