<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">
<ct:quickCheck>
<cfprocessingdirective pageEncoding="utf-8">

<cftransaction>
<cfif not len(url.id)>
	<cfset url.id = CreateUUID()>
	<cfquery name="in" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		insert into links
		(LinksId,LinksName,Link,created) values
		(<cfqueryparam cfsqltype="cf_sql_char" value="#url.id#">,
		'#trim(form.LinksName)#','#trim(form.Link)#',<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(now())#">)
	</cfquery>
</cfif>

<cfquery name="update" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	update links set 
	LinksName		= '#trim(form.LinksName)#',
	Link			= '#trim(form.Link)#',
	lastupdated		= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(now())#">,
	note			= '#trim(form.note)#',
	updatedBy		= #session.id#
	where LinksId 	= <cfqueryparam cfsqltype="cf_sql_char" value="#url.id#">
</cfquery>

</cftransaction>
<cfset session.msg = "Link Saved">