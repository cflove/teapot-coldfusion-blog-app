<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">
<cfprocessingdirective pageEncoding="utf-8">
<ct:quickCheck>

<cffile action="read" file="#settingfile#" variable="set" charset = "utf-8">

<cfset pos			= findnocase(var,set)>
<!--- find the list position of this variable --->
<cfset pos			= listlen(left(set,pos),'#chr(10)#')>
<!--- get the full string                     --->
<cfset thisString	= listgetat(set,pos,chr(10))>
<cfset string 		= left(thisString,find('= "',thisString)+2)>
<!--- update this string                     --->
<cfset string		= '#string##replace(trim(url.css),'"','""','all')#">'>
<!--- update the file                        --->
<cfset set			= ListSetAt(set,pos,string,chr(10))>
<!--- save the setting file                  --->
<cffile
	action 		= "write"
	mode		= "#application.chomd#"
	file 		= "#settingfile#"
	output 		= "#set#"
	addNewLine 	= "No" 
	charset 	= "utf-8" >

<cfset session.ApplicationReset = "Yes">
<cfset session.msg = "Theme Updated">