<cfprocessingdirective pageEncoding="utf-8">

<cfswitch expression="#url.switch#">
	<cfcase value="apply">
		<cfset var 			= "Application.sti.layout">
		<cfset settingfile	= ExpandPath('../#application.warehouse#/settings.cfm')>
		<cfinclude template="Act_SettingChange.cfm">
		<cflocation addtoken="no" url="#application.ork.uf('switch=home')#">
	</cfcase>
	<cfdefaultcase>
		<cfinclude template="dsp_theam.cfm">
	</cfdefaultcase>
</cfswitch>
