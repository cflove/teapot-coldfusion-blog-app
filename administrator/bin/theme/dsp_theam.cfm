<cfprocessingdirective pageEncoding="utf-8">
<div style="padding:10px">
<cfdirectory directory="#ExpandPath('../styles/')#" name="d">

<cfoutput query="d">
	<div style="float:left; text-align:center; width:250px; height:300px; padding:5px; margin:5px; overflow:hidden">
		<img src="#application.root#/styles/#d.name#/images/layout.png" style="border:1px solid gray" /><br />
		<div style="padding-top:5px;">
			<b>#replacenocase(name,'_',' ','all')#</b>
			<div style="padding:5px">
			<cfif Application.sti.layout eq name>
				<img src="images/chk_1.gif" align="absmiddle" /> Current
			<cfelse>
				<img src="images/chk_0.gif" align="absmiddle" /> <a href="javascript:conf('Change Theme','Apply (#replacenocase(name,'_',' ','all')#) Theme?','#application.ork.uf('switch=apply&css=#name#')#')">Apply</a> 
			</cfif>
			</div>
			<cfif FileExists(ExpandPath('../styles/#d.name#/info.txt'))>
				<cfsavecontent variable="info"><cfinclude template="../../../styles/#d.name#/info.txt"></cfsavecontent>
				<cfif listlen(info,chr(13)) gt 2><div class="alert" style="cursor:help" original-title="#listgetat(info,3,chr(13))#">#trim(listfirst(info,chr(13)))#</div></cfif>
			</cfif>
		</div>
	</div>
</cfoutput>
</div>