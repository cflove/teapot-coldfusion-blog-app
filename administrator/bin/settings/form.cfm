<!---- **************************************************** --->
<!---- Read the Setting Page, Generate a Form               --->
<!---- **************************************************** --->
<cfimport 
	taglib =	"../../system/form"  
	prefix =	"fm">
	
<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">
<ct:msg>
<ct:quickCheck>
<cffile action="read" file="#settingfile#" variable="set" charset="utf-8">

<cfform class="formfield" action="#application.ork.uf('switch=save')#" method="post">
<input type="hidden" name="acordingpage" id="acordingpage" value="<cfoutput>#url.page#</cfoutput>" />
<div style="padding:10px">
<cfset box 	= 0>
<cfset line = 1>
<cfloop list="#set#" delimiters="#chr(10)#" index="i">

<cfif left(i,3) neq '(!)'>
<cfoutput>
	<cfif left(i,1) eq '['>
		<!--- group --->
		<cfif val(box)></div></cfif>
		<cfset box = 1+box>
		<div class="accordinghead">#listfirst(i,'[]')#</div>
		<div style="display:none" class="accordinbox">
	<cfelse>
		<cfif left(i,2) eq '<!'>
			<!--- hint from the comment line ---->
			<div class="settingitem">
			#application.ork.ActivateURL(rereplace(listfirst(i,'['),'<!---|--->',"",'all'),'_blank"')#
			<cfif listlen(i,'[]') gt 2>
				<cfset options = listgetat(i,2,'[]')>
			<cfelse>
				<cfset options = "">
			</cfif>
		<cfelseif left(i,7) eq '<cfset '>
			<!--- value form cfset tag --->
			<cfset value = trim ( right(i, len(i) - (find('= "',i)+2) ) )>
			<cfif len(value) gt 2>
				<cfset value = left(value,len(value)-2)>
			<cfelse>
				<cfset value = ''>			
			</cfif>
			<cfset value = replace(value,'""','"','all')>
			<div style="padding-left:20px; padding-top:5px">
			<cfif listlen(options)>
				<cfif listlen(options) lte 3>
					<cfset ocount = 1>
					<cfloop list="#options#" index="o">
						<input type="radio" name="filed_#line#" <cfif value eq listfirst(o,'-')>checked="checked"</cfif> value="#listfirst(o,'-')#" id="filed_#line##ocount#_f" /> <label style="padding:4px" for="filed_#line##ocount#_f">#listlast(o,'-')#</label>
						<cfset ocount = 1+ocount>
					</cfloop>
				<cfelse>
					<select name="filed_#line#" id="filed_#line#" class="input">
					<cfloop list="#options#" index="o"><option <cfif value eq listfirst(o,'-')>selected="selected"</cfif> value="#listfirst(o,'-')#">#listlast(o,'-')#</option></cfloop>
					</select>
				</cfif>
			<cfelse>
				<cfinput type="text" class="text settingtext" onKeyUp="textSize(this.id)" value="#value#" style="width:500px" name="filed_#line#" id="filed_#line#" /><br />
			</cfif>
			</div></div>
		</cfif>
	</cfif>
</cfoutput>
</cfif>
<cfset line = 1+line>
</cfloop>
</div>
</div>

<fm:label>
<input title="Click Here to Save" class="button" type="submit" value="Save" name="submit" />
</fm:label>
</cfform>

<script type="text/javascript">

$("document").ready(function() {
	acording($('.accordinghead:eq(<cfoutput>#url.page-1#</cfoutput>)'))
	$('<img src="images/arrowsd.png" width="16" align="absmiddle" /> &nbsp;').prependTo($('.accordinghead'))
	$('.accordinghead').click(function() {acording(this)})
	$(".settingtext").keyup(function(){ textSize( $(this).attr('id') ) }).trigger('keyup')
})

$('.formfield').submit(function(){
	if( $('.settingitem:contains("Mail From Address")').children().children('input').val() == '' ) {
		alert('[Mail Settings > Mail From Address] is Required')
		return false
	}
})

function acording(i) {
	if (!$(i).next().is(':visible')) {
		$('.accordinbox:visible').prev().children('img').attr('src','images/arrowsd.png')
		$('.accordinbox:visible').slideUp('normal')
	}
	$('#acordingpage').val( ($(i).parent().children().index(i)/2)+1 )
	$(i).next().slideDown('normal')
	$(i).next().prev().children('img').attr('src','images/arrowdwn.png')
}
</script>