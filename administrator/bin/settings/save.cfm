<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">
<ct:quickCheck>

<cffile action="read" file="#settingfile#" variable="set" charset="utf-8"> 
<cfset newset = ArrayNew(1)>
<cfset line = 1>
<cfloop list="#set#" delimiters="#chr(10)#" index="i">
	<cfif StructKeyExists(form,'filed_#line#')>
		<cfset string = left(i,find('= "',i)+2)>
		<cfset ArrayAppend(newset,'#string##replace(form['filed_#line#'],'"','""','all')#">')>
	<cfelse>
		<cfset ArrayAppend(newset,i)>
	</cfif>
	<cfset line = 1+line>
</cfloop>

<cffile
	action 		= "write"
	mode		= "#application.chomd#"
	file 		= "#settingfile#"
	output 		= "#ArrayToList(newset,chr(10))#"
	addNewLine 	= "No" 
	charset 	= "utf-8" >

<cfset session.ApplicationReset = "Yes">
<cfset session.msg = "Settings Updated">