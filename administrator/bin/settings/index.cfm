<cfprocessingdirective pageEncoding="utf-8">
<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">
	
<cfset settingfile = ExpandPath('../#application.warehouse#/settings.cfm')>
<cfswitch expression="#url.switch#">
	<cfcase value="save">
		<cfinclude template="save.cfm">
		<cflocation addtoken="no" url="#application.ork.uf('switch=rest&page=#form.acordingpage#')#">
	</cfcase>
	<cfdefaultcase>
		<cfinclude template="form.cfm">
	</cfdefaultcase>
</cfswitch>