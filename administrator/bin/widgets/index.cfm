<cfprocessingdirective pageEncoding="utf-8">

<cfset filepath = ExpandPath('../#application.warehouse#/widget_list.cfm')>

<cfswitch expression="#url.switch#">
	<cfcase value="addwidget">
		<cfinclude template="act_addWidget.cfm">
	</cfcase>
	<cfcase value="upload">
		<cfinclude template="dsp_upload.cfm">
	</cfcase>
	<cfcase value="saveSettings">
		<cfinclude template="act_SaveSettings.cfm">	
		<cflocation addtoken="no" url="#application.ork.uf('switch=home')#">
	</cfcase>
	<cfcase value="settings">
		<cfinclude template="dsp_settings.cfm">
	</cfcase>
	<cfdefaultcase>
		<cfinclude template="dsp_widgets.cfm">
	</cfdefaultcase>
</cfswitch>
