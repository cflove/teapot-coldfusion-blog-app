<!---- **************************************************** --->
<!---- Read the Setting Page, Generate a Form               --->
<!---- **************************************************** --->
<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">
<cfimport 
	taglib =	"../../system/form"  
	prefix =	"fm">
<ct:quickCheck>

<cffile action="read" file="#application.wh#widgets/settings/#url.wName#" variable="set" charset="utf-8">

<cfform class="formfield" action="#application.ork.uf('switch=saveSettings&wName=#url.wName#')#" method="post">

<div style="padding:10px">
<div class="accordinghead" style="cursor:auto"><cfoutput>
Widget
<img src="images/arrowsd.png" align="absmiddle"> #trim(listfirst(replacenocase(url.wName,'_',' ','all'),'.'))# 
<img src="images/arrowsd.png" align="absmiddle"> Settings</cfoutput></div>

<cfset line = 1>
<cfloop list="#set#" delimiters="#chr(10)#" index="i">
		<cfoutput>
		<cfif left(i,2) eq '<!'>
			<!--- hint from the comment line ---->
			<div class="settingitem">
			#application.ork.ActivateURL(rereplace(listfirst(i,'['),'<!---|--->',"",'all'),'_blank"')#
			<cfif listlen(i,'[]') gt 2>
				<cfset options = listgetat(i,2,'[]')>
			<cfelse>
				<cfset options = "">
			</cfif>
		<cfelseif left(i,7) eq '<cfset '>
			<!--- value form cfset tag --->
			<cfset value = trim ( right(i, len(i) - (find('= "',i)+2) ) )>
			<cfif len(value) gt 2>
				<cfset value = left(value,len(value)-2)>
			<cfelse>
				<cfset value = ''>			
			</cfif>
			<cfset value = replace(value,'""','"','all')>
			<div style="padding-left:20px; padding-top:5px">
			<cfif listlen(options)>
				<cfif listlen(options) lte 3>
					<cfset ocount = 1>
					<cfloop list="#options#" index="o">
						<input type="radio" name="filed_#line#" <cfif value eq listfirst(o,'-')>checked="checked"</cfif> value="#listfirst(o,'-')#" id="filed_#line##ocount#_f" /> <label style="padding:4px" for="filed_#line##ocount#_f">#listlast(o,'-')#</label>
						<cfset ocount = 1+ocount>
					</cfloop>
				<cfelse>
					<select name="filed_#line#" class="input">
					<cfloop list="#options#" index="o"><option <cfif value eq listfirst(o,'-')>selected="selected"</cfif> value="#listfirst(o,'-')#">#listlast(o,'-')#</option></cfloop>
					</select>
				</cfif>
			<cfelse>
				<cfinput type="text" class="text" onKeyUp="textSize(this.id)" value="#value#" style="width:500px" name="filed_#line#" id="filed_#line#" /><br />
			</cfif>
			</div></div>
		</cfif>
		</cfoutput>
<cfset line = 1+line>
</cfloop>
</div>

<fm:label>
<input title="Click Here to Save" class="button" type="submit" value="Save" name="submit" />
</fm:label>
</cfform>


<script type="text/javascript">
$("document").ready(function() {
	$("input[type:text].text").trigger('keyup');
})
</script>