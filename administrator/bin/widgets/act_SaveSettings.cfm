<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">

<ct:quickCheck>

<!--- ******************************************************* ---->
<!--- Save Settings. Write them back to settings file         ---->
<!--- ******************************************************* ---->
<cffile action="read" file="#application.wh#widgets/settings/#url.wName#" variable="set" charset="utf-8"> 
<cfset newset = ArrayNew(1)>
<cfset line = 1>
<cfloop list="#set#" delimiters="#chr(10)#" index="i">
	<cfif StructKeyExists(form,'filed_#line#')>
		<cfset string = left(i,find('= "',i)+2)>
		<cfset ArrayAppend(newset,'#string##replace(form['filed_#line#'],'"','""','all')#">')>
	<cfelse>
		<cfset ArrayAppend(newset,i)>
	</cfif>
	<cfset line = 1+line>
</cfloop>

<cffile
	action 		= "write"
	mode		= "#application.chomd#"
	file 		= "#application.wh#widgets/settings/#url.wName#"
	output 		= "#ArrayToList(newset,chr(10))#"
	addNewLine 	= "No" 
	charset 	= "utf-8" >

<!--- ******************************************************* ---->
<!--- Republish required for some widgets                     ---->
<!--- ******************************************************* ---->
<cfdirectory action="list" directory="#ExpandPath('./plugins/AfterPublish')#" name="plugins" filter="plugin_*.cfm">
<cfloop query="plugins"><cfif plugins.name eq 'plugin_#url.wName#'><cfinclude template="../../plugins/AfterPublish/#plugins.name#" /></cfif></cfloop>

<cfset session.msg = "Settings Updated">