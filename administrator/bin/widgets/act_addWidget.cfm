<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">

<ct:quickCheck>

<cffile
	action 			= "upload"
	fileField 		= "form.file"
	mode			= "#application.chomd#"
	destination 	= "#application.wh#widgets/"
	nameConflict 	= "overwrite">

<cfset clientFileName = cffile.clientFileName>

<cfif not ListFindNoCase('zip',cffile.clientFileExt)>
	<cfset session.error	= "Error Uploading file. The File You Upload is #ucase(clientFileExt)#. Please upload Zip File only.">
	<cffile
		action 	= "delete"
		file 	= "#application.wh#widgets/#cffile.serverFile#">
	<cflocation addtoken="no" url="#application.ork.uf('switch=upload')#">
<cfelse>
	<!--- unzip the file     --->
	<cfset unzipFile("#application.wh#widgets/#cffile.serverFile#","#application.wh#widgets/")>
	<cftry>
	<cfset thread = CreateObject("java", "java.lang.Thread")>
	<!--- wait 5 seconds      --->
	<cfset thread.sleep(5000)>
	<!--- delete the zip file --->
	<cffile
		action 	= "delete"
		file 	= "#application.wh#widgets/#cffile.serverFile#">
		<cfcatch></cfcatch>
	</cftry>
	<cfset session.msg	= "New Widget Added">
	<!--- fw the user        ---->
	<cfif FileExists("#application.wh#widgets/settings/#clientFileName#.cfm")>
		<cflocation addtoken="no" url="#application.ork.uf('switch=settings&wname=#clientFileName#.cfm')#">
	<cfelse>
		<cflocation addtoken="no" url="#application.ork.uf('switch=home')#">
	</cfif>
</cfif>


<cfscript>
/**
* Unzips a file to the specified directory.
* 
* @param zipFilePath      Path to the zip file (Required)
* @param outputPath      Path where the unzipped file(s) should go (Required)
* @return void 
* @author Samuel Neff (sam@serndesign.com) 
* @version 1, September 1, 2003 
*/
function unzipFile(zipFilePath, outputPath) {
    var zipFile = ""; // ZipFile
    var entries = ""; // Enumeration of ZipEntry
    var entry = ""; // ZipEntry
    var fil = ""; //File
    var inStream = "";
    var filOutStream = "";
    var bufOutStream = "";
    var nm = "";
    var pth = "";
    var lenPth = "";
    var buffer = "";
    var l = 0;

    zipFile = createObject("java", "java.util.zip.ZipFile");
    zipFile.init(zipFilePath);
    
    entries = zipFile.entries();
    
    while(entries.hasMoreElements()) {
        entry = entries.nextElement();
        if(NOT entry.isDirectory()) {
            nm = entry.getName(); 
            
            lenPth = len(nm) - len(getFileFromPath(nm));
            
            if (lenPth) {
            pth = outputPath & left(nm, lenPth);
        } else {
            pth = outputPath;
        }
        if (NOT directoryExists(pth)) {
            fil = createObject("java", "java.io.File");
            fil.init(pth);
            fil.mkdirs();
        }
        filOutStream = createObject(
            "java", 
            "java.io.FileOutputStream");
        
        filOutStream.init(outputPath & nm);
        
        bufOutStream = createObject(
            "java", 
            "java.io.BufferedOutputStream");
        
        bufOutStream.init(filOutStream);
        
        inStream = zipFile.getInputStream(entry);
        buffer = repeatString(" ",1024).getBytes(); 
        
        l = inStream.read(buffer);
        while(l GTE 0) {
            bufOutStream.write(buffer, 0, l);
            l = inStream.read(buffer);
        }
        inStream.close();
        bufOutStream.close();
        }
    }
    zipFile.close();
}
</cfscript>