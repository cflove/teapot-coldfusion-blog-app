<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">

<ct:quickCheck>
<ct:msg>

<!--- ********************************************** --->
<!--- get all widgets from the widget folder         --->
<!--- ********************************************** --->
<cfdirectory action="list" directory="#application.wh#widgets/" name="widgets" filter="*.cfm" type="file">

<cfset totalused = ''>
<!--- ********************************************** --->
<!--- get current list from widget_list.cfm          --->
<!--- ********************************************** --->
<cffile action="read" file="#ExpandPath('../#application.warehouse#/widget_list.cfm')#" variable="w" charset="utf-8">
<cfset wList = "">
<cfloop list="#w#" index="i" delimiters="#chr(10)#">
	<cfif listlen(i,'''') eq 3>
		<cfset wList = ListAppend(wList, listlast(listgetat(i,2,''''),'/\') )>
	</cfif>
</cfloop>

<cfset totalused = ListAppend(totalused,wList)>

<!--- ********************************************** --->
<!--- get current list from footer_widget_list.cfm   --->
<!--- ********************************************** --->
<cffile action="read" file="#ExpandPath('../#application.warehouse#/footer_widget_list.cfm')#" variable="f" charset="utf-8">
<cfset fwList = "">
<cfloop list="#f#" index="i" delimiters="#chr(10)#">
	<cfif listlen(i,'''') eq 3>
		<cfset fwList = ListAppend(fwList, listlast(listgetat(i,2,''''),'/\') )>
	</cfif>
</cfloop>


<!--- ********************************************** --->
<!--- display unused widgets                         --->
<!--- ********************************************** --->
<cfset totalused = ListAppend(totalused,fwList)>
<div style="padding:10px">
<div style="float:left; width:300px; padding:5px">
<b>Available Widgets</b>

<ul class="sortable" style="padding:0px; min-height:100px; border:1px dotted #E5E5E5" id="sortable1">
<cfoutput query="widgets">
	<cfif not listfindnocase(totalused,name)>
		<li style="width:300px; list-style:none; clear:both" class="drager">
			<div class="accordinghead" style="float:left; width:250px;">
				<input type="hidden" name="list" value="#name#" />
				<span class="widgetLabel" id="#name#">#listfirst(replacenocase(name,'_',' ','all'),'.')#</span>
			</div>
			<div style="float:left; padding-top:10px">
				<cfif FileExists("#application.wh#widgets/settings/#name#")>
					<a style="border-bottom:none" href="#application.ork.uf('switch=settings&wName=#name#')#"><img src="images/settings.png" border="0" class="settingicon" align="absmiddle" original-title="Settings" /></a>
				</cfif>
			</div>
			<div style="clear:both"></div>
		</li>
	</cfif>
</cfoutput>
</ul>
</div>


<!--- ********************************************** --->
<!--- panels                                         --->
<!--- ********************************************** --->
<div style="float:left; width:300px; padding:5px">
<b>Side Panel</b>

<ul class="sortable" style="padding:0px; min-height:100px; border:1px dotted #E5E5E5" id="sortable2">
<cfloop from="1" to="#listlen(wList)#" index="i">
	<cfset name	 	= trim(listgetat(wList,i))>
		<li style="width:300px; list-style:none; clear:both">
			<div class="accordinghead" style="float:left; width:250px;">
				<cfoutput>
					<input type="hidden" name="list" value="#name#" />
					<span class="widgetLabel" id="#name#">#trim(listfirst(replacenocase(name,'_',' ','all'),'.'))#</span>
				</cfoutput>
			</div>
			<div style="float:left; padding-top:10px">
				<cfif FileExists("#application.wh#widgets/settings/#name#")>
					<cfoutput><a style="border-bottom:none" href="#application.ork.uf('switch=settings&wName=#name#')#"><img src="images/settings.png" border="0" class="settingicon alert" align="absmiddle" title="Change Settings" /></a></cfoutput>
				</cfif>
			</div>
			<div style="clear:both"></div>
		</li>
</cfloop>
</ul>
</div>

<div style="float:left; width:300px; padding:5px">
<b>Footer Panel</b>

<ul class="sortable" style="padding:0px; min-height:100px; border:1px dotted #E5E5E5" id="sortable3">
<cfloop from="1" to="#listlen(fwList)#" index="i">
	<cfset name	 	= trim(listgetat(fwList,i))>
		<li style="width:300px; list-style:none; clear:both">
			<div class="accordinghead" style="float:left; width:250px;">
				<cfoutput>
					<input type="hidden" name="list" value="#name#" />
					<span class="widgetLabel" id="#name#">#trim(listfirst(replacenocase(name,'_',' ','all'),'.'))#</span>
				</cfoutput>
			</div>
			<div style="float:left; padding-top:10px">
				<cfif FileExists("#application.wh#widgets/settings/#name#")>
					<cfoutput><a style="border-bottom:none" href="#application.ork.uf('switch=settings&wName=#name#')#"><img src="images/settings.png" border="0" class="settingicon" align="absmiddle" title="Change Settings" /></a></cfoutput>
				</cfif>
			</div>
			<div style="clear:both"></div>
		</li>
</cfloop>
</ul>
</div>
<div style="clear:both"></div>

<div id="widStat" style="padding-bottom:10px; font-weight:bold"></div>

* Drag Widgets from "Available Widgets" and drop them in to "Panels" to activate and move back to disable. <br />
* You can move Widgets between Panels. <br /> 
* Move Widgets vertically to set display order. <br />
* <a href="<cfoutput>#application.ork.uf('switch=upload')#</cfoutput>">Upload Widgets</a>
</div>

<SCRIPT type="text/javascript">
	$(function() {
		$("#sortable1, #sortable2 , #sortable3").sortable({
			connectWith: '.sortable',
			delay: 500,
			stop: function(e, ui) { updatewidget() }

		}).disableSelection();
	});
	function updatewidget() {
		var side = []; 
		$('#sortable2 input[name="list"]').each(function(i, s){ 
			side[i] = $(this).val(); 
		});	
		var footer = []; 
		$('#sortable3 input[name="list"]').each(function(i, s){ 
			footer[i] = $(this).val(); 
		});	
		$('#widStat').html('<img src="images/loading.gif" align="absmiddle" > Saving')
		$.ajax({url : cfc + "?method=widgetSave&returnformat=plain&side=" + side.toString() +'&footer='+ footer.toString(), dataType: 'html',cache:false, success: function(data) { 
			$('#widStat').html('<img src="images/success.png" align="absmiddle" > Widget Updated')		
		}})
	}
</SCRIPT>