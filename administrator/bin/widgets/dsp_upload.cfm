<cfimport 
	taglib =	"../../system/form"  
	prefix =	"fm">
	
<div style="padding:10px">
<div class="accordinghead">Upload Widget</div>
<cfform class="formfield" action="#application.ork.uf('switch=addwidget')#" method="post" enctype="multipart/form-data">

<fm:label label="Widget File">
	<input type="file" class="text" name="file" />
	(Zip File)
</fm:label> 
<fm:label>
<input title="Click Here to Save" class="button" type="submit" value="Upload" name="submit" />
</fm:label>
</cfform>
</div>

<script type="text/javascript">
$(".formfield").submit(function() {
	$('.button').val('Processing..')
	$('.button').attr('disabled', 'disabled');
})
</script>