<div style="padding:10px">
<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">

<ct:tabbox
	tabs	= "All Pages,Draft,New Page,Group&nbsp;&&nbsp;Order">
	<cfswitch expression="#url.tab#">
    	<cfcase value="4">
        	<cfinclude template="dsp_order.cfm">
        </cfcase>
    	<cfcase value="3">
			<cfset url.id = 0>
        	<cfinclude template="../blogpost/edit_post.cfm">
        </cfcase>
    	<cfcase value="2">
        	<cfinclude template="../blogpost/list_post.cfm">
        </cfcase>
    	<cfdefaultcase>
        	<cfinclude template="../blogpost/list_post.cfm">
        </cfdefaultcase>
    </cfswitch>
</ct:tabbox>
<br /><br />
</div>