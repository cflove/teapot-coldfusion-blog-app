<cfquery name="get" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	SELECT blogpostid, blogpostname,mblogpostid from blogpost
	where pagetype = <cfqueryparam cfsqltype="cf_sql_tinyint" value="2"> 
	and mblogpostid = 0
	order by orderby, blogpostname, mblogpostid
</cfquery>

<!--- ****************************************************************** --->
<!--- menu                                                               --->
<!--- ****************************************************************** --->
<cfparam name="url.action" default="main">
<div style="padding:10px">
<cfif get.recordCount>
<cfoutput>
<span id="sortlinks">
	<cfif get.recordCount gt 1>
		<img src="images/chk_#YesNoFormat(compare('main',url.action))#.gif" align="absmiddle" />
		<a href="#application.ork.uf('action=main&tab=#url.tab#')#">Sort Main Pages</a>
		<img src="images/div.gif" align="absmiddle" />
	</cfif>
	<span id="sublink" style="display:none"> <img src="images/chk_#YesNoFormat(compare('sub',url.action))#.gif" align="absmiddle" /> <a href="#application.ork.uf('action=sub&tab=#url.tab#')#">Sort Sub Pages</a></span>
</span>
</cfoutput>

<!--- ****************************************************************** --->
<!--- page list                                                          --->
<!--- ****************************************************************** --->
<ul id="mpsort" style="width:400px; list-style:none">
<cfoutput query="get">
	<li class="accordinghead" style="cursor:none; font-weight:normal"><img src="images/move.png" align="absmiddle" /> #blogpostname#<input type="hidden" name="mblogpostid" value="#blogpostid#" /> <a href="#application.ork.uf('switch=edit&id=#blogpostid#')#"><img class="alert" border="0" src="images/editwhite.png" title="Click Here to Edit This Page" align="right" /></a></li>
		<cfquery name="sub" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			SELECT blogpostid, blogpostname,mblogpostid from blogpost
			where pagetype = <cfqueryparam cfsqltype="cf_sql_tinyint" value="2"> 
			and mblogpostid = #val(blogpostid)#
			order by orderby, blogpostname, mblogpostid
		</cfquery>
		<!--- sub page list --->
		<ul class="spsort" id="subbox#blogpostid#" value="#blogpostid#" style="width:300px; list-style:none; padding-bottom:5px; padding-top:5px;">
		<cfloop query="sub">
			<li class="accordinghead" style="border:1px solid ##F60; font-weight:normal"><img src="images/move.png" align="absmiddle" /> #sub.blogpostname#  <input type="hidden" name="sblogpostid" value="#blogpostid#" /> <a href="#application.ork.uf('switch=edit&id=#blogpostid#')#"><img class="alert" border="0" src="images/editwhite.png" title="Click Here to Edit This Page" align="right" /></a></li>
		</cfloop>
		</ul>
</cfoutput>
</ul>

<div id="widStat" style="padding-bottom:10px; font-weight:bold"></div>
* Drag and drop pages to set new display order.

<cfelse>
	No Pages Available 
</cfif>
</div>

<script type="text/javascript">
<!--- ****************************************************************** --->
<!--- jquery sortable and ajax updates                                   --->
<!--- ****************************************************************** --->
<cfswitch expression="#url.action#">
<cfcase value="main">
<!--- set display order of main pages ---->
		$(function() {
			<!--- if no sub pages available, remove the link --->
			$('.spsort').each(
				function() {
					if ( $(this).children('li').length )
					$('#sublink').css('display','')
					$(this).remove()
				}
			)
			$('#mpsort').children('li').css('border','1px solid #F60')
			$('#mpsort').sortable({ stop: function(e,ui) { setorder() }}).disableSelection()
		});
		// add main page ids in to an array in the new order
		function setorder() {
			$('#widStat').html('<img src="images/loading.gif" align="absmiddle" > Saving')
			var mpages = []; 
			$('#mpsort input[name="mblogpostid"]').each(function(i, s){ 
				mpages[i] = $(this).val(); 
			});	
			// send to the server
			$.ajax({url: cfc + "?method=pageorder&returnformat=plain&pages="+ mpages.toString(), dataType: "text", cache:false, success: function(data){ 
				rpageorder()
			}})
		}
</cfcase>
<cfcase value="sub">
<!--- set display order of sub pages ---->
	$(function() {
		$('.mgrip').css('display','none')
		// get list of sub page boxes
		var subboxlist = []
		$('#mpsort .spsort').each(function(i, s){ 
			subboxlist[i] = '#'+$(this).attr('id')
		})
		// create a sortable list 
		$( subboxlist.toString() ).sortable({
			connectWith: '.spsort',
			stop : function(e,ui) {subpageorder()}
		}).disableSelection();
	})
	function subpageorder() {
		// add main pages and subpage ids in to an array 
		var pagelist = []
		$('#mpsort input[name="sblogpostid"]').each(function(i, s){ 
			pagelist[i] = $(this).val()+'|'+$(this).parent().parent().attr('value')
		});	
		// send to the server
		$.ajax({url: cfc + "?method=subpageorder&returnformat=plain&pages="+ pagelist.toString(), dataType: "text", cache:false, success: function(data){ 
			rpageorder()
		}})
	}
</cfcase>
</cfswitch>
function rpageorder() {
	$('#widStat').html('<img src="images/success.png" align="absmiddle" > Display Order Updated')
}
</script>