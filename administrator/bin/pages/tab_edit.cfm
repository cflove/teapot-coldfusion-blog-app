<div style="padding:10px">
<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">

<ct:tabbox
	tabs	= "Edit Page,All Pages,Draft,New Page,Group&nbsp;&&nbsp;Order">
	<cfswitch expression="#url.tab#">
    	<cfcase value="5">
			<cflocation addtoken="no" url="#application.ork.uf('switch=post&tab=4','id')#">
        </cfcase>
    	<cfcase value="4">
			<cflocation addtoken="no" url="#application.ork.uf('switch=post&tab=3','id')#">
        </cfcase>
    	<cfcase value="3">
        	<cflocation addtoken="no" url="#application.ork.uf('switch=post&tab=2')#">
        </cfcase>
    	<cfcase value="2">
        	<cflocation addtoken="no" url="#application.ork.uf('switch=post&tab=1')#">
        </cfcase>
    	<cfdefaultcase>
        	<cfinclude template="../blogpost/edit_post.cfm">
        </cfdefaultcase>
    </cfswitch>
</ct:tabbox>
<br /><br />
</div>