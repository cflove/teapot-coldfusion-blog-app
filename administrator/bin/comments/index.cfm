<cfprocessingdirective pageEncoding="utf-8">

<cfswitch expression="#url.switch#">
	<cfcase value="publish">
		<cfinclude template="act_publish.cfm">
	</cfcase>
	<cfcase value="commentremove">
		<cfinclude template="act_deleteComment.cfm">
	</cfcase>
	<cfdefaultcase>
		<cfinclude template="tab_comments.cfm">
	</cfdefaultcase>
</cfswitch>
