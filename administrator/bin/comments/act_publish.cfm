<cfparam name="url.spam" default="">
<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">
<ct:quickCheck>

<cfquery name="publish" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	update comments  set
	valid 	= <cfqueryparam cfsqltype="cf_sql_bit" value="1">,
	publish	= <cfqueryparam cfsqltype="cf_sql_tinyint" value="1">
	where commentid = <cfqueryparam cfsqltype="cf_sql_char" value="#url.id#">
</cfquery>

<!--- ************************************************************************************ --->
<!--- Email Subscription                                                                   --->
<!--- ************************************************************************************ --->

<cfquery name="thispost" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	SELECT blogpost.blogpostname, blogpost.url, bloguser.email, blogpost.blogpostid, blogpost.created
	FROM blogpost INNER JOIN
	bloguser ON blogpost.bloguser = bloguser.bloguserid
	where blogpostid = #val(url.key)#
</cfquery>

<ct:act_collection id="#url.key#">
<cfset commentid = url.id>
<cfinclude template="../../../teapot/comments/act_commentSubscription.cfm">

<cfset session.msg =  "Comment Published">
<cflocation addtoken="no" url="#application.ork.uf('switch=home&id=#url.id#&tab=#url.tab#')#">