<cfprocessingdirective pageEncoding="utf-8">
<!--- ******************************************************** --->
<!--- query                                                    --->
<!--- ******************************************************** --->
<cfquery name="master" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	SELECT comments.commentid FROM blogpost INNER JOIN
	comments ON blogpost.blogpostid = comments.blogpostid where
	<cfswitch expression="#url.tab#">
		<cfcase value="2"> comments.publish = <cfqueryparam cfsqltype="cf_sql_tinyint" value="0"> 
		and comments.valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1"></cfcase>
		<cfcase value="3"> comments.valid = <cfqueryparam cfsqltype="cf_sql_bit" value="0"></cfcase>
		<cfdefaultcase> comments.publish = <cfqueryparam cfsqltype="cf_sql_tinyint" value="1"> 
		and comments.valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1"></cfdefaultcase>
	</cfswitch>
	<cfif val(url.id2)>
		and blogpost.blogpostid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id2)#">
	</cfif>
	<cfif len(url.srch)>
		and (blogpost.blogpostname like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#trim(url.srch)#%"> 
		or comments.comments like  <cfqueryparam cfsqltype="cf_sql_varchar" value="%#trim(url.srch)#%"> )
	</cfif>
	<cfif len(url.order)>
		order by #Decrypt(url.order, application.key, 'AES', 'hex')# 
		<cfswitch expression="#url.dir#"><cfcase value="1">desc</cfcase><cfdefaultcase>asc</cfdefaultcase></cfswitch>
	<cfelse>
		ORDER BY comments.created desc
	</cfif>
</cfquery>

<cfset searchlist	= "#QuotedValueList(master.commentid)#">
<cfinclude template="../../system/library/grid_calc.cfm">

<cfquery name="get" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	SELECT blogpost.blogpostid, blogpost.blogpostname, comments.commentid, comments.comments, comments.created, comments.valid, comments.publish, comments.name, comments.email, comments.website,
	comments.publish, blogpost.url, blogpost.created as bcreated, comments.ipaddress
	FROM blogpost INNER JOIN
	comments ON blogpost.blogpostid = comments.blogpostid
	where 
	<cfif listlen(searchlist)>comments.commentid in (#PreserveSingleQuotes(searchlist)#)<cfelse>comments.commentid = ''</cfif>
	<cfif len(url.order)>
		order by #Decrypt(url.order, application.key, 'AES', 'hex')# 
		<cfswitch expression="#url.dir#"><cfcase value="1">desc</cfcase><cfdefaultcase>asc</cfdefaultcase></cfswitch>
	<cfelse>
		ORDER BY comments.created desc
	</cfif>
</cfquery>

<!--- ******************************************************** --->
<!--- bread crub for comments by the post                      --->
<!--- ******************************************************** --->
<cfif val(url.id2)>
<cfquery name="thispost" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select blogpostname from blogpost where blogpostid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id2)#">
</cfquery>
<div style="background-image:url(images/bcbg.png);"><img src="images/bcbg.png" align="absmiddle" /><cfoutput> <a href="#application.ork.uf('','id2,page,srch')#">All Posts</a> <img align="absmiddle" src="images/bcarow.gif" /> #thispost.blogpostname#</cfoutput></div>
</cfif>

<cfset titlelist = "Author|name,Comment|comments ,Post|blogpostname">
<cfinclude template="../../system/library/grid_header.cfm">

<!--- ******************************************************** --->
<!--- grid                                                     --->
<!--- ******************************************************** --->

<cfoutput query="get">
<tr <cfif not currentrow mod 2>class="zibrablk"<cfelse>class="zibrawhite"</cfif>>
	<td valign="top" width="180">
	<img src="http://www.gravatar.com/avatar/#lcase(Hash(lcase(email)))#?s=30" style="float:left; width:30px; margin-right:5px" />
	<div style="float:left">
	<cfif len(website)><a target="_blank" style="text-decoration:none" href="#website#">#name#</a><cfelse>#name#</cfif>
	<cfif len(email)><br />(#email#)</cfif>
	<cfif len(ipaddress)><br />(#ipaddress#)</cfif>
	<br />#dateformat(get.created,'mmmm-dd-yy')#
	</div>
	</td>
	<td id="#get.commentid#">
	<div class="commentline">
	<cfif len(get.comments) gt 200>
		<div style="display:inline">#application.ork.fullLeft(get.comments,200)#...(<a href="##" class="showfull">full</a>)</div>
		<span style="display:none"><div class="fullcmt" style="display:inline">#get.comments#</div> (<a href="##" class="showshot">short</a>)</span>
	<cfelse>
		<span class="fullcmt">#get.comments#</span>
	</cfif>
	</div>
	<div style="padding-top:5px" class="bmtbtnset">
	<a class="deletelink" href="javascript:conf('Delete Comment','Are you sure you want to delete this comment?','#application.ork.uf('switch=commentremove&id=#get.commentid#&tab=#url.tab#&key=#get.blogpostid#')#')">Delete</a>
	<img src="images/div.gif" align="absmiddle" />
	<a href="javascript:" class="editlink">Edit</a> 
	<cfif not val(get.valid) or get.publish neq 1>
		<img src="images/div.gif" align="absmiddle" />
		<a href="#application.ork.uf('switch=publish&id=#get.commentid#&tab=#url.tab#&key=#get.blogpostid#')#">Publish</a>
	</cfif>
	</div>
	<div class="editbox"></div>
	</td>
	<td valign="top" align="right"><a href="#application.root##DateFormat(get.bcreated,'yyyy')#/#DateFormat(get.bcreated,'mm')#/#get.url#.cfm" target="_blank">#blogpostname#</a></td>
</tr>
</cfoutput>
</table>
<div id="editorsniplet" style="display:none"><textarea style="width:99%;height:100px" class="input"></textarea><div style="padding:5px"><input type="button" class="button" value="Cancel" /> <input type="button" class="buttonblu" value="Save" /></div></div>
<cfinclude template="../../system/library/grid_navi.cfm">
</div>
<!------>
<!--- ******************************************************** --->
<!--- comment edit javascript                                  --->
<!--- ******************************************************** --->
<cfsavecontent variable="js">
<script type="text/javascript">
$(document).ready(function(){
	// show hide
	$('.commentline').on('click','.showfull',function(){ $(this).parent().css('display','none'); $(this).parent().next().show('slow') })
	$('.commentline').on('click','.showshot',function(){ $(this).parent().css('display','none'); $(this).parent().prev().show('slow') })
	// edit
	$('.editlink').click(function(){
		$(this).parent().slideUp('slow').prev().slideUp('slow')
		$(this).parent().parent().children('.editbox').html( $('#editorsniplet').html() ).show('slow').children('textarea').html( $(this).parent().prev().find('.fullcmt').html().replace(/<br>/gi,'\r') )
	})
	// cancel edit 
	$('.editbox').on('click','.button[type="button"][value="Cancel"]',function(){
		$(this).parent().parent().prev().css('display','block').prev().css('display','block')
		$(this).parent().parent().slideUp('slow').empty()
	})
	// save edit 
	$('.editbox').on('click','.buttonblu[type="button"][value="Save"]',function(){
		$.ajax({url : cfc + "?method=editComment&returnformat=plain&commentid=" + $(this).parent().parent().parent().attr('id') +'&comments='+$(this).parent().prev().val(), dataType: 'json',cache:false, success: function(data) { 
			if ( data.I !=='') {
				try{
				$('#'+data.I+' div:first-child').html(data.html)
				} catch(r) {
					$('#'+data.I+' div:first-child').html(data.HTML)
				}
				$('#'+data.I+' div:first-child').show('slow')
				$('#'+data.I+' .bmtbtnset').show('slow')
				$('#'+data.I +' .editbox').slideUp('slow');
				showmsg(data.MSG)
			} else {
				showerror(data.MSG)
			}
		}})
	})
	})
</script>
</cfsavecontent>
<cfhtmlhead text="#js#">