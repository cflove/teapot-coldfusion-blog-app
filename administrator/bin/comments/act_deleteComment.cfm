<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">
<ct:quickCheck>

<cfif url.id eq 'all'>
	<!--- delete all spams --->
	<cfquery name="dellist" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		select commentid from comments where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="0">
	</cfquery>
	<cfset url.id = valuelist(dellist.commentid)>
</cfif>

<!--- **************************************************** --->
<!--- check if child comments avalalble                    --->
<!--- **************************************************** --->
<cftransaction>
<cfloop list="#url.id#" index="i">

	<cfquery name="chk" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		select commentid, blogpostid from comments where mcommentid = <cfqueryparam cfsqltype="cf_sql_char" value="#i#">
	</cfquery>
	
	<cfif chk.recordCount>
		<!--- move all the child comments to upper level before delete this comment --->
		<cfquery name="mstr" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			select mcommentid from comments where commentid = <cfqueryparam cfsqltype="cf_sql_char" value="#i#">
		</cfquery>
		<cfloop query="chk">
			<cfquery name="update" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
				update comments  set
				mcommentid = '#mstr.mcommentid#'
				where commentid = <cfqueryparam cfsqltype="cf_sql_char" value="#i#">
			</cfquery>
		</cfloop>
	</cfif>
	
	<cfquery name="chk" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		select valid, ipaddress from comments where commentid = <cfqueryparam cfsqltype="cf_sql_char" value="#i#">
	</cfquery>
	<!--- add deleted spam messages ip to the blacklist --->
	<cfif chk.recordCount and not val(chk.valid)>
		<cfquery name="chkSpam" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			select ipaddress from blacklisted_ips where ipaddress = '#chk.ipaddress#'
		</cfquery>
		<cfif chkSpam.recordCount>
			<cfquery name="update" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
				update blacklisted_ips set 
				lastfound	= #CreateODBCDate(now())#
				where ipaddress = '#trim(chk.ipaddress)#'
			</cfquery>
		<cfelse>
			<cfquery name="insert" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
				insert into blacklisted_ips
				(lastfound, ipaddress ) values
				(#CreateODBCDate(now())#, '#trim(chk.ipaddress)#' )
			</cfquery>
		</cfif>
	</cfif>
	
	<!--- Delete this comment --->
	<cfquery name="delete" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		delete from comments where commentid = <cfqueryparam cfsqltype="cf_sql_char" value="#i#">
	</cfquery>
</cfloop>
</cftransaction>

<cfset session.msg =  "Comment Removed">
<cflocation url="index.cfm?path=#url.path#&tab=#url.tab#">