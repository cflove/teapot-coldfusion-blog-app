<div style="padding:10px">
<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">

<cfquery name="pu" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select count(commentid) as ccount FROM blogpost INNER JOIN
	comments ON blogpost.blogpostid = comments.blogpostid where comments.publish = <cfqueryparam cfsqltype="cf_sql_tinyint" value="1"> and comments.valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
	<cfif val(url.id2)>
		and blogpost.blogpostid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id2)#">
	</cfif>
</cfquery>
<cfquery name="p" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select count(commentid) as ccount FROM blogpost INNER JOIN
	comments ON blogpost.blogpostid = comments.blogpostid where comments.publish = <cfqueryparam cfsqltype="cf_sql_tinyint" value="0"> and comments.valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
	<cfif val(url.id2)>
		and blogpost.blogpostid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id2)#">
	</cfif>
</cfquery>
<cfquery name="s" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select count(commentid) as ccount FROM blogpost INNER JOIN
	comments ON blogpost.blogpostid = comments.blogpostid where comments.valid = <cfqueryparam cfsqltype="cf_sql_bit" value="0">
	<cfif val(url.id2)>
		and blogpost.blogpostid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id2)#">
	</cfif>
</cfquery>

<cfset puicon 	= '<div class="tabhint">#val(pu.ccount)#</div>'>
<cfset picon 	= '<div class="tabhint">#val(p.ccount)#</div>'>
<cfset sicon 	= '<div class="tabhint">#val(s.ccount)#</div>'>

<ct:tabbox
	tabs	= 'Published #puicon#,Pending #picon#,Spam #sicon#'>
	<cfswitch expression="#url.tab#">
    	<cfcase value="3">
			<cfinclude template="list_comment.cfm">
			<cfif get.recordCount>
				<cfoutput><div class="emptycmnt" style="padding-left:10px; padding-top:5px;"><a href="javascript:conf('Empty Spam Folder','Empty Spam Folder?','#application.ork.uf('switch=commentremove&id=all&tab=#url.tab#')#')">Empty Spam Folder</a></div><br /></cfoutput>
				<script type="text/javascript">
					$('.grid').before( $('.emptycmnt').clone() )
				</script>
			</cfif>
        </cfcase>
    	<cfcase value="2">
			<cfinclude template="list_comment.cfm">
        </cfcase>
    	<cfdefaultcase>
        	<cfinclude template="list_comment.cfm">
        </cfdefaultcase>
    </cfswitch>
</ct:tabbox>
<br /><br />
</div>

