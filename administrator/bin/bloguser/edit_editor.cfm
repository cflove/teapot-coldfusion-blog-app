<cfimport 
	taglib =	"../../system/form"  
	prefix =	"fm">

<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">

<cfquery name="thisq" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select blogusername,email,username,description, password from bloguser where bloguserid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">
</cfquery>

<cfform action="#application.ork.uf('switch=add&id=#url.id#')#" method="post" class="formfield" enctype="multipart/form-data">

<fm:label required label="User Name">
<cfif val(url.id)>
	<cfoutput>#thisq.username#<input type="hidden" name="username" value="#thisq.username#"></cfoutput>
<cfelse>
	<cfinput 
		name		= "username" 
		type		= "text"
		class		= "text"
		maxlength	= "20"
		value		= "#thisq.username#"
		required	= "yes"
		message		= "User Name is Required" />
</cfif>
</fm:label>

<fm:label label="Password">
<cfif not val(url.id)>
	<!--- new user, password is required --->
	<cfinput 
		name		= "password" 
		id			= "password" 
		type		= "password"
		class		= "text"
		maxlength	= "20" 
		required	= "yes" 
		message		= "Please Enter Password" /> 
<cfelse>
	<!--- existing user, can leave password blank --->
	<a id="editpw" href="javascript:">edit</a>
	<input 
		name		= "password" 
		style		= "display:none"
		id			= "password" 
		type		= "password"
		class		= "text"
		maxlength	= "20" />
		<script type="text/javascript">
		$(function(){
			$('#passwordagain').css('display','none')
			$('#editpw').click(function(){
				$('#password').slideDown('fast')
				$('#passwordagain').slideDown('fast')
				$('#password').trigger('keyup')
				$(this).remove()
			})
		})
		</script>
</cfif>
</fm:label>

<div id="passwordagain">
<fm:label label="Password (again)">
<cfinput 
	name		= "passwordagain" 
	id			= "passwordagain" 
	type		= "password"
	class		= "text"
	maxlength	= "20" /> 
	<fm:passwordmeater>
</fm:label>
</div>

<fm:label required label="Name">
<cfinput 
	name		= "blogusername" 
	type		= "text"
	class		= "text"
	maxlength	= "20"
	value		= "#thisq.blogusername#"
	required	= "yes"
	message		= "Name is Required" />
</fm:label>

<fm:label required label="Email">
<cfinput 
	name		= "email" 
	type		= "text"
	class		= "text"
	maxlength	= "40"
	value		= "#thisq.email#"
	validate	= "email"
	required	= "yes"
	message		= "Email is Required" />
</fm:label>


<fm:label label="Description">
<textarea
	name		= "description" 
	class		= "textarea" 
	style		= "width:500px"
	maxlength	= "500"><cfoutput>#thisq.description#</cfoutput></textarea>
</fm:label>

<fm:label label="Photo (jpg)">
<input 
	name		= "file" 
	type		= "file"
	class		= "text" />
	<cfif FileExists('#application.wh#users/#url.id#.jpg')>
		<cfoutput><a href="#application.root##application.warehouse#/users/#url.id#.jpg" target="_blank"><img border="0" style="border:2px solid ##333" src="#application.root##application.warehouse#/users/thumb_#url.id#.jpg?#rand()#" align="absmiddle" /></a>
		(<a href="#application.ork.uf('switch=idelete&id=#url.id#')#">Delete</a>)</cfoutput>
	</cfif>
</fm:label>

<cfif val(url.id)>
	<cfset delete = "<a href=""javascript:conf('Delete Editor','Are you sure you want to delete this Editor?','#application.ork.uf('switch=delete&id=#url.id#')#')"">Delete</a>">
<cfelse>
	<cfset delete = "">
</cfif>

<fm:label label="#delete#">
	<input title="Click Here to Save" class="button" type="submit" value="Save" name="submit" />
</fm:label>
</cfform>