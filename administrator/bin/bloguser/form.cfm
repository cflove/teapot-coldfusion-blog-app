<cfimport 
	taglib =	"../../system/form"  
	prefix =	"fm">

<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">

<ct:toolbar>

<cfform action="#application.ork.uf('switch=add&id=#url.id#')#" method="post" class="formfield">

<fm:label required label="Blogusername">
<cfinput 
	name		= "blogusername" 
	type		= "text"
	class		= "text"
	maxlength	= "40"
	value		= "#thisq.blogusername#"
	required	= "yes"
	message		= "Blogusername is Required" />
</fm:label>

<fm:label required label="Username">
<cfinput 
	name		= "username" 
	type		= "text"
	class		= "text"
	maxlength	= "40"
	value		= "#thisq.username#"
	required	= "yes"
	message		= "Username is Required" />
</fm:label>

<fm:label required label="Email">
<cfinput 
	name		= "email" 
	type		= "text"
	class		= "text"
	maxlength	= "80"
	value		= "#thisq.email#"
	validate	= "email"
	required	= "yes"
	message		= "Email is Required" />
</fm:label>

<fm:label required label="Password">
<cfinput 
	name		= "password" 
	type		= "password"
	onKeyUp		= "chkpw(this.value)"
	class		= "text"
	maxlength	= "40"
	value		= "#thisq.password#"
	required	= "yes"
	message		= "Password is Required" /><fm:passwordmeater>
</fm:label>

<fm:label label="Description">
<cftextarea
	name		= "description" 
	class		= "textarea"
	maxlength	= "1000"
	value		= "#thisq.description#" />
</fm:label>

<fm:label>
<input title="Click Here to Save" class="button" type="submit" value="Save" name="submit" />
</fm:label>
</cfform>
