<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">
<ct:quickCheck>
<cfimport 
	taglib =	"../../system/form"  
	prefix =	"fm">

<cfif not len(trim(form.blogusername)) or not len(trim(form.username)) or not len(trim(form.email))>
	<cfset session.error = "Please Complete All required fields">
</cfif>

<cfif not StructKeyExists(session,'error')>
	<cftransaction>
	<!--- **************************************************** --->
	<!--- update password                                      --->
	<!--- **************************************************** --->
	<cfif len(trim(form.password))>
		<ct:passwordenc password="#form.password#">
		<cfif val(url.id)>
			<cfquery name="update" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
				update bloguser set
				password 	= <cfqueryparam cfsqltype="cf_sql_varchar" value="#passwordenc#">,
				salt	 	= <cfqueryparam cfsqltype="cf_sql_varchar" value="#salt#">,
				updatedBy	= #session.id#
				where bloguserid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">
			</cfquery>
		</cfif>
	</cfif>
	
	<!--- **************************************************** --->
	<!--- insert                                               --->
	<!--- **************************************************** --->
	<cfif not val(url.id)>
		<cfif not len(trim(form.password))>
			<cfset session.error = "Please Enter Password">
		</cfif>
		<cflock timeout="1" name="bloguseradd">
		<cfquery name="in" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			insert into bloguser
			(createdBy,blogusername,username,email,createdate,password,salt,cookiekey) values
			(#session.id#,
			<cfqueryparam cfsqltype="cf_sql_varchar"	value="#trim(form.blogusername)#">,
			<cfqueryparam cfsqltype="cf_sql_varchar"	value="#trim(form.username)#">,
			<cfqueryparam cfsqltype="cf_sql_varchar"	value="#trim(form.email)#">,
			<cfqueryparam cfsqltype="cf_sql_timestamp"	value="#CreateODBCDateTime(now())#">,
			<cfqueryparam cfsqltype="cf_sql_varchar"	value="#passwordenc#">,
			<cfqueryparam cfsqltype="cf_sql_varchar" 	value="#salt#">,
			<cfqueryparam cfsqltype="cf_sql_varchar"	value="#CreateUUID()#">)
			<cfswitch expression="#application.dbtype#">
				<cfcase value="mssql">
					select SCOPE_IDENTITY() as id
				</cfcase>
				<cfcase value="PostgreSQL">
					RETURNING bloguserid as id;
				</cfcase>
			</cfswitch>
		</cfquery>
		<cfswitch expression="#application.dbtype#">
			<cfcase value="mysql">
				<cfquery name="in" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
					SELECT LAST_INSERT_ID() as id
				</cfquery>
			</cfcase>
		</cfswitch>
		</cflock>
		<cfset url.id = in.id>
	</cfif>
	<!--- **************************************************** --->
	<!--- update                                               --->
	<!--- **************************************************** --->
	<cfquery name="update" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		update bloguser set 
			blogusername 	= <cfqueryparam cfsqltype="cf_sql_varchar" value="#trim(form.blogusername)#">,
			email 			= <cfqueryparam cfsqltype="cf_sql_varchar" value="#trim(form.email)#">,
			description 	= <cfqueryparam cfsqltype="cf_sql_varchar" value="#trim(form.description)#">,
			lastupdated		= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(now())#">,
			unlockkey		= NULL,
			retrycount		= 0,
			updatedBy		= #session.id#
			where bloguserid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">
	</cfquery>
	<!--- **************************************************** --->
	<!--- update email                                         --->
	<!--- **************************************************** --->
	<cfquery name="chk" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		select bloguserid from bloguser where
		email 	= <cfqueryparam cfsqltype="cf_sql_varchar" value="#trim(form.email)#"> and
		bloguserid <> <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">
	</cfquery>
	<cfif chk.recordCount>
		<cfset session.error = "Email Address already exist">
	<cfelse>
		<cfquery name="update" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			update bloguser set
			email		= <cfqueryparam cfsqltype="cf_sql_varchar" value="#trim(form.email)#">,
			updatedBy	= #session.id#
			where bloguserid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">
		</cfquery>	
	</cfif>
	</cftransaction>
</cfif>

<fm:image path="#application.wh#users">