<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">
<ct:quickCheck>
<cfif val(url.id)>
	<cfquery name="chk" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		select bloguser from blogpost
		where bloguser = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#"> 
	</cfquery>
	
	<cfif chk.recordCount>
		<cfquery name="del" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			update bloguser set
			valid = <cfqueryparam cfsqltype="cf_sql_bit" value="0">
			where bloguserid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">
		</cfquery>	
	<cfelse>
		<cfquery name="del" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
			delete from bloguser  
			where bloguserid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#"> 
		</cfquery>
	</cfif>
</cfif>
<cfset session.m	= "Editor Removed">
<cflocation addtoken="no" url="#application.ork.uf('switch=home','id')#">