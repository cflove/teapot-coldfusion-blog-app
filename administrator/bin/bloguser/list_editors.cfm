<cfquery name="master" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select bloguserid from bloguser where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
	<cfif len(url.srch)>
		and (blogusername like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#trim(url.srch)#%"> or
			username like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#trim(url.srch)#%"> or
			email like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#trim(url.srch)#%"> or
			description like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#trim(url.srch)#%">)
	</cfif>
	<cfif len(url.order)>
		order by #Decrypt(url.order, application.key, 'AES', 'hex')# 
		<cfswitch expression="#url.dir#"><cfcase value="1">desc</cfcase><cfdefaultcase>asc</cfdefaultcase></cfswitch>
	<cfelse>
		ORDER BY bloguserid desc
	</cfif>
</cfquery>

<cfset searchlist	= "#valuelist(master.bloguserid)#">
<cfinclude template="../../system/library/grid_calc.cfm">

<cfquery name="get" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	SELECT bloguser.bloguserid, bloguser.blogusername, bloguser.username, bloguser.email, bloguser.valid, count(blogpostid) as postcount
	FROM bloguser LEFT OUTER JOIN
	blogpost ON bloguser.bloguserid = bloguser
	where <cfif listlen(searchlist)>bloguserid in (#searchlist#)<cfelse>bloguserid = 0</cfif>
	group by bloguser.bloguserid, bloguser.blogusername, bloguser.username, bloguser.email, bloguser.valid
	<cfif len(url.order)>
		order by #Decrypt(url.order, application.key, 'AES', 'hex')# 
		<cfswitch expression="#url.dir#"><cfcase value="1">desc</cfcase><cfdefaultcase>asc</cfdefaultcase></cfswitch>
	<cfelse>
		ORDER BY bloguserid desc
	</cfif>
</cfquery>

<cfset titlelist = "User Name|blogusername,Name|username,Email|email,## of Posts">
<cfinclude template="../../system/library/grid_header.cfm">

<cfoutput query="get">
<tr <cfif not currentrow mod 2>class="zibrablk"<cfelse>class="zibrawhite"</cfif>>
	<td><a href="#application.ork.uf('switch=edit&id=#bloguserid#')#"><img border="0" src="images/file#valid#.gif" align="absmiddle" /> #blogusername#</a></td>
	<td> #username#</td>
	<td>#email#</td>
	<td>#postcount#</td>
</tr>
</cfoutput>

</table>
<cfinclude template="../../system/library/grid_navi.cfm">
</div>