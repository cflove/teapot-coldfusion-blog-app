<div style="padding:10px">
<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">

<ct:tabbox
	tabs	= "Editors,New Editor">
	<cfswitch expression="#url.tab#">
    	<cfcase value="2">
        	<cfinclude template="edit_editor.cfm">
        </cfcase>
    	<cfdefaultcase>
			<cfset url.id = 0>
        	<cfinclude template="list_editors.cfm">
        </cfdefaultcase>
    </cfswitch>
</ct:tabbox>
<br /><br />
</div>

