<cfprocessingdirective pageEncoding="utf-8">

<cfswitch expression="#url.switch#">
	<cfcase value="delete">
		<cfinclude template="act_delete.cfm">
	</cfcase>
	<cfcase value="idelete">
		<cfinclude template="act_idelete.cfm">
		<cfinclude template="act_about.cfm">
	</cfcase>
	<cfcase value="add">
		<cfinclude template="qry_add.cfm">
		<cfinclude template="act_about.cfm">
	</cfcase>
	<cfcase value="edit">
		<cfinclude template="tab_edit.cfm">
	</cfcase>
	<cfdefaultcase>
		<cfinclude template="tab_users.cfm">
	</cfdefaultcase>
</cfswitch>
