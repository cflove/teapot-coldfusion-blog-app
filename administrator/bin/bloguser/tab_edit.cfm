<div style="padding:10px">
<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">

<ct:tabbox
	tabs	= "Edit User,Editors,New Editor">
	<cfswitch expression="#url.tab#">
    	<cfcase value="3">
        	<cflocation addtoken="no" url="#application.ork.uf('switch=post&tab=2','id')#">
        </cfcase>
    	<cfcase value="2">
        	<cflocation addtoken="no" url="#application.ork.uf('switch=post&tab=1','id')#">
        </cfcase>
    	<cfdefaultcase>
			<cfinclude template="edit_editor.cfm">
        </cfdefaultcase>
    </cfswitch>
</ct:tabbox>
<br /><br />
</div>

