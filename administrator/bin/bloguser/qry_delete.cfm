<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">
<ct:quickCheck>
<!--- **************************************************** --->
<!--- delete record, set valid off                         --->
<!--- **************************************************** --->
<cfquery name="maxid" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	update bloguser set
	valid = <cfqueryparam cfsqltype="cf_sql_bit" value="0">
	where bloguserid = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(url.id)#">
</cfquery>

<cfset session.msg = "Removed Successfully.">
<cflocation addtoken="no" url="#application.ork.uf('switch=new','id')#">
