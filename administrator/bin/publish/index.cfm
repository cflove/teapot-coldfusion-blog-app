<cfimport 
	taglib =	"../../system/ct"  
	prefix =	"ct">
<ct:quickCheck>
<div style="padding:10px" id="republish">
	<a href="javascript:republish()"><img src="images/refresh.png" border="0" align="absmiddle" /> Republish The Entire Site.</a>
</div>

<cfif YesNoFormat(Application.sti.cfindex)>
<div style="padding:10px" id="reindex">
	<a href="javascript:reindex()"><img src="images/search.png" border="0" align="absmiddle" /> Rebuild Site Search Index.</a>
</div>
</cfif>

<div style="padding:10px" id="submitsite">
	<a href="javascript:submitsite()"><img src="images/globe.png" border="0" align="absmiddle" /> Publish to search engines.</a>
	<div class="tinystat" style="padding:5px"></div>
</div>

<script type="text/javascript">
	function republish() {
	$('#republish img').attr('src','images/loading.gif')
		$.ajax({url: cfc + "?method=republish&returnformat=plain", dataType: "text", cache:false, success: function(data){ 
			$('#republish').html(data)
		}})
	}
	
	function reindex() {
		$('#reindex img').attr('src','images/loading.gif')
		$.ajax({url: cfc + "?method=reindex&returnformat=plain", dataType: "text", cache:false, success: function(data){ 
			$('#reindex').html(data)
		}})
	}
	
	function submitsite() {
		$('#submitsite img').attr('src','images/loading.gif')
		$.ajax({url: cfc + "?method=submitsite&returnformat=plain", dataType: "text", cache:false, success: function(data){ 
			$('#submitsite img').attr('src','images/globe.png')
			$('#submitsite .tinystat').html(data)
		}})
	}
</script>