<cfset q 	= attributes.q>
<cfif not val(q.total)>
	<div style="padding:10px; font-weight:bold">No Records Found</div>
</cfif>
<div class="navi">
	<cfset pagecount	= Ceiling(q.total/q.shown)>
    <cfif pagecount gt 1>

    	<cfoutput>
        <div id="gnavbttnbox" class="gnavbttnbox">
            <select class="gselector" id="gselector" name="gselector" onchange="window.location='#application.ork.uf('','page,id')#shown='+$('gselector').value">
            	<option value="10">10</option>
                <cfloop from="20" to="100" step="20" index="i"><cfif i lte (Ceiling(q.total/10)*10)><option <cfif shown eq i>selected="selected"</cfif> value="#i#">#i#</option></cfif></cfloop>
            </select>
            <cfif q.thispage gt 1>
            	<a href="#application.ork.uf('page=#val(q.thispage-1)#','id')#"><img src="images/gbck.gif" style="position:relative; top:3px" border="0" width="14" height="14" /></a>
            </cfif>
            
            <!--- limit button set calculation : Start --->
            <cfset Start = (Ceiling(q.thispage/10)*10)-9>
            <cfset end	 = (Ceiling(q.thispage/10)*10)>
            <cfif end gt pagecount>
            	<cfset end = pagecount>
            </cfif>
            <!--- limit button set calculation : Ends  --->
            
            <cfloop from="#Start#" to="#end#" index="i">
                <cfif q.thispage eq i> <u>#i#</u><cfelse> <a href="#application.ork.uf('page=#i#','id')#">#i#</a></cfif> <img src="images/gdiv.gif" width="2" height="9" />
            </cfloop>
            <cfif pagecount gt q.thispage>
            	 <a href="#application.ork.uf('page=#val(q.thispage+1)#','id')#"><img src="images/gnxt.gif" style="position:relative; top:3px" border="0" width="14" height="14" /></a> 
            </cfif>
            <span class="pageline">Page <input type="text" id="gpagejump" class="gpagejump" onkeypress="{if (event.keyCode==13) window.location='#application.ork.uf('','page,id')#page='+$('gpagejump').value}"  name="gpagejump" value="#url.page#" /> of #pagecount#
            (#q.total# Records)&nbsp;</span>
        </div>
    </cfoutput>
	<cfelse>
    
    </cfif>
    <cfoutput><div id="gnavsrchbox" class="gnavsrchbox">
    <cfif len(url.srch)>
    	<a href="#application.ork.uf('fs=1','page,srch,id')#"><img src="images/gryclose.gif" style="position:relative; top:3px" border="0" width="15" height="15" /></a>
    </cfif>
    <input type="text" class="gsearch" id="gsearch" value="#url.srch#" name="gsearch" onkeypress="{if (event.keyCode==13) window.location='#application.ork.uf('fs=1','page,id')#srch='+$('gsearch').value}" /> <input type="button" onclick="window.location='#application.ork.uf('fs=1','page,id')#srch='+$('gsearch').value" class="gbtn" value="Search" /></div></cfoutput>

</div>