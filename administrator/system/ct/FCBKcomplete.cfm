<cfparam name="attributes.data"    			default=""> 
<cfparam name="attributes.cache"    		default="false"> 
<cfparam name="attributes.filter_case"    	default="false"> 
<cfparam name="attributes.filter_hide"    	default="true"> 
<cfparam name="attributes.filter_selected" 	default="true"> 
<cfparam name="attributes.firstselected" 	default="true"> 
<cfparam name="attributes.jsfolder"    		default="js/jquery/FCBKcomplete"> 
<cfparam name="attributes.newel"		 	default="true"> 
<cfparam name="attributes.complete_text" 	default="">

<cfswitch expression="#thisTag.ExecutionMode#"> 
<cfcase value="start"> 
<cfif not StructKeyExists(request,'listSelectID')> 
<cfset request.listSelectID = 1> 
<cfoutput> 
<cfsavecontent variable="script"> 
	<link rel="stylesheet" href="#attributes.jsfolder#/style.css" type="text/css" media="screen" charset="utf-8" /> 
	<script src="#attributes.jsfolder#/jquery.fcbkcomplete.min.js" type="text/javascript" charset="utf-8"></script> 
</cfsavecontent> 
</cfoutput> 
<cfhtmlhead text="#script#"> 
<cfelse> 
<cfset request.listSelectID = 1+request.listSelectID> 
</cfif> 
<cfoutput><span id="listSelect#request.listSelectID#"></cfoutput> 
</cfcase> 

<cfdefaultcase> 
</span> 
<cfsavecontent variable="script"> 
<script type="text/javascript"> 
$(document).ready(function() { 
<cfoutput>$("##listSelect#request.listSelectID# select").fcbkcomplete({ 
json_url: "#attributes.data#", 
cache:#attributes.cache#,filter_case:#attributes.filter_case#,filter_hide:#attributes.filter_hide#,complete_text:'#attributes.complete_text#',filter_selected:#attributes.filter_selected#,firstselected:#attributes.firstselected#,newel:#attributes.newel# 
}); 
</cfoutput> 
var w = $('.holder').outerWidth() 
$('.facebook-auto').width( w ) 
$('.facebook-auto ul').width( w ) 
}) 
</script> 
</cfsavecontent> 
<cfhtmlhead text="#script#"> 
</cfdefaultcase> 
</cfswitch> 