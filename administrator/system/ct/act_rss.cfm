<!---- **************************************************** ---->
<!---- create RSS Feed                                      ---->
<!---- **************************************************** ---->
<cfinclude template="../../../#application.warehouse#/widgets/settings/RSS.cfm">
<cfparam name="attributes.folder" default="#ExpandPath('../')#">
<cfif val(widget.RSSPostCount)>
<cfsavecontent variable="xml">
<rss version="2.0" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:cc="http://web.resource.org/cc/" >
<cfset gmt = DateAdd('s',GetTimeZoneInfo().UTCTotalOffset,now())>
<channel><cfoutput>
<title><![CDATA[#Application.sti.sitename#]]></title>
<link>#application.root#</link>
<description><![CDATA[#Application.sti.sitedesc#]]></description>
<language>en-us</language>
<pubDate>#dateformat(gmt,'ddd, dd mmm yyyy')# #timeformat(gmt,'HH:mm:ss')#</pubDate>
<lastBuildDate>#dateformat(gmt,'ddd, dd mmm yyyy')# #timeformat(gmt,'HH:mm:ss')#</lastBuildDate>
<generator>cflove.SimpleBlog</generator>
<docs>http://blogs.law.harvard.edu/tech/rss</docs>
<managingEditor>#application.sti.MailFrom#</managingEditor>
<webMaster>#application.sti.MailFrom#</webMaster></cfoutput><cfsilent>
<cfquery name="q" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select top #widget.RSSPostCount# blogpostid, blogpostname, created, url, <cfif YesNoFormat(widget.FullDescription)>blog<cfelse>smalldes</cfif> as description
	from blogpost
	where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
	order by blogpostid desc
</cfquery></cfsilent>
<cfoutput query="q"><cfsilent>
	<cfquery name="c" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		SELECT bloglabel.bloglabelname
		FROM blogpost_bloglabel INNER JOIN
		bloglabel ON blogpost_bloglabel.bloglabelid = bloglabel.bloglabelid
		where blogpostid = #q.blogpostid# and valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
	</cfquery></cfsilent>
<item>
<title><![CDATA[#blogpostname#]]></title>
<link>#application.root##DateFormat(created,'yyyy')#/#DateFormat(created,'mm')#/#q.url#.cfm</link>
<description><![CDATA[#rereplacenocase(description,'<cf:execute.*?</cf:execute*.>','','all')#]]></description>
<cfloop query="c"><category><![CDATA[#c.bloglabelname#]]></category></cfloop>
<cfset gmt = DateAdd('s',GetTimeZoneInfo().UTCTotalOffset,now())>
<pubDate>#dateformat(gmt,'ddd, dd mmm yyyy')# #timeformat(gmt,'HH:mm:ss')#</pubDate>
<guid>#application.root##DateFormat(created,'yyyy')#/#DateFormat(created,'mm')#/#q.url#.cfm</guid>
</item>
</cfoutput>
</channel>
</rss>
</cfsavecontent>
<cfset xml = XMLParse(xml)>
<cfset xml = ToString(xml)>

<cffile 
	action			="write"  
	nameconflict	="overwrite" 
	charset			="utf-8"
	file			="#attributes.folder#/rss.xml" 
	output			="#xml#">
<!--- ping google blog search --->
<cfhttp url="http://blogsearch.google.com/ping?name=#URLEncodedFormat(Application.sti.sitename)#&url=#URLEncodedFormat(application.root)#&changesURL=#URLEncodedFormat(application.root)#rss.xml"></cfhttp>

<cfelse>
	<cfif FileExists("#attributes.folder#rss.xml")>
		<cffile action="delete" file="#attributes.folder#rss.xml">
	</cfif>
</cfif>

<!---- **************************************************** ---->
<!---- create site map XML                                  ---->
<!---- **************************************************** ---->

<cfquery name="q" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select created, url,lastupdated from blogpost where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1"> 
	order by blogpostid desc
</cfquery>

<cfoutput>
<cfsavecontent variable="sitefeed">
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<url><loc>#application.root#</loc></url><url><loc>#application.root#index.cfm</loc></url><cfloop query="q"><url><loc>#application.root##DateFormat(created,'yyyy')#/#DateFormat(created,'mm')#/#q.url#.cfm</loc><lastmod>#dateformat(q.lastupdated,'YYYY-MM-DD')#</lastmod></url></cfloop>
</urlset>
</cfsavecontent>
</cfoutput>

<cffile
	action 		= "write"
	file 		= "#attributes.folder#sitemap.xml"
	output 		= "#trim(sitefeed)#"
	addNewLine 	= "No" 
	charset 	= "utf-8" >