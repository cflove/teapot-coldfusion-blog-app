<cfif IsDefined('attributes.q')>
	<cfset q 	= attributes.q>
<cfelse>
	<cfquery name="q" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
		select #request.this.table#id as id from #request.this.table#
		<cfif len(url.srch)>
			where (#request.this.table#name like '%#url.srch#%')
		</cfif>
		order by #request.this.table#id desc
	</cfquery>
</cfif>

<cfset grid	= StructNew()>
<cfset grid.searchlist	= valuelist(q.id)>

<cfif val(url.shown)>
	<cfcookie name="resPerPage" value="#url.shown#" expires="never">
</cfif>
<cfset grid.shown		= cookie.resPerPage>
<cfset grid.thispage 	= url.page>
<cfset grid.total 		= q.recordCount>

<cfset grid.getlist	= ''>
<cfset grid.endpage	= grid.thispage*grid.shown>
<cfset grid.stpage	= grid.endpage-grid.shown+1>
<cfif grid.endpage gt grid.total>
	<cfset grid.endpage = grid.total>
</cfif>

<cfloop from="#grid.stpage#" to="#grid.endpage#" index="i"> 
    <cfset grid.getlist	= ListAppend(grid.getlist, listgetat(grid.searchlist,i))>
</cfloop>
<cfset grid.searchlist = grid.getlist>

<cfset caller.grid = grid>