<cf_quickCheck>
<cfparam name="attributes.folder" 			default="#ExpandPath('../')#">
<cfparam name="attributes.ID"	 			default="">
<cfparam name="attributes.makeCollection" 	default="Yes">
<cfparam name="attributes.action"	 		default="">
<!--- **************************************************** --->
<!--- create search collection                             --->
<!--- **************************************************** --->
<cfif YesNoFormat(Application.sti.cfindex)>
	<cfif YesNoFormat(attributes.makeCollection)>
		<cftry>
			<cfcollection action= "list" name="collectionlist">
			<cfcatch>
				<div class="bigbox">
				<b>Oh My! cfcollection isn't running.</b><br /><br />
				Teapot can't build a search index. Please go to <i>Site > Settings > Advance Settings</i> and disable 'ColdFusion Search indexing'.
				</div>
				<cfabort>
			</cfcatch>
		</cftry>
		<cfif not listfindnocase(valuelist(collectionlist.name),application.ds)>
			<cfcollection action="create" collection="#application.ds#" path="#application.wh#">
		</cfif>
	</cfif>
	
	<cfswitch expression="#attributes.action#">
		<cfcase value="delete">
			<cfindex
				collection	= "#application.ds#"
				action		= "delete"
				type		= "custom"
				key			= "#attributes.ID#">
		</cfcase>
		<cfdefaultcase>
			<cfquery name="thispostfull" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
				SELECT blogpost.blogpostid, blogpost.smalldes, blogpost.blog, comments.comments, blogpost.blogpostname
				FROM blogpost LEFT OUTER JOIN
				comments ON blogpost.blogpostid = comments.blogpostid
				WHERE (blogpost.valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">  )
				<cfif val(attributes.id)>
					and blogpost.blogpostid = #attributes.id#
				</cfif>
			</cfquery>
			<cfif thispostfull.recordCount>
				<cfindex
					collection	= "#application.ds#"
					action		= "update"
					type		= "custom" 
					body		= "comments, blog, smalldes, blogpostname" 
					query		= "thispostfull"
					key			= "blogpostid">
			</cfif>
		</cfdefaultcase>
	</cfswitch>
</cfif>
