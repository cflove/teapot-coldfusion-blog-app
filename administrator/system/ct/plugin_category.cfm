<!--- **************************************************** --->
<!--- Category                                             --->
<!--- **************************************************** --->
<cfinclude template="../../../#application.warehouse#/widgets/settings/Pages.cfm">
<cfquery name="pageM" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select blogpostid, blogpostname, created,url from blogpost 
	where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1"> and mblogpostid = 0 
	and pagetype = <cfqueryparam cfsqltype="cf_sql_tinyint" value="2">
	order by orderby, blogpostname, mblogpostid
</cfquery>

<cfsavecontent variable="catsave">
<cfif pageM.recordCount>
[cfprocessingdirective pageEncoding="utf-8">[cfinclude template="settings/Pages.cfm"><ul class="list" id="PagesWidget"><li class="title">[cfoutput>#widget.title#[/cfoutput></li>
<cfoutput query="pageM"><li class="item"><a href="#application.root##DateFormat(created,'yyyy')#/#DateFormat(created,'mm')#/#pageM.url#.cfm">#blogpostname#</a>
<cfquery name="pageS" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
	select blogpostid, blogpostname, created,url 
	from blogpost 
	where valid = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
	and mblogpostid = #val(pageM.blogpostid)# 
	and pagetype = <cfqueryparam cfsqltype="cf_sql_tinyint" value="2">
	order by orderby, blogpostname, mblogpostid
</cfquery>
<cfif pageS.recordCount>
<ul class="subset">
<cfloop query="pageS"><li class="sub"><a href="#application.root##DateFormat(pageS.created,'yyyy')#/#DateFormat(pageS.created,'mm')#/#pageS.url#.cfm">#pageS.blogpostname#</a></li></cfloop>
</ul>
</cfif>
</li></cfoutput></ul></cfif>
</cfsavecontent>

<cfset catsave = replace(catsave,'[cf','<cf','all')>
<cfset catsave = replace(catsave,'[/cf','</cf','all')>
<cffile
	action 		= "write"
	mode		= "#application.chomd#"
	file 		= "#application.wh#widgets/Pages.cfm"
	output 		= "#trim(catsave)#"
	addNewLine 	= "No" 
	charset 	= "utf-8" >
