<cfsilent>
<cfparam name="attributes.password" 	default="">
<cfparam name="attributes.salt" 		default="">

<cfif not len(attributes.salt)>
	<cfset attributes.salt = CreateUUID()>
</cfif>

<cfset attributes.password	= hash("#trim(attributes.password)##attributes.salt##application.salt#","SHA-256")>
<cfset caller.passwordenc 	= attributes.password>
<cfset caller.salt 			= attributes.salt>
</cfsilent>