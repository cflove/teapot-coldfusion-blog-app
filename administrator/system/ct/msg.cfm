<cfif StructKeyExists(session,'error')>
	<div id="errmsg"><img src="images/rdleft.gif" align="absmiddle" />&nbsp; <cfoutput>#JSStringFormat(session.error)#</cfoutput>&nbsp; <img src="images/rdrite.gif" align="absmiddle" /></div>
	<cfset StructDelete(session, "error")>
</cfif>
<cfif StructKeyExists(session,'msg')>
	<div id="msg"><img src="images/blulft.gif" align="absmiddle" />&nbsp; <cfoutput>#JSStringFormat(session.msg)#</cfoutput>&nbsp; <img src="images/blurite.gif" align="absmiddle" /></div>
	<cfset StructDelete(session, "msg")>
</cfif>