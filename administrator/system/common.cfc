<cfcomponent>

<!--- ******************************************************** --->
<!--- handle URL values                                        --->
<!--- ******************************************************** --->
<cffunction	
	name 		= "uf"
	output 		= "No"
	displayName = "URL Format"	
	description = "use for generate link url with current url valuues">

<cfargument name="add" 		required="no" 	type="string" 	default=""		hint="URL Query String Part to be attached">
<cfargument name="remove" 	required="no" 	type="string" 	default=""		hint="URL values to be Removed">
<cfargument name="crypt" 	required="no" 	type="boolean" 	default="No"	hint="Encrypt the URL or not">

	<cfset  local		= StructNew()>
	<cfset local.string		= "&path=#url.path#&page=#url.page#&order=#url.order#&dir=#url.dir#&switch=#url.switch#&id=#url.id#&id2=#url.id2#&">
	<!--- *********************************************************** --->
	<!--- Remove ADD values from the main string before attach them   --->
	<!--- *********************************************************** --->
	<cfloop list="#arguments.add#" index="local.i" delimiters="&">
		<cfset key			= listfirst(local.i,'=')>
		<cfset local.string	= REReplaceNoCase(local.string,"\&#trim(key)#\=[^\&]*","","ALL")> 
	</cfloop>

	<!--- *********************************************************** --->
	<!--- Remove values developer wanted to be removed                --->
	<!--- *********************************************************** --->
	<cfloop list="#arguments.remove#" index="local.ii">
		<cfset local.string	= REReplaceNoCase(local.string,"\&#trim(local.ii)#\=[^\&]*","","ALL")> 
	</cfloop>
	
	<!--- *********************************************************** --->
	<!--- remove variable that have no values                         --->
	<!--- *********************************************************** --->
	<cfset local.string	= REReplaceNoCase(local.string,"[^\&]*\=\&","","ALL")>
	<!--- *********************************************************** --->
	<!--- attach Add String                                           --->
	<!--- *********************************************************** --->
	<cfset local.string = '#arguments.add##local.string#'>

	<!--- *********************************************************** --->
	<!--- Encrypt the complete URL string.                            --->
	<!--- *********************************************************** --->
	<cfif YesNoFormat(arguments.crypt)>
		<cfset local.string = Encrypt(local.string, application.key, 'AES', 'hex')>
	</cfif>
	<cfset local.string	= "index.cfm?crypt=#arguments.crypt#&#local.string#">
	
	<cfreturn local.string>
</cffunction>

<!--- ******************************************************** --->
<!--- get xml node from menu.cfm xml file                      --->
<!--- ******************************************************** --->
<cffunction name="xmlnode" 	output="false" 	returntype="any" access="remote" hint="Return XML Node">
	<cfargument name="xmlpos"	required="yes" 	type="any" default="#url.path#">
		
		<cfif not IsArray(xmlpos)>
			<cfset arguments.xmlpos = ListToArray(arguments.xmlpos)>
		</cfif>
		
		<cffile action="read" file="#ExpandPath('system/')#menu.cfm" variable="tree">
		<cfset local.tree = XMLParse(tree,'false').cfsilent>
		<cfloop index="i" from="1" to="#ArrayLen(arguments.xmlpos)#">
			<cfset local.tree = local.tree.XmlChildren[arguments.xmlpos[i]]>
		</cfloop>
	<cfreturn local.tree>
</cffunction>

<!--- ******************************************************** --->
<!--- format string in to a search engin friendly value        --->
<!--- ******************************************************** --->
<cffunction name="urlf" access="public" output="false" returnType="string" hint="UDF that returns an SEO friendly string.">
	<cfargument name="title" 	type="string" required="yes">
    <cfargument name="default" 	type="string" required="no" default="News-Post">
    <cfargument name="lenth" 	type="string" required="no" default="150">

	<cfset title = replaceNoCase(title,"&amp;","and","all")>
    <cfset title = replaceNoCase(title,"&","and","all")>
    <cfset title = reReplaceNoCase(trim(title),"[^a-zA-Z1-9\- ]","","ALL")>
	<cfset title = reReplaceNoCase(title,"  "," ","all")>
    <cfset title = left(title,lenth)>
    <cfset title = reReplaceNoCase(trim(title)," ","-","all")>
	<cfif not len(title)>
    	<cfset title	= default>
    </cfif>
    
	<cfreturn lcase(title)>
</cffunction>


<!--- http://cflib.org/udf/FullLeft --->
<cfscript>
function fullLeft(str, count) {
	if (not refind("[[:space:]]", str) or (count gte len(str)))
		return Left(str, count);
	else if(reFind("[[:space:]]",mid(str,count+1,1))) {
	  	return left(str,count);
	} else { 
		if(count-refind("[[:space:]]", reverse(mid(str,1,count)))) return Left(str, (count-refind("[[:space:]]", reverse(mid(str,1,count))))); 
		else return(left(str,1));
	}
}

//http://www.cflib.org/udf/ActivateURL
function ActivateURL(string) {
    var nextMatch = 1;
    var objMatch = "";
    var outstring = "";
    var thisURL = "";
    var thisLink = "";
    var    target = IIf(arrayLen(arguments) gte 2, "arguments[2]", DE(""));
    var paragraph = IIf(arrayLen(arguments) gte 3, "arguments[3]", DE("false"));
    
    do {
        objMatch = REFindNoCase("(((https?:|ftp:|gopher:)\/\/)|(www\.|ftp\.))[-[:alnum:]\?%,\.\/&##!;@:=\+~_]+[A-Za-z0-9\/]", string, nextMatch, true);
        if (objMatch.pos[1] GT nextMatch OR objMatch.pos[1] EQ nextMatch) {
            outString = outString & Mid(String, nextMatch, objMatch.pos[1] - nextMatch);
        } else {
            outString = outString & Mid(String, nextMatch, Len(string));
        }
        nextMatch = objMatch.pos[1] + objMatch.len[1];
        if (ArrayLen(objMatch.pos) GT 1) {
            // If the preceding character is an @, assume this is an e-mail address
            // (for addresses like admin@ftp.cdrom.com)
            if (Compare(Mid(String, Max(objMatch.pos[1] - 1, 1), 1), "@") NEQ 0) {
                thisURL = Mid(String, objMatch.pos[1], objMatch.len[1]);
                thisLink = "<A HREF=""";
                switch (LCase(Mid(String, objMatch.pos[2], objMatch.len[2]))) {
                    case "www.": {
                        thisLink = thisLink & "http://";
                        break;
                    }
                    case "ftp.": {
                        thisLink = thisLink & "ftp://";
                        break;
                    }
                }
                thisLink = thisLink & thisURL & """";
                if (Len(Target) GT 0) {
                    thisLink = thisLink & " TARGET=""" & Target & """";
                }
                thisLink = thisLink & ">" & thisURL & "</A>";
                outString = outString & thisLink;
                // String = Replace(String, thisURL, thisLink);
                // nextMatch = nextMatch + Len(thisURL);
            } else {
                outString = outString & Mid(String, objMatch.pos[1], objMatch.len[1]);
            }
        }
    } while (nextMatch GT 0);
        
    // Now turn e-mail addresses into mailto: links.
    outString = REReplace(outString, "([[:alnum:]_\.\-]+@([[:alnum:]_\.\-]+\.)+[[:alpha:]]{2,4})", "<A HREF=""mailto:\1"">\1</A>", "ALL");
        
    if (paragraph) {
        outString = ParagraphFormat(outString);
    }
    return outString;
}

</cfscript>

</cfcomponent>