<cfsilent>
<cfparam name="url.path" 			default="1">
<cfparam name="url.header" 			default="Yes"> 	<!--- show header - use in popups --->
<cfparam name="cookie.resPerPage"	default="10">   <!--- number of search results per a page --->
<cfparam name="url.page"			default="1"> 	<!--- search result page --->
<cfparam name="url.order" 			default="">		<!--- search result display order --->
<cfparam name="url.dir" 			default="">		<!--- search result display direction --->
<cfparam name="url.srch" 			default="">		<!--- search key --->
<cfparam name="url.shown" 			default="">		<!--- grid : number of resutls --->
<cfparam name="url.tab" 			default="1">	<!--- item tabs --->
<cfparam name="url.id" 				default="">
<cfparam name="url.id2" 			default="">
<cfparam name="url.switch" 			default="">		<!--- use for switch inside folders --->
<cfif not StructKeyExists(application,'ork')>
	<cfset application.ork 	= CreateObject("Component", "common")>
</cfif>
</cfsilent>