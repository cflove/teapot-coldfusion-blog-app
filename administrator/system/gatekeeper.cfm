<cfsilent><!---and not listlast(GetCurrentTemplatePath(),'/\') eq 'login.cfm'--->
<cfif not StructKeyExists(cookie,'blogid')>
	<cfif len(cgi.QUERY_STRING)>
		<cfset session.msg	= "Session Expired. Please Login.">
	</cfif>
	<cflocation addtoken="no" url="login.cfm?r=#Encrypt('#cgi.QUERY_STRING# ', application.key, 'AES', 'hex')#">
<cfelse>
	<cfif not StructKeyExists(session,'id')>
		<cftry>
			<cfquery name="getu" datasource="#application.ds#" username="#application.un#" password="#application.pw#">
				select bloguserid from bloguser where cookiekey = '#Decrypt(cookie.blogid, application.key, 'AES', 'hex')#'
			</cfquery>
			<cfif getu.recordCount>
				<cfset session.id		= getu.bloguserid>
			<cfelse>
				<cflocation addtoken="no" url="login.cfm?r=#Encrypt('#cgi.QUERY_STRING# ', application.key, 'AES', 'hex')#">
			</cfif>
			<cfcatch>
				<cfset StructDelete(cookie,'blogid')>
				<cflocation addtoken="no" url="login.cfm">
			</cfcatch>
		</cftry>
	</cfif>
</cfif>
</cfsilent>