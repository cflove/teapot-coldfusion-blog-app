<script type="text/javascript">
$(function(){
	$('#password').keyup(function(){
		var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
		var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
		var enoughRegex = new RegExp("(?=.{4,}).*", "g");
		if ( $(this).val() !== '' ) {
			$('#swstring').show('slow')
		} else {
			$('#swstring').hide('slow')
		}
		if( false == enoughRegex.test( $(this).val() ) )
		{
			$('#pwst1').attr('src','images/graydot.gif')
			$('#pwst2').attr('src','images/graydot.gif')
			$('#pwst3').attr('src','images/graydot.gif')
			$('#pwst').html('very weak')
		}
		else if( strongRegex.test( $(this).val() ) )
		{
			$('#pwst1').attr('src','images/organgedot.gif')
			$('#pwst2').attr('src','images/organgedot.gif')
			$('#pwst3').attr('src','images/organgedot.gif')
			$('#pwst').html('strong')
		}
		else if( mediumRegex.test( $(this).val() ) )
		{
			$('#pwst1').attr('src','images/organgedot.gif')
			$('#pwst2').attr('src','images/organgedot.gif')
			$('#pwst3').attr('src','images/graydot.gif')
			$('#pwst').html('medium')
		}
		else
		{
			$('#pwst1').attr('src','images/organgedot.gif')
			$('#pwst2').attr('src','images/graydot.gif')
			$('#pwst2').attr('src','images/graydot.gif')
			$('#pwst').html('weak')
		}
	})
})
</script>

<div style="clear:both; padding-left:5px; padding-top:5px; color:#666">
<img id="pwst1" src="images/graydot.gif" /> <img id="pwst2" src="images/graydot.gif" /> <img id="pwst3" src="images/graydot.gif" /> <span id="swstring" style="display:none">password strength is <span id="pwst"></span></span>
</div>