<cfparam name="attributes.field" 			default="file">
<cfparam name="attributes.save" 			default="#url.id#.jpg">
<cfparam name="attributes.path" 			default="#application.wh#">
<cfparam name="attributes.types" 			default="jpg">
<cfparam name="attributes.width" 			default="78">
<cfparam name="attributes.hight" 			default="78">
<cfparam name="attributes.largewidth" 		default="700">
<cfparam name="attributes.largehight" 		default="700">

<cfif len(Evaluate('form.#attributes.field#'))>
<cffile
    action 			= "upload"
    fileField 		= "form.#attributes.field#" 
    nameconflict	= "makeunique"
	mode			= "#application.chomd#"
    destination 	= "#attributes.path#">
	
<cfif not ListFindNoCase(attributes.types,file.clientFileExt)>
	<cfset session.error	= "Error Uploading file. The File You Upload is #ucase(clientFileExt)#. Please upload #ucase(attributes.types)# Files Only.">
	<cffile
		action 	= "delete"
		file 	= "#attributes.path#/#cffile.serverFile#">
<cfelse>
	<!--- ******************************************************************** --->
	<!--- Create Thumb Image                                                   --->
	<!--- ******************************************************************** --->
	<cfif len(attributes.width) and (attributes.hight)>
		<cftry>
		<cfif listfirst(server.coldfusion.productversion) gte 8>
			<cfset myImage	= ImageRead('#attributes.path#/#cffile.serverFile#')>
			<cfset ImageScaleToFit(myImage,attributes.width,attributes.hight)>
			<cfset ImageWrite(myImage,'#attributes.path#/thumb_#attributes.save#')>
		<cfelse>
			<cfset myImage = CreateObject("Component", "iEdit")>
			<cfset myImage.SelectImage("#attributes.path#/#cffile.serverFile#")>
			<cfset myImage.ScaletoFit(attributes.width,attributes.hight)>
			<cfset myImage.output("#attributes.path#/thumb_#attributes.save#", "jpg",100)>
		</cfif>
		<cfcatch>
			<cfset session.error ="Thumbnail creation Failed. [#cfcatch.Message#]">
		</cfcatch>
		</cftry>
	</cfif>
	<cffile
		action 		= "rename"
		source 		= "#attributes.path#/#cffile.serverFile#"
		destination = "#attributes.path#/#attributes.save#">

	<!--- ******************************************************************** --->
	<!--- Resize Large Image                                                   --->
	<!--- ******************************************************************** --->
	<cfif val(attributes.largewidth) and val(attributes.largehight)>
		<cftry>
		<cfif listfirst(server.coldfusion.productversion) gte 8>
			<cfset myImage	= ImageRead('#attributes.path#/#attributes.save#')>
			<cfset ImageScaleToFit(myImage,attributes.largewidth,attributes.largehight)>
			<cfset ImageWrite(myImage,'#attributes.path#/#attributes.save#')>
		<cfelse>
			<cfset myImage = CreateObject("Component", "iEdit")>
			<cfset myImage.SelectImage("#attributes.path#/#attributes.save#")>
			<cfset myImage.ScaletoFit(attributes.largewidth,attributes.largehight)>
			<cfset myImage.output("#attributes.path#/#attributes.save#", "jpg",100)>
		</cfif>
		<cfcatch>
			<cfset session.error ="Image Resizing Failed. [#cfcatch.Message#]">
		</cfcatch>
		</cftry>
	</cfif>
</cfif>
</cfif>