<cfprocessingdirective pageEncoding="utf-8">
<cfset request.this 		= application.ork.xmlnode().XmlAttributes>

<!--- set default switch from XML file --->
<cfif StructKeyExists(request.this,'switch') and not len(url.switch)>
	<cfset url.switch = request.this.switch>
<cfelse>
	<cfparam name="url.switch" default="">
</cfif>

<cfif StructKeyExists(request.this,'folder')>
	<cfinclude template="../bin/#request.this.folder#/index.cfm">
</cfif>