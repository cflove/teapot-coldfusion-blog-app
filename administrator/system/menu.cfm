<?xml version="1.0" encoding="utf-8"?>
<cfsilent>
	<item label="Blog">
		<item label="Posts" folder="blogpost"></item>
		<item label="Pages" folder="pages"></item>
	</item>
	<item label="Comments">
		<item label="Comments" folder="comments"></item>
	</item>
	<item label="Extras">
		<item label="Links" folder="links"></item>
	</item>
	<item label="Site">
		<item label="Editors" folder="bloguser"></item>
		<item label="Theme" folder="theme"></item>
		<item label="Widgets" folder="widgets"></item>
		<item label="Settings" folder="settings"></item>
		<item label="Publish" folder="publish"></item>
	</item>
</cfsilent>