<cfprocessingdirective pageEncoding="utf-8">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head><link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="<cfoutput>#application.root#styles/#Application.sti.layout#</cfoutput>/post.css"/>
<link rel="stylesheet" type="text/css" href="js/jquery/css/pepper-grinder/jquery-ui-1.10.0.custom.min.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<script type='text/javascript' src='js/jquery/jquery-1.9.1.min.js'></script>
<script type='text/javascript' src='js/jquery/jquery-ui-1.10.0.custom.min.js'></script>
<script type='text/javascript' src='js/jquery/jquery.tipsy.js'></script>
<script type='text/javascript' src='js/ork.js'></script>
<title><cfoutput>#Application.sti.sitename#</cfoutput></title>
</head><body>
<!--- ******************************************************* --->
<!--- Display Error/sucess messages passed from last page     --->
<!--- ******************************************************* --->
<div id="alertbox"><table onclick="hidealertbox()" cellpadding="0" cellspacing="0" border="0"><tr><td width="5"><img src="images/alrtlft.gif" /></td><td class="alertboxmsg"></td><td width="5"><img src="images/alrtrite.gif" /></td></tr></table></div>
<cfif IsDefined('session.msg') and not IsDefined('session.error')>
<script type="text/javascript">
	$("document").ready(function() { setTimeout('showmsg("<cfoutput>#JSStringFormat(session.msg)#</cfoutput>")', 1000);  })
	<cfset StructDelete(session, "msg")>
</script>
<cfelse>
	<cfif IsDefined('session.error')>
	<script type="text/javascript">
		$("document").ready(function() { setTimeout('showerror("<cfoutput>#JSStringFormat(session.error)#</cfoutput>")', 1000) })
		<cfset StructDelete(session, "error")>
	</script>
	</cfif>
</cfif>

<div id="fullbox">
<cfif YesNoFormat(url.header)>
	<cfset headerdislpay = "block">
<cfelse>
	<cfset headerdislpay = "none">
</cfif>

<div id="header" style="display:<cfoutput>#headerdislpay#</cfoutput>">
<div id="headerwrap"  style="display:<cfoutput>#headerdislpay#</cfoutput>">

<!--- ******************************************************* --->
<!--- Read the Menu XML and create a top menu tabs            --->
<!--- ******************************************************* --->
<cffile action="read" file="#ExpandPath('./system/')#menu.cfm" variable="menu">
<cfset menu = XMLParse(menu,'false').cfsilent>
<cfloop index="i" from="1" to="#ArrayLen(menu.XmlChildren)#" step="1">
<cfoutput>
	<cfif listfirst(url.path) eq i>
		<div class="toptabsel" id="toptab#i#" onclick="window.location.href = 'index.cfm?path=#i#'">
			<div class="toptablft"></div>
			<div class="toptabmid">#menu.XmlChildren[i].XmlAttributes.label#</div>
			<div class="toptabrite"></div>
		</div>
	<cfelse>
		<div class="toptab" id="toptab#i#" onclick="window.location.href = 'index.cfm?path=#i#'">
			<div class="toptablft"></div>
			<div class="toptabmid">#menu.XmlChildren[i].XmlAttributes.label#</div>
			<div class="toptabrite"></div>
		</div>
	</cfif>
</cfoutput>
</cfloop>
<div class="sitename"><cfoutput>#Application.sti.sitename#<div id="signout"><a href="login.cfm?path=logout">#cookie.name# .</cfoutput> Sign Out</a></div></div>
</div>
</div>
<div id="submenu" style="display:<cfoutput>#headerdislpay#</cfoutput>">
<!--- ******************************************************* --->
<!--- submenu(s)                                              --->
<!--- ******************************************************* --->
<cfset path2here = ArrayNew(1)>
<cfloop from="1" to="#listlen(url.path)#" index="s">

<cfset ArrayAppend(path2here,listgetat(url.path,s))>

<cfset amendedpath 	= "#url.path#,1">
<cfset itemhere		= listgetat(amendedpath,s+1)>
<cfset pathxml 		= application.ork.xmlnode(path2here)>
<cfif arraylen(pathxml.XmlChildren)>
<div id="submenubox">
	<cfloop index="i" from="1" to="#ArrayLen(pathxml.XmlChildren)#" step="1">
		<cfif i eq itemhere>
			<cfset url.path = "#ArrayToList(path2here)#,#i#">
			<cfoutput><div class="submenusel" onclick="window.location = 'index.cfm?path=#ArrayToList(path2here)#,#i#'"><img align="absmiddle" src="images/tbsellft.png" height="18" />#pathxml.XmlChildren[i].XmlAttributes.label#<img align="absmiddle" src="images/tbselrite.png" height="18" /></div></cfoutput> 
		<cfelse>
			<cfoutput><a class="submenu" href="index.cfm?path=#ArrayToList(path2here)#,#i#">#pathxml.XmlChildren[i].XmlAttributes.label#</a></cfoutput> 
		</cfif>
	</cfloop>
</div>
</cfif>
</cfloop>
</div>
<div id="menubtm" style="display:<cfoutput>#headerdislpay#</cfoutput>"></div>