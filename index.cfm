<cflocation addtoken="no" url="install"><cfprocessingdirective pageEncoding="utf-8">
<cfinclude template="teapot/library/global.cfm">

<cfswitch expression="#url.action#">
<cfcase value="search">
	<cfinclude template = "teapot/library/dsp_header.cfm">
	<cfinclude template = "teapot/search/index.cfm">
</cfcase>
<cfcase value="unsub">
	<cfinclude template="teapot/comments/unsubscribe.cfm">
</cfcase>
<cfcase value="spamcomment">
	<cfinclude template = "teapot/comments/act_commentFinal.cfm">
</cfcase>
<cfcase value="commentSubmit">
	<cfinclude template = "teapot/library/dsp_header.cfm">
	<cfinclude template = "teapot/comments/act_comments.cfm">
	<cfinclude template = "teapot/comments/act_spamDelay.cfm">
</cfcase>
<cfcase value="download">
	<cfinclude template = "teapot/post/act_download.cfm">
</cfcase>
<cfdefaultcase>
	<cfinclude template = "teapot/library/dsp_header.cfm">
	<cfinclude template = "teapot/post/qry_fulllist.cfm">
	<cfinclude template = "teapot/post/index.cfm">
</cfdefaultcase>
</cfswitch>

<cfinclude template = "teapot/library/dsp_footer.cfm">